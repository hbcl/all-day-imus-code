% This function is for specific activity.
% Use this function when you are confident about what are you doing.
% e.g. someone is walking during 1010 sec to 3021 sec, then you run the following code to analyze the data

%% clean up everything
clear all,close all,clc;

%% Load the data
% Only deal with the ".h5" file, using the following function to load the data
%[files_left] = uigetfile({'*.h5'},'Select IMU Files for Left foot');
%[files_right] = uigetfile({'*.h5'},'Select IMU Files for right foot');
%[LeftWb,LeftAb,RightWb,RightAb,PERIOD] = sync_apdm(files_left,files_right);

[name, folder] = uigetfile({'*.h5'},'Select IMU Files for analysis');
combined_file = fullfile(folder, name);
[LeftWb,LeftAb,LeftMb,RightWb,RightAb,RightMb,PERIOD] = sync_New_apdm_combined(combined_file);

%% define the section
% Look at the data to make sure:
%   1. During the static period, the data should lay on the x-axis.
%   2. if not, we should add "BIAS" part in "getdata function"

% note: Unit of SECTION is sec.
% note: if things are not going as expected, check the synchronization of both feet
% note: for running, set MIN_WALK_SPEED as 3, W_FF = 60, A_FF =2
% note: for walking, set MIN_WALK_SPEED as 2, W_FF = 40, A_FF =1
Begin = 1;
End = size(LeftAb,1)*PERIOD; 
BIAS = [];
 % this will make the plots over the whole day       
SECTION = [Begin End];
[LeftWb,LeftAb] = getdata(LeftWb,LeftAb,PERIOD,SECTION,BIAS);
[RightWb,RightAb] = getdata(RightWb,RightAb,PERIOD,SECTION,BIAS);

disp('Please consider which section of data you want to analyze');pause;
Begin = input('Enter the section begins (sec)--> ');
End =  input('Enter the section ends (sec)--> ');
BIAS = input('Enter the length of bias (sec)(enter to use default bias)--> ');
SECTION1 = [Begin End];
% plotbrowser('on')
% disp('How much you wanna shift right foot(postive for shifting right, negative for left)');pause;
%shift = input('Enter the shift amount (sec)(negative to left, positive to right)--> ');
%SECTION2 = [Begin+shift End+shift];
[LeftWb,LeftAb] = getdata(LeftWb,LeftAb,PERIOD,SECTION1,BIAS);
[RightWb,RightAb] = getdata(RightWb,RightAb,PERIOD,SECTION1,BIAS);

%% compute position
% this function is valid for continious steady activity. (less than 3 hours)
[left_walk_info,right_walk_info] = compute_pos_two_imus(LeftWb,LeftAb,RightWb,RightAb,PERIOD);

%% compute strides
Filter = input('Do you wanna turn on the filter, if yes, type 1, otherwise type 0--> ');
left_strides= stride_segmentation(left_walk_info,PERIOD,Filter);
right_strides= stride_segmentation(right_walk_info,PERIOD,Filter);
%comb_sec(left_strides,right_strides)

%% info about strides in each bout (use it for whole day data)
% Note! this function is time consuming!
%[left_strides.Section, left_strides.Hist, left_strides.data]= data_divider(LeftWb,LeftAb,PERIOD);
%[right_strides.Section, right_strides.Hist, right_strides.data]= data_divider(RightWb,RightAb,PERIOD);

%% plot all the info
left_strides.length = left_strides.frwd(end,:);
left_strides.clearance = clearance(left_strides,PERIOD );
plt_ltrl_frwd_strides(left_strides);
plt_frwd_elev_strides(left_strides);
plt_stride_var(left_strides);
plt_valid( left_strides );
right_strides.length = right_strides.frwd(end,:);
right_strides.clearance = clearance(right_strides,PERIOD );
plt_ltrl_frwd_strides(right_strides);
plt_frwd_elev_strides(right_strides);
plt_stride_var(right_strides);
plt_valid( right_strides );
plt_len_spd(left_strides,right_strides);
%plt_dis_spd(left_strides,right_strides);
%plt_dis_len(left_strides,right_strides);

%% saving all figures to current directory
% h = get(0,'children');
% for i=1:length(h)
% % saveas(h(i), ['figure' num2str(i)], 'fig');
%   saveas(h(i), ['figure' num2str(i)], 'png');
% end
% 
% %% saving all variables
% save('strides.mat',PERIOD,left_strides,right_strides,LeftWb,LeftAb,RightWb,RightAb,SECTION1,SECTION2)
