# How to process full-day IMU data #

1st  Identify which IMU is for left foot and which IMU is for right foot 

(This is very important! (label of data might be unreliable) check the record (both voice and notes) make sure two feet actually match)

2nd  Check the voice recorder and identify what’s happening during the day.

 (time and duration of each activity)

3rd  Load the data using the main function provided in the bitbucket called `All_Day_Analysis.m` or `Quick_Process.m`
 
(`Quick_Process.m` gives you a general view of whole day data, `All_Day_Analysis.m` gives you a detailed view about a certain section)

4th  Process the data according to each activity separately( read each comment in the code may solve the concerns)

5th  Check the synchronization if two feet are not matching well

NOTE: if some errors happen during analysis, check the following link. https://docs.google.com/document/d/18x_0ME1ii6pg37XSfhEzIHUslvoAkHKsUS753S_MkY8/edit

NOTE: there are several sections included in the main function, I suggested to process them by sections according to what figures you need (there are detailed comments about each section in main functions)