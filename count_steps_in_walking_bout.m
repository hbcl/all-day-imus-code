function [walking_bout,start_ind,frwd_speeds] = count_steps_in_walking_bout(strides)
max_rest = 4/0.0078; 
totalsteps = size(strides.start_end,2);
walking_bout = [];
nsteps = 1;
count = 1;
this_start = 1;
this_speed = [];
this_speed(1) = strides.frwd_speed(1);
for i=2:totalsteps
    if (strides.start_end(1,i) - strides.start_end(2,i-1) <= max_rest) 
        % this step starts less than a couple seconds from where the previous step ends
        nsteps=nsteps+1;
        this_speed(nsteps) = strides.frwd_speed(i);
    else
      walking_bout(count) = nsteps;
      start_ind(count) = this_start;
      frwd_speeds(count) = mean(this_speed);
      this_speed = [];
      this_speed(1) = strides.frwd_speed(i);
      nsteps = 1;
      count=count+1;
      this_start = i;
    end
    
end

if (nsteps > 1) 
   walking_bout(count) = nsteps;
   start_ind(count) = this_start;
   frwd_speeds(count) = mean(this_speed);
end


end