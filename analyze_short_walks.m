close all
clear all

addpath('New Code/');

subjects = 2;
distances = 1:10;
section = 1:4;

% this is the directory you keep data in
% looks for data in folders labelled subject1, subject2, ...
% .h5 filenames should include left, right, or waist (*left*.h5)
% there should also be a .mat file with auto-osman output
% for each section, trials should be labelled '1a', '1b, '2a', ...
base_dir = '/Users/elizabeth/Documents/extended_reaching/walking_only/';

% distances in meters
actual_dist = [1.1 1.7 2.2 2.8 3.3 3.8 5.1 7 9.1 12.7];
trials=['a','b']; % 2 trials of each distance in each section
colors = jet(length(actual_dist));
for isubject=subjects
    
% this is where figures should be saved
save_dir = ['figures_subject' int2str(isubject) '/'];

subjectfolder = strcat('subject',int2str(isubject));
lefts = dir(fullfile(base_dir,subjectfolder,'*left*.h5'));
rights = dir(fullfile(base_dir,subjectfolder,'*right*.h5'));
waists = dir(fullfile(base_dir,subjectfolder,'*waist*.h5'));
%filename = dir(fullfile(base_dir,subjectfolder,strcat('walking',int2str(isubject),'_trials.mat')));
filename = dir(fullfile(base_dir,subjectfolder,'*.mat'));

if (size(lefts,1) > 1)
    disp('Warning: multiple h5 files found for left foot')
    for i=1:size(lefts,1)
       disp(fullfile(lefts(i).folder,lefts(i).name)) 
    end
    apdm_left = fullfile(lefts(1).folder,lefts(1).name);
elseif (size(lefts,1) < 1)
    error('No file found for left foot')
else
    apdm_left = fullfile(lefts.folder,lefts.name);
end 

if (size(rights,1) > 1)
    disp('Warning: multiple h5 files found for right foot')
    for i=1:size(rights,1)
       disp(fullfile(rights(i).folder,rights(i).name)) 
    end
    apdm_right = fullfile(rights(1).folder,rights(1).name);
elseif (size(rights,1) < 1)
    error('No file found for right foot')
else
    apdm_right = fullfile(rights.folder,rights.name);
end 

%if (size(waists,1) > 1)
%    disp('Warning: multiple h5 files found for waist')
%    for i=1:size(waists,1)
%       disp(fullfile(waists(i).folder,waists(i).name)) 
%    end
%    apdm_waist = fullfile(waists(1).folder,waists(1).name);
%elseif (size(waists,1) < 1)
%    error('No file found for waist')
%else
%    apdm_waist = fullfile(waists.folder,waists.name);
%end 

if (size(filename,1) > 1)
    disp('Warning: multiple options found for auto-osman output file')
    for i=1:size(filename,1)
       disp(fullfile(filename(i).folder,filename(i).name)) 
    end
    filename1 = fullfile(filename(1).folder,filename(1).name);
elseif (size(filename,1) < 1)
    error('No file found for auto-osman output')
else
    filename1 = fullfile(filename.folder,filename.name);
end 


load(filename1);

[LeftWb1,LeftAb1,RightWb1,RightAb1,PERIOD] = sync_New_apdm(apdm_left,apdm_right,1,0);
BIAS = [];

filtered_vboth_all_dist = {};
lengthspeedplot=figure('Name','Step length vs. speed','Renderer', 'painters', 'Position', [10 10 1200 600]);

for idistance = distances
  figurelabel = ['Subject ' int2str(isubject) ' Distance ' int2str(idistance)];
right_vel={};
left_vel={};
count = 1;


total_vel = {};
rstepnum = {};
lstepnum = {};
right_times = {};
left_times = {};
step_times = {};

avgvel1_all = {};
avgvel2_all = {};
V1_all = {};
V2_all = {};
V1_all_rescaled = {};
V2_all_rescaled = {};
ff2_all = {};
ff1_all = {};
color1_all = {};
color2_all = {};
avgvel1_point_all = {};
avgvel2_point_all = {};
times1_all = {};
times2_all = {};

footvelplot=figure('Name',figurelabel,'Renderer', 'painters', 'Position', [10 10 1200 600]);
%footposplot=figure('Name',figurelabel,'Renderer', 'painters', 'Position', [10 10 1200 600]);
%step_length_plot=figure('Name',figurelabel,'Renderer', 'painters', 'Position', [10 10 1200 600]);
for isection=section % 4 sections
      for itrial=1:length(trials) % 2 trials per section
        index = find(strcmp({sections(isection).trials.name},strcat(num2str(idistance),trials(itrial)))==1);
        Begin = sections(isection).trials(index).startTime;
        End = sections(isection).trials(index).endTime; 
        
        SECTION = [Begin End];
        [LeftWb,LeftAb] = getdata(LeftWb1,LeftAb1,PERIOD,SECTION,BIAS,[],[],[],0);
        [RightWb,RightAb] = getdata(RightWb1,RightAb1,PERIOD,SECTION,BIAS,[],[],[],0);
        %[WaistWb,WaistAb] = getdata(WaistWb1,WaistAb1,PERIOD,SECTION,BIAS,[],[],[],0);
      Filter = 0;
      walking_filter = 0;
      if walking_filter
        periodic_section = find_walking_sections(LeftWb,LeftAb,RightWb,RightAb,WaistWb,WaistAb,PERIOD);
        periodic_section = merge_walking_sections(periodic_section,4/PERIOD);
      else
        periodic_section = ones(length(LeftAb),1);    
      end
      [left_walk_info,right_walk_info] = compute_pos_two_imus(LeftWb,LeftAb,RightWb,RightAb,PERIOD,walking_filter,periodic_section);
      
      % try to fix the problem with choosing the first footfall position
      if (length(left_walk_info.FFstart) == 1) && (length(right_walk_info.FFstart) == 2)
          % check that right foot takes the first step
          if (right_walk_info.FFstart(1) < left_walk_info.FFstart(1))
            left_walk_info.FFstart(1)=floor(0.5*(right_walk_info.FFstart(1)+right_walk_info.FFend(1)));
          end
          % and the last step
          if (right_walk_info.FFend(end) > left_walk_info.FFend(end))
            left_walk_info.FFend(1) = floor(0.5*(right_walk_info.FFstart(end)+right_walk_info.FFend(end)));
          end
      end    
      
      % same thing if left foot has 2 steps and right foot has 1
      if (length(left_walk_info.FFstart) == 2) && (length(right_walk_info.FFstart) == 1)
          if (left_walk_info.FFstart(1) < right_walk_info.FFstart(1))
            right_walk_info.FFstart(1)=floor(0.5*(left_walk_info.FFstart(1)+left_walk_info.FFend(1)));
          end
          if (left_walk_info.FFend(end) > right_walk_info.FFend(end))
            right_walk_info.FFend(1) = floor(0.5*(left_walk_info.FFstart(end)+left_walk_info.FFend(end)));
          end
      end 
      
      left_strides= stride_segmentation(left_walk_info,PERIOD,Filter);
      right_strides= stride_segmentation(right_walk_info,PERIOD,Filter);

      % plot the strides
      %plt_ltrl_frwd_strides(left_strides);
      %title('Left strides, flat terrain')
      %plt_ltrl_frwd_strides(right_strides);
      %title('Right strides, flat terrain')
      
      
      
      if ~isempty(left_strides) && ~isempty(right_strides)
     left_strides_new = left_strides;
     right_strides_new = right_strides;
          % if first step or last step are backwards, cut them out
          if (left_strides.length(1) < 0) % left foot, first step
             names = fieldnames(left_strides);
             for i=1:length(names)
                 if (strcmp(names{i},'time') || strcmp(names{i},'foot_heading') || strcmp(names{i},'diff_foot_heading') || strcmp(names{i},'step_samples') || strcmp(names{i},'frwd_speed_corrected') || strcmp(names{i},'frwd_speed') || strcmp(names{i},'length') )
                   new_field=getfield(left_strides,names{i});
                   if (length(new_field) > 1)
                     new_field = new_field(2:end);
                     left_strides_new = setfield(left_strides_new,names{i},new_field);
                   end
                 end
                 if (strcmp(names{i},'frwd_swing') || strcmp(names{i},'ltrl_swing') || strcmp(names{i},'frwd') || strcmp(names{i},'ltrl') || strcmp(names{i},'abs_frwd') || strcmp(names{i},'elev') || strcmp(names{i},'theta') || strcmp(names{i},'start_end'))
                   new_field=getfield(left_strides,names{i});
                   if (size(new_field,2) > 1)
                     new_field = new_field(:,2:end);
                     left_strides_new = setfield(left_strides_new,names{i},new_field);
                   end
                 end     
             end
             if (length(left_walk_info.FFstart) > 1)
               left_walk_info.FFstart = left_walk_info.FFstart(2:end);
               left_walk_info.FFend = left_walk_info.FFend(2:end);
             end
          end
          % left foot, last step
          if (left_strides.length(end) < 0)
             names = fieldnames(left_strides);
             for i=1:length(names)
                 if (strcmp(names{i},'time') || strcmp(names{i},'foot_heading') || strcmp(names{i},'diff_foot_heading') || strcmp(names{i},'step_samples') || strcmp(names{i},'frwd_speed_corrected') || strcmp(names{i},'frwd_speed') || strcmp(names{i},'length') )
                   new_field=getfield(left_strides,names{i});
                   if (length(new_field) > 1)
                     new_field = new_field(1:end-1);
                     left_strides_new = setfield(left_strides_new,names{i},new_field);
                   end
                 end
                 if (strcmp(names{i},'frwd_swing') || strcmp(names{i},'ltrl_swing') || strcmp(names{i},'frwd') || strcmp(names{i},'ltrl') || strcmp(names{i},'abs_frwd') || strcmp(names{i},'elev') || strcmp(names{i},'theta') || strcmp(names{i},'start_end'))
                   new_field=getfield(left_strides,names{i});
                   if (size(new_field,2) > 1)
                     new_field = new_field(:,1:end-1);
                     left_strides_new = setfield(left_strides_new,names{i},new_field);
                   end
                 end     
             end
             if (length(left_walk_info.FFstart) > 1)
               left_walk_info.FFstart = left_walk_info.FFstart(1:end-1);
               left_walk_info.FFend = left_walk_info.FFend(1:end-1);
             end
          end          
          
          if (right_strides.length(1) < 0) % right foot, first step
             names = fieldnames(right_strides);
             for i=1:length(names)
                 if (strcmp(names{i},'time') || strcmp(names{i},'foot_heading') || strcmp(names{i},'diff_foot_heading') || strcmp(names{i},'step_samples') || strcmp(names{i},'frwd_speed_corrected') || strcmp(names{i},'frwd_speed') || strcmp(names{i},'length') )
                   new_field=getfield(right_strides,names{i});
                   if (length(new_field) > 1)
                     new_field = new_field(2:end);
                     right_strides_new = setfield(right_strides_new,names{i},new_field);
                   end
                 end
                 if (strcmp(names{i},'frwd_swing') || strcmp(names{i},'ltrl_swing') || strcmp(names{i},'frwd') || strcmp(names{i},'ltrl') || strcmp(names{i},'abs_frwd') || strcmp(names{i},'elev') || strcmp(names{i},'theta') || strcmp(names{i},'start_end'))
                   new_field=getfield(right_strides,names{i});
                   if (size(new_field,2) > 1)
                     new_field = new_field(:,2:end);
                     right_strides_new = setfield(right_strides_new,names{i},new_field);
                   end
                 end     
             end
             if (length(right_walk_info.FFstart) > 1)
               right_walk_info.FFstart = right_walk_info.FFstart(2:end);
               right_walk_info.FFend = right_walk_info.FFend(2:end);
             end
          end
          % right foot, last step
          if (right_strides.length(end) < 0)
             names = fieldnames(right_strides);
             for i=1:length(names)
                 if (strcmp(names{i},'time') || strcmp(names{i},'foot_heading') || strcmp(names{i},'diff_foot_heading') || strcmp(names{i},'step_samples') || strcmp(names{i},'frwd_speed_corrected') || strcmp(names{i},'frwd_speed') || strcmp(names{i},'length') )
                   new_field=getfield(right_strides,names{i});
                   if (length(new_field) > 1)
                     new_field = new_field(1:end-1);
                     right_strides_new = setfield(right_strides_new,names{i},new_field);
                   end
                 end
                 if (strcmp(names{i},'frwd_swing') || strcmp(names{i},'ltrl_swing') || strcmp(names{i},'frwd') || strcmp(names{i},'ltrl') || strcmp(names{i},'abs_frwd') || strcmp(names{i},'elev') || strcmp(names{i},'theta') || strcmp(names{i},'start_end'))
                   new_field=getfield(right_strides,names{i});
                   if (size(new_field,2) > 1)
                     new_field = new_field(:,1:end-1);
                     right_strides_new = setfield(right_strides_new,names{i},new_field);
                   end
                 end     
             end
             if (length(right_walk_info.FFstart) > 1)
               right_walk_info.FFstart = right_walk_info.FFstart(1:end-1);
               right_walk_info.FFend = right_walk_info.FFend(1:end-1);
             end
          end          
          
          left_strides = left_strides_new;
          right_strides = right_strides_new;
          
          left_total = sum(left_strides.frwd(end,:));
          right_total = sum(right_strides.frwd(end,:));
          new_total = (left_total+right_total)/2.0;
          
          left_strides_new = zeros(size(left_strides.frwd,2),1);
          right_strides_new = zeros(size(right_strides.frwd,2),1);
          
          for i=1:size(left_strides.frwd,2)
            left_strides_new(i) = left_strides.frwd(end,i)*new_total/left_total;
          end

          for i=1:size(right_strides.frwd,2)
            right_strides_new(i) = right_strides.frwd(end,i)*new_total/right_total;
          end
    
          left_strides_rescaled = left_strides;
          right_strides_rescaled = right_strides;
          left_walk_info_rescaled = left_walk_info;
          right_walk_info_rescaled = right_walk_info;
          
          names = fieldnames(left_strides);
          for i=1:length(names)
              if ~(strcmp(names{i},'time') || strcmp(names{i},'foot_heading') || strcmp(names{i},'diff_foot_heading') || strcmp(names{i},'start_end'))
              left_strides_rescaled = setfield(left_strides,names{i},getfield(left_strides,names{i})*new_total/left_total);
              right_strides_rescaled = setfield(right_strides,names{i},getfield(right_strides,names{i})*new_total/right_total);
              end
          end
          
          names = fieldnames(left_walk_info);
          for i=1:length(names)
              if ~(strcmp(names{i},'is_walking') || strcmp(names{i},'quaternion') || strcmp(names{i},'euler') || strcmp(names{i},'FFstart') || strcmp(names{i},'FFend'))
              left_walk_info_rescaled = setfield(left_walk_info,names{i},getfield(left_walk_info,names{i})*new_total/left_total);
              right_walk_info_rescaled = setfield(right_walk_info,names{i},getfield(right_walk_info,names{i})*new_total/right_total);
              end
          end
          
          left_scale_factor(idistance,count) = new_total/left_total;
          right_scale_factor(idistance,count) = new_total/right_total;
          
      right_vel{count} = right_strides.frwd(end,:)./right_strides.time*right_scale_factor(idistance,count);
      left_vel{count} = left_strides.frwd(end,:)./left_strides.time*left_scale_factor(idistance,count);
      labels{count} = ['Trial',' ',sections(isection).trials(index).name,' ',sections(isection).name];
      
      left_times{count} = left_strides.time;
      right_times{count} = right_strides.time;
      
      % combine strides into a single array

      lstepcount = length(left_vel{count});
      rstepcount = length(right_vel{count});
      total_vel{count} = zeros(lstepcount+rstepcount,1);
      step_times{count} = zeros(lstepcount+rstepcount,1);
      rstepnum{count} = zeros(rstepcount,1);
      lstepnum{count} = zeros(lstepcount,1);
      ri = 1;
      li = 1;

      for ti = 1:(lstepcount+rstepcount)
        if (ri > rstepcount)
          total_vel{count}(ti) = left_vel{count}(li);
          step_times{count}(ti) = left_strides.time(li);
          lstepnum{count}(li) = ti;
          li = li+1;
        elseif (li > lstepcount)
          total_vel{count}(ti) = right_vel{count}(ri);
          step_times{count}(ti) = right_strides.time(ri);
          rstepnum{count}(ri) = ti;
          ri = ri+1;
        elseif (right_strides.start_end(1,ri) < left_strides.start_end(1,li))
          total_vel{count}(ti) = right_vel{count}(ri);
          step_times{count}(ti) = right_strides.time(ri);
          rstepnum{count}(ri) = ti;
          ri = ri+1;
        else
          total_vel{count}(ti) = left_vel{count}(li);
          step_times{count}(ti) = left_strides.time(li);
          lstepnum{count}(li) = ti;
          li = li+1;
        end
    
      end
     

      left_FF = left_walk_info.FFstart;
      left_FF = [left_FF left_walk_info.FFend(end)];
      right_FF = right_walk_info.FFstart;
      right_FF = [right_FF right_walk_info.FFend(end)];

      % check which foot takes the first step
      if right_FF(1) < left_FF(1)
        FF1 = right_FF;
        FF2 = left_FF;
        walk_info1 = right_walk_info;
        walk_info2 = left_walk_info;
        walk_info1_rescaled = right_walk_info_rescaled;
        walk_info2_rescaled = left_walk_info_rescaled;
        strides1 = right_strides;
        strides2 = left_strides;
        label1 = 'Right foot';
        label2 = 'Left foot';
        color1 = 'b';
        color2 = 'r';
      else
        FF1 = left_FF;
        FF2 = right_FF;
        walk_info1 = left_walk_info;
        walk_info2 = right_walk_info;
        walk_info1_rescaled = left_walk_info_rescaled;
        walk_info2_rescaled = right_walk_info_rescaled;
        strides1 = left_strides;
        strides2 = right_strides;
        label1 = 'Left foot';
        label2 = 'Right foot';
        color1 = 'r';
        color2 = 'b';
      end
      
      if FF1(end) > FF2(end) % check which foot fall is last
        ffend = FF1(end);
      else
        ffend = FF2(end);
      end

      % rotate the foot paths so that they align with the x-axis

      ang2 = find_avg_direction(walk_info2.P(FF1(1):ffend,1),walk_info2.P(FF1(1):ffend,2));
      Prot2 = rotateP(walk_info2.P(FF1(1):ffend,:),ang2,'z'); 
      Vrot2 = rotateP(walk_info2.V(FF1(1):ffend,:),ang2,'z');
      P2 = Prot2(:,1); 
      V2 = Vrot2(:,1);


      % P2, V2 start at first footfall, end at last footfall

      ang1 = find_avg_direction(walk_info1.P(FF1(1):ffend,1),walk_info1.P(FF1(1):ffend,2));
      Prot1 = rotateP(walk_info1.P(FF1(1):ffend,:),ang1,'z');
      Vrot1 = rotateP(walk_info1.V(FF1(1):ffend,:),ang1,'z');
      P1 = Prot1(:,1);
      V1 = Vrot1(:,1);

      

      
      % adjust the footfall indices
      offset = FF1(1);
      FF1 = FF1-offset+1; 
      FF2 = FF2-offset+1;
      
      % make sure velocity is 0 before the first footfall
      V2(1:FF2) = 0;
      V1(1:FF1) = 0;
      
      % make sure the velocities don't end up negative
      if mean(V1(FF1(1):FF1(end))) < 0
         V1=-V1; 
      end
      if mean(V2(FF2(1):FF2(end))) < 0
         V2=-V2; 
      end
      
      
      if strcmp(label2,'Left foot')
          V2_rescaled = V2*new_total/left_total;
          V1_rescaled = V1*new_total/right_total;
          
          P2_rescaled = P2*new_total/left_total;
          P1_rescaled = P1*new_total/right_total;   
      else
          V2_rescaled = V2*new_total/right_total;
          V1_rescaled = V1*new_total/left_total; 
          
          P2_rescaled = P2*new_total/right_total;
          P1_rescaled = P1*new_total/left_total; 
      end
      
      %disp(length(P1));
      % shift so both feet start and end together
      dif1 = P2_rescaled(FF2(1)) - P1_rescaled(FF1(1)); % difference between feet at beginning
      dif2 = P2_rescaled(FF2(end)) - P1_rescaled(FF1(end)); % difference between feet at end
      % dif1 and dif2 should be equal if using P1_rescaled and P2_rescaled
      %disp(dif2-dif1)
      P1_rescaled = P1_rescaled-(P2_rescaled(FF2(1)) - (0.5*(dif1+dif2)));
      P2_rescaled = P2_rescaled-P2_rescaled(FF2(1)); % shift left to start at 0


      % plot the left and right foot position
      %figure(footposplot)
      %subplot(2,4,2*(isection-1)+itrial)      
      %t=(0:length(P1_rescaled)-1)*PERIOD;
      %plot(t,P1_rescaled,'DisplayName',label1,'Color',color1)
      %hold on
      P2_rescaled(1:FF2(1)) = 0;
      %plot(t,P2_rescaled,'DisplayName',label2,'Color',color2)

      % plot left and right footfalls
      %plot(FF1*PERIOD,P1_rescaled(FF1),'ko','DisplayName','Foot fall')
      %h1=plot(FF1*PERIOD,P1_rescaled(FF1),'ko');
      %set(get(get(h1(1),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
      %h1=plot(FF2*PERIOD,P2_rescaled(FF2),'ko');
      %set(get(get(h1(1),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

      avg = (P1_rescaled+P2_rescaled)*0.5;
      %plot(t,avg,'DisplayName','Average of both feet');
      %xlabel('Time (s)')
      %ylabel('Position (m)')
      %legend('Location','northwest')
      
      
      % plot avg foot velocity over each step and continuous foot velocity
      %figure(footvelplot)
      %subplot(2,4,2*(isection-1)+itrial)
      avgvel1 = zeros(size(V1));
      avgvel2 = zeros(size(V2));
      avgvel1_point = zeros(length(FF1)-1,1);
      times1 = zeros(length(FF1)-1,1);
      avgvel2_point = zeros(length(FF2)-1,1);
      times2 = zeros(length(FF2)-1,1);
      
      for i=1:length(FF1)-1
        avgvel1(FF1(i):FF1(i+1)) = strides1.frwd_speed(i);
        avgvel1_point(i) = strides1.frwd_speed(i);
        times1(i) = (FF1(i)+FF1(i+1))/2.0*PERIOD;
      end
      for i=1:length(FF2)-1
        avgvel2(FF2(i):FF2(i+1)) = strides2.frwd_speed(i);
        avgvel2_point(i) = strides2.frwd_speed(i);
        times2(i) = (FF2(i)+FF2(i+1))/2.0*PERIOD;
      end
      
      %plot(t,avgvel1,'Color',color1,'LineWidth',1,'DisplayName',label1)
      %hold on
      %plot(t,avgvel2,'Color',color2,'LineWidth',1,'DisplayName',label2)
      %plot(t,V1,'Color',color1,'DisplayName',label1)
      %plot(t,V2,'Color',color2,'DisplayName',label2)
      %plot(t,waistV,'Color','k','DisplayName','Waist')
%      if 2*(isection-1)+itrial == 1
%        legend
%      end
      %xlabel('Time (s)')
      %ylabel('Forward Velocity (m/s)')
      
      % save velocities to calculate average later
      avgvel1_all{count} = avgvel1;
      avgvel2_all{count} = avgvel2;
      V1_all{count} = V1;
      V2_all{count} = V2;
      V1_all_rescaled{count} = V1_rescaled;
      V2_all_rescaled{count} = V2_rescaled;
      ff1_all{count} = FF1;
      ff2_all{count} = FF2;
      color1_all{count} = color1;
      color2_all{count} = color2;
      avgvel1_point_all{count} = avgvel1_point;
      avgvel2_point_all{count} = avgvel2_point;
      times1_all{count} = times1;
      times2_all{count} = times2;
      
      
      count = count+1;
      end
      
      end
end

% put all the velocities into a big array to get the average
length1=cellfun(@(x) length(x),V1_all);
max1 = max(length1);
min1 = min(length1);
%V1_arr = zeros(count-1,max1);
%V2_arr = zeros(count-1,max1);
V1_arr_resamp = zeros(count-1,min1);
V2_arr_resamp = zeros(count-1,min1);
V1_arr_rescaled_resamp = zeros(count-1,min1);
V2_arr_rescaled_resamp = zeros(count-1,min1);
avgvel1_resamp = zeros(count-1,min1);
avgvel2_resamp = zeros(count-1,min1);
for i=1:count-1
   %V1_arr(i,1:length1(i))=V1_all{i}; 
   %V2_arr(i,1:length1(i))=V2_all{i}; 
   V1_arr_resamp(i,:) = resample(V1_all{i},min1,length1(i));
   V2_arr_resamp(i,:) = resample(V2_all{i},min1,length1(i));
   V1_arr_rescaled_resamp(i,:) = resample(V1_all_rescaled{i},min1,length1(i));
   V2_arr_rescaled_resamp(i,:) = resample(V2_all_rescaled{i},min1,length1(i));
   avgvel1_resamp(i,:) = resample(avgvel1_all{i},min1,length1(i));
   avgvel2_resamp(i,:) = resample(avgvel2_all{i},min1,length1(i));
end
t=(0:min1-1)*PERIOD;
V1mean = mean(V1_arr_resamp,1);
V2mean = mean(V2_arr_resamp,1);

if ~exist('peak_speed','var')
   peak_speed = zeros(length(actual_dist),count-1); 
end

if ~exist('peak_speed2','var')
   peak_speed2 = zeros(length(actual_dist),count-1); 
end

if ~exist('walk_duration','var')
   walk_duration = zeros(length(actual_dist),count-1); 
end

figure(footvelplot)
hold on
extra_length = 0; % pad the velocity arrays with zeros to make the plots look nicer
%extra_length = floor(size(V1_arr_rescaled_resamp,2)/4);
vboth = zeros(count-1,size(V1_arr_rescaled_resamp,2)+(extra_length*2));
filter_freq = 1.0;
%bigV1_arr_resamp = zeros(length(V1_arr_resamp(1,:))+(extra_length*2),1);
%bigV2_arr_resamp = zeros(length(V2_arr_resamp(1,:))+(extra_length*2),1);
tmpplot = zeros(size(V1_arr_rescaled_resamp,2)+(extra_length*2),1);
tbig = 1:length(tmpplot);
tbig = (tbig-extra_length)/size(V1_arr_rescaled_resamp,2);
%bigvboth = zeros(size(V1_arr_rescaled_resamp,2)+(extra_length*2),1);
npoints=size(V1_arr_rescaled_resamp,2);
for i=1:count-1
      tmpplot = zeros(size(V1_arr_rescaled_resamp,2)+(extra_length*2),1);
      tmpplot(extra_length+1:end-extra_length) = V1_arr_resamp(i,:);
      plot(tbig,tmpplot,'Color',color1_all{i})
      tmpplot(extra_length+1:end-extra_length) = V2_arr_resamp(i,:);
      plot(tbig,tmpplot,'Color',color2_all{i})
      % average of both feet
      %vboth(i,:) = mean([V1_arr_rescaled_resamp(i,:); V2_arr_rescaled_resamp(i,:)]);
      tmpplot(extra_length+1:end-extra_length) = mean([V1_arr_rescaled_resamp(i,:); V2_arr_rescaled_resamp(i,:)]);
      plot(tbig,tmpplot,'Color','#7E2F8E');
      % low pass filter average of both feet
      [b,a] = butter(2,filter_freq*PERIOD);
      tmpfilter = zeros(npoints*3,1);
      mean_v1_v2 = mean([V1_arr_rescaled_resamp(i,:); V2_arr_rescaled_resamp(i,:)]);
      tmpfilter(1:npoints) = -flip(mean_v1_v2);
      tmpfilter(npoints+1:2*npoints) = mean_v1_v2;
      tmpfilter(2*npoints+1:3*npoints) = -flip(mean_v1_v2);
      filteredtmp = filtfilt(b,a,tmpfilter);
      filtered_vboth = zeros(npoints+2*extra_length,1);
      filtered_vboth(extra_length+1:extra_length+npoints) = filteredtmp(npoints+1:npoints*2);
      tmpplot = filtered_vboth;
      plot(tbig,tmpplot,'Color','#77AC30');
      vboth(i,:) = filtered_vboth;
      % average step velocity
      tmpplot = zeros(size(avgvel1_all{i},1)+(extra_length*2),1);
      tavgvel = 1:length(tmpplot);
      tavgvel = (tavgvel-extra_length)/size(avgvel1_all{i},1);
      tmpplot(extra_length+1:end-extra_length) = avgvel1_all{i};
      plot(tavgvel,tmpplot,'Color',color1_all{i},'LineWidth',1)
      tmpplot(extra_length+1:end-extra_length) = avgvel2_all{i};
      plot(tavgvel,tmpplot,'Color',color2_all{i},'LineWidth',1)  
      % save peak body speed to plot later
      peak_speed(idistance,i) = max(vboth(i,:));
      peak_speed2(idistance,i) =  max([avgvel1_point_all{i}; avgvel2_point_all{i}]);
end
% and save walk duration
walk_duration(idistance,:) = length1*PERIOD;
tmpplot = zeros(size(V1_arr_rescaled_resamp,2)+(extra_length*2),1);
tmpplot(extra_length+1:end-extra_length) = V1mean;
%plot(tbig,tmpplot,'Color','k','LineWidth',2,'DisplayName','Average over all trials');
tmpplot(extra_length+1:end-extra_length) = V2mean;
%plot(tbig,tmpplot,'Color','k','LineWidth',2,'DisplayName','Average over all trials');
totalmean = mean([V1mean; V2mean]);
tmpplot(extra_length+1:end-extra_length) = totalmean;
%plot(tbig,tmpplot,'Color','#7E2F8E','LineWidth',2,'DisplayName','Average of both feet over all trials');
filtered_totalmean = mean(vboth);
filtered_vboth_all_dist{idistance} = vboth;
tmpplot = filtered_totalmean;
plot(tbig,tmpplot,'Color','#77AC30','LineWidth',2,'DisplayName','Average of both feet over all trials');
for i=1:count-1
  this_ff1=ff1_all{i};
  ff1_new = this_ff1*PERIOD/walk_duration(idistance,i);
  this_ff2=ff2_all{i};
  ff2_new = this_ff2*PERIOD/walk_duration(idistance,i);
  dif_ff1=diff(ff1_new);
  plot(ff1_new(1:end-1)+(0.5*dif_ff1),avgvel1_point_all{i},'o','Color',color1_all{i},'MarkerSize',5)
  dif_ff2=diff(ff2_new);
  plot(ff2_new(1:end-1)+(0.5*dif_ff2),avgvel2_point_all{i},'o','Color',color2_all{i},'MarkerSize',5)
    
end
xlabel('Time (percentage of walk)')
ax = gca;
ax.FontSize = 18;

%figure(step_length_plot)
%hold on
% for j=1:count-1
%   if strcmp(color1_all{j},'r')
%     step_lengths_left=zeros(length(avgvel1_all{j}),1);
%     t=(1:length(avgvel1_all{j}))*PERIOD;
%     ind = 1;
%   else
%     step_lengths_left=zeros(length(avgvel2_all{j}),1);
%     t=(1:length(avgvel2_all{j}))*PERIOD;
%     ind=ff2_all{j}(1);
%   end
%   times = left_times{j};
%   vels=left_vel{j};
%   for i=1:length(vels)
%     step_lengths_left(ind:ind+times(i)/PERIOD)=vels(i)*times(i);
%     ind=ind+times(i)/PERIOD;
%   end
%   plot(t,step_lengths_left,'Color','r')
%   % right foot
%   if strcmp(color1_all{j},'b')
%     step_lengths_right=zeros(length(avgvel1_all{j}),1);
%     t=(1:length(avgvel1_all{j}))*PERIOD;
%     ind=1;
%   else
%     step_lengths_right=zeros(length(avgvel2_all{j}),1);
%     t=(1:length(avgvel2_all{j}))*PERIOD;
%     ind=ff2_all{j}(1);
%   end
%   times = right_times{j};
%   vels=right_vel{j};
%   for i=1:length(vels)
%     step_lengths_right(ind:ind+times(i)/PERIOD)=vels(i)*times(i);
%     ind=ind+times(i)/PERIOD;
%   end
%   plot(t,step_lengths_right,'Color','b')
% end
% 
% xlabel('Time (s)')
% ylabel('Stride length (m)')

% plot step velocity as points
%figure
%hold on
%for i=1:count-1
%    plot(times1_all{i},avgvel1_point_all{i},'Color',color1_all{i});
%    plot(times2_all{i},avgvel2_point_all{i},'Color',color2_all{i});
%end
%xlabel('Time (s)')
%ylabel('Forward Velocity (m/s)')


% plot with both feet combined
% step speed vs. step number
colors=jet(count-1);
stepfig=figure('Name',figurelabel,'Renderer', 'painters', 'Position', [10 10 1200 600]);
hold on
for i=1:count-1
  % plot with a dashed line if uneven terrain
  if endsWith(labels{i},'uneven2') || endsWith(labels{i},'uneven1')
    plot(total_vel{i},'--','DisplayName',labels{i},'Color',colors(i,:));
  else % plot with a solid line
    plot(total_vel{i},'DisplayName',labels{i},'Color',colors(i,:));
  end
  h1 = plot(rstepnum{i},right_vel{i},'s','Color',colors(i,:));
  h2 = plot(lstepnum{i},left_vel{i},'^','Color',colors(i,:));
  set(get(get(h1(1),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
  set(get(get(h2(1),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');      
end


xlabel('Step number');
ylabel('Forward velocity (m/s)');
title(figurelabel);
legend
ax = gca;
ax.FontSize = 18;

% step distance vs. step number
distnum=figure('Name',figurelabel,'Renderer', 'painters', 'Position', [10 10 1200 600]);
hold on
for i=1:count-1
  % plot with a dashed line if uneven terrain
  if endsWith(labels{i},'uneven2') || endsWith(labels{i},'uneven1')
    plot(total_vel{i}.*step_times{i},'--','DisplayName',labels{i},'Color',colors(i,:));
  else % plot with a solid line
    plot(total_vel{i}.*step_times{i},'DisplayName',labels{i},'Color',colors(i,:));
  end
  h1 = plot(rstepnum{i},right_vel{i}.*right_times{i},'s','Color',colors(i,:));
  h2 = plot(lstepnum{i},left_vel{i}.*left_times{i},'^','Color',colors(i,:));
  set(get(get(h1(1),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
  set(get(get(h2(1),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');      
end


xlabel('Step number');
ylabel('Step distance (m)');
title(figurelabel);
legend


% step time vs. step number
timenum=figure('Name',figurelabel,'Renderer', 'painters', 'Position', [10 10 1200 600]);
hold on
for i=1:count-1
  % plot with a dashed line if uneven terrain
  if endsWith(labels{i},'uneven2') || endsWith(labels{i},'uneven1')
    plot(step_times{i},'--','DisplayName',labels{i},'Color',colors(i,:));
  else % plot with a solid line
    plot(step_times{i},'DisplayName',labels{i},'Color',colors(i,:));
  end
  h1 = plot(rstepnum{i},right_times{i},'s','Color',colors(i,:));
  h2 = plot(lstepnum{i},left_times{i},'^','Color',colors(i,:));
  set(get(get(h1(1),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
  set(get(get(h2(1),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');      
end


xlabel('Step number');
ylabel('Step time (s)');
title(figurelabel);
legend


% step distance vs. step time
distnum=figure('Name',figurelabel,'Renderer', 'painters', 'Position', [10 10 1200 600]);
hold on
for i=1:count-1
  % plot with a dashed line if uneven terrain
  if endsWith(labels{i},'uneven2') || endsWith(labels{i},'uneven1')
    plot(step_times{i},total_vel{i}.*step_times{i},'--','DisplayName',labels{i},'Color',colors(i,:));
  else % plot with a solid line
    plot(step_times{i},total_vel{i}.*step_times{i},'DisplayName',labels{i},'Color',colors(i,:));
  end
  h1 = plot(right_times{i},right_vel{i}.*right_times{i},'s','Color',colors(i,:));
  h2 = plot(left_times{i},left_vel{i}.*left_times{i},'^','Color',colors(i,:));
  set(get(get(h1(1),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
  set(get(get(h2(1),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');      
end


xlabel('Step time (s)');
ylabel('Step distance (m)');
title(figurelabel);
legend


figure(lengthspeedplot)
hold on
colors=jet(length(actual_dist));
for i=1:count-1
    this_vel = right_vel{i};
    this_length = right_vel{i}.*right_times{i};
    nsteps = length(this_vel);
    step_colors = pink(nsteps);
    for j=1:nsteps
        plot(this_vel(j),this_length(j),'Marker','s','MarkerEdgeColor',colors(idistance,:),'MarkerFaceColor',step_colors(j,:),'MarkerSize',10)
    end
%plot(right_vel{i},right_vel{i}.*right_times{i},'Marker','s','Color',colors(idistance,:),'LineStyle','none')
%plot(left_vel{i},left_vel{i}.*left_times{i},'Marker','^','Color',colors(idistance,:),'LineStyle','none')
    this_vel = left_vel{i};
    this_length = left_vel{i}.*left_times{i};
    nsteps = length(this_vel);
    step_colors = pink(nsteps);
    for j=1:nsteps
        plot(this_vel(j),this_length(j),'Marker','^','MarkerEdgeColor',colors(idistance,:),'MarkerFaceColor',step_colors(j,:),'MarkerSize',10)
    end

end
ylabel('Step length')
xlabel('Stride forward speed')

end % for idistance=distances

if (length(distances) > 3)
figure
plot(actual_dist(distances),peak_speed(distances,:),'-ob');
% peak_speed = max of average velocity of both feet
hold on
%plot(actual_dist,peak_speed2,'-ok');
% peak_speed2 = max stride speed
xlabel('Distance');
ylabel('Peak speed (filtered body speed)');
xlim([0 inf]) 
ylim([0 inf])

% add a fit line
n_dist = length(distances);
x_fit = zeros(n_dist+1,1); % include the point (0,0)
y_fit = zeros(n_dist+1,1);
x_fit(2:end) = actual_dist(distances);
y_fit(2:end) = mean(peak_speed(distances,:),2);
fitfun = fittype( @(a,b,x) a*(1-exp(-x/b)) );
[fitted_curve,gof] = fit(x_fit,y_fit,fitfun,'StartPoint',[1.5 2.0]);
coeffvals = coeffvalues(fitted_curve);
x100 = 0:0.01:x_fit(end);
h=plot(x100,fitted_curve(x100),'LineWidth',1.2,'color','k');
eqnlabel = [num2str(coeffvals(1)) '*(1-exp(-x/' num2str(coeffvals(2)) '))'];
label(h,eqnlabel)

% plot peak speed vs. time
figure
plot(walk_duration(distances,:),peak_speed(distances,:),'-o')
hold on
xlabel('Walk duration (s)');
ylabel('Peak speed (filtered body speed)');
x_fit(2:end) = mean(walk_duration(distances,:),2);
y_fit(2:end) = mean(peak_speed(distances,:),2);
[fitted_curve,gof] = fit(x_fit,y_fit,fitfun,'StartPoint',[1.5 2.0]);
coeffvals = coeffvalues(fitted_curve);
%h2=plot(x100,fitted_curve(x100));
eqnlabel = [num2str(coeffvals(1)) '*(1-exp(-x/' num2str(coeffvals(2)) '))'];
%label(h2,eqnlabel)
% another fit line, but add an x offset
x_fit2 = x_fit(2:end); % take out (0,0)
y_fit2 = y_fit(2:end);
fitfun = fittype( @(a,b,c,x) a*(1-exp(-(x-c)/b)) );
[fitted_curve,gof] = fit(x_fit2,y_fit2,fitfun,'StartPoint',[1.7 0.5 1.0]);
fitted_curve_speed_dur{isubject} = fitted_curve;
coeffvals = coeffvalues(fitted_curve);
h2=plot(x100,fitted_curve(x100),'LineWidth',1.2,'color','k');
ylim([0 inf])
eqnlabel = [num2str(coeffvals(1)) '*(1-exp(-(x-'  num2str(coeffvals(3)) ')/' num2str(coeffvals(2)) '))'];
label(h2,eqnlabel)

figure
plot(actual_dist(distances),walk_duration(distances,:),'-o');
xlabel('Distance');
ylabel('Walk duration (s)');
xlim([0 inf]) 
ylim([0 inf])
hold on
%plot(actual_dist,actual_dist/mean(peak_speed2(end,:)),'color','k')
%plot(actual_dist,actual_dist/mean(peak_speed(end,:)),'color','b')

% fit line for this plot

y_fit(2:end)=mean(walk_duration(distances,:),2);
x_fit(2:end) = actual_dist(distances);
fitfun = fittype( @(a,b,c,x) x/a+b*(1-exp(-x/c)) );
[fitted_curve,gof] = fit(x_fit,y_fit,fitfun,'StartPoint',[1.5 2.2 1.0]);
coeffvals = coeffvalues(fitted_curve);
h3 = plot(x100,fitted_curve(x100),'LineWidth',1.2,'color','k');
fitted_curve_dur_dist{isubject} = fitted_curve;
eqnlabel = ['x/' num2str(coeffvals(1)) '+' num2str(coeffvals(2)) '*(1-exp(-x/' num2str(coeffvals(3)) '))'];
label(h3,eqnlabel)
end %if (length(distances) > 3) 

% plot filtered avg speed for all distances
colors = jet(length(distances));
avg_speed_all=figure('Renderer', 'painters', 'Position', [10 10 1200 600]);
hold on
for i=1:length(distances)
    tbig = 1:size(filtered_vboth_all_dist{distances(i)},2);
    %tbig = tbig/((4.0/6.0)*size(filtered_vboth_all_dist{i},2)) - (1/6.0);
    tbig = (tbig-extra_length)/(size(filtered_vboth_all_dist{distances(i)},2)-2*extra_length);
    %t = (1:size(filtered_vboth_all_dist{i},2))./size(filtered_vboth_all_dist{i},2);
    plot(tbig,filtered_vboth_all_dist{distances(i)},'Color',colors(i,:));
    plot(tbig,mean(filtered_vboth_all_dist{distances(i)}),'Color',colors(i,:),'LineWidth',2);

end
 xlabel('Time (percentage of walk)')
 ylabel('Filtered average speed of both feet')


% the same thing but speed in seconds
avg_speed_all2=figure('Renderer', 'painters', 'Position', [10 10 1200 600]);
hold on
for i=1:length(distances)
    nsamples = size(filtered_vboth_all_dist{distances(i)},2);
    for j=1:size(filtered_vboth_all_dist{distances(i)},1)
        tbig = 1:nsamples;
        %tbig = (tbig/((4.0/6.0)*nsamples) - (1/6.0))*walk_duration(i,j);
        tbig = (tbig-extra_length)/(nsamples-2*extra_length)*walk_duration(distances(i),j);
        plot(tbig,filtered_vboth_all_dist{distances(i)}(j,:),'Color',colors(i,:));
    end
    plot(tbig,mean(filtered_vboth_all_dist{distances(i)}),'Color',colors(i,:),'LineWidth',2);

end
 xlabel('Time (s)')
 ylabel('Filtered average speed of both feet')
 ax = gca;
ax.FontSize = 18;

end

% save all the figures
if ~exist(save_dir, 'dir')
  mkdir(save_dir);
end
h = get(0,'children');
for i=1:length(h)
  saveas(h(i), [strcat(save_dir,'figure') num2str(length(h)-i+1)], 'png'); % this function saves every figure in .png type.
end
