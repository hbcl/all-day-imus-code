function [walking_bout_dist] = get_distance_in_walking_bout(strides)

totalsteps = size(strides.start_end,2);
walking_bout_dist = [];
dist = strides.frwd(end,1);
nsteps = 1;
count = 1;
for i=2:totalsteps
    if (strides.start_end(1,i) == strides.start_end(2,i-1)) % this step starts where the last one ends
        dist = dist+strides.frwd(end,i);
        nsteps=nsteps+1;
    else
      walking_bout_dist(count) = dist;
      dist = strides.frwd(end,i);
      count=count+1;
      nsteps = 1;
    end
    
end

if (nsteps > 1) 
   walking_bout_dist(count) = dist; 
end


end