function[left_strides_total] = merge_strides(left_strides)


for i=1:length(left_strides)
    step_length_arr(i) = size(left_strides(i).frwd,1);
    num_steps_arr(i) = size(left_strides(i).frwd,2);
end
step_length = max(step_length_arr);
num_steps = sum(num_steps_arr);

left_strides_total = struct('frwd_swing',zeros(step_length,num_steps),'ltrl_swing',zeros(step_length,num_steps),'frwd',zeros(step_length,num_steps),'ltrl',zeros(step_length,num_steps),'abs_ltrl',zeros(step_length,num_steps),'abs_frwd',zeros(step_length,num_steps),'elev',zeros(step_length,num_steps),'theta',zeros(step_length,num_steps),'start_end',zeros(2,num_steps),'foot_heading',zeros(num_steps,1),'diff_foot_heading',zeros(num_steps,1),'step_samples',zeros(1,num_steps),'time',zeros(1,num_steps),'frwd_speed_compensated',zeros(1,num_steps),'frwd_speed',zeros(1,num_steps),'length',zeros(1,num_steps));
ind = 1;
group1 = ["frwd_swing","ltrl_swing","frwd","ltrl","abs_ltrl","abs_frwd","elev","theta"];
newfield = zeros(step_length,num_steps);
for igroup=1:length(group1)
    newfield = zeros(step_length,num_steps);
    ind = 1;
    for i=1:length(left_strides)
      thisfield = getfield(left_strides(i),group1(igroup));
      newfield(1:step_length_arr(i),ind:ind+num_steps_arr(i)-1) = thisfield;
      if step_length_arr(i) < step_length
      for j=ind:ind+num_steps_arr(i)-1
        newfield(step_length_arr(i)+1:end,j) = thisfield(end,j-ind+1);
      end
      end
      ind=ind+num_steps_arr(i);
    end
    left_strides_total = setfield(left_strides_total,group1(igroup),newfield);
end
group2 = ["foot_heading","diff_foot_heading","step_samples","time","frwd_speed_compensated","frwd_speed","length"];
for igroup=1:length(group2)
  newfield = zeros(1,num_steps);
  ind = 1;
  for i=1:length(left_strides)
     thisfield = getfield(left_strides(i),group2(igroup));
     newfield(ind:ind+num_steps_arr(i)-1) = thisfield;
     ind=ind+num_steps_arr(i);
  end
  left_strides_total = setfield(left_strides_total,group2(igroup),newfield);
end

ind=1;
for i=1:length(left_strides)
    left_strides_total.start_end(:,ind:ind+num_steps_arr(i)-1) = left_strides(i).start_end;
    ind=ind+num_steps_arr(i);
end

end