function [W,A] = set_orientation(w,a,ORIENTATION,PERIOD)
 
    ORIGINAL = 0;
	LED_UP_RIGHT_FRWD = 1; % This one is normally used on feet
	LED_UP_LEFT_FRWD = 2;
	LED_LOW_RIGHT_FRWD = 3;
	LED_LOW_DOWN_FRWD = 4;
	LED_UP_RIGHT_BACK = 5; 
	LED_LOW_LEFT_BACK = 6;

    %ORIENTATION = LED_UP_RIGHT_FRWD;
	switch(ORIENTATION)
		case ORIGINAL 
			% +X  <---o +Z
			%         |
			%         V +Y
			WX = w(:,1); WY = w(:,2); WZ = w(:,3);
			AX = a(:,1); AY = a(:,2); AZ = a(:,3);
		case LED_UP_RIGHT_FRWD
			WX = w(:,2); WY = w(:,1); WZ = -w(:,3);
			AX = a(:,2); AY = a(:,1); AZ = -a(:,3);
		case LED_UP_LEFT_FRWD
			WX = w(:,1); WY = -w(:,2); WZ = -w(:,3);
			AX = a(:,1); AY = -a(:,2); AZ = -a(:,3);
		case LED_LOW_DOWN_FRWD
			WX = -w(:,2); WY = -w(:,1); WZ = -w(:,3);
			AX = -a(:,2); AY = -a(:,1); AZ = -a(:,3);
		case LED_LOW_RIGHT_FRWD
			WX = -w(:,1); WY = w(:,2); WZ = -w(:,3);
			AX = -a(:,1); AY = a(:,2); AZ = -a(:,3);
		case LED_UP_RIGHT_BACK
			WX = w(:,3); WY = -w(:,1); WZ = w(:,2);
			AX = a(:,3); AY = -a(:,1); AZ = a(:,2);
		case LED_LOW_LEFT_BACK
			WX = w(:,3); WY = w(:,1); WZ = -w(:,2);
			AX = a(:,3); AY = a(:,1); AZ = -a(:,2);
	end;

	W = [WX,WY,WZ]*PERIOD;
	A = [AX,AY,AZ];
