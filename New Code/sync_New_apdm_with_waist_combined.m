% Author: Lauro Ojeda, 2012-2015
function [LeftWb,LeftAb,RightWb,RightAb,WaistWb,WaistAb,PERIOD,LeftMb,RightMb,WaistMb] = sync_New_apdm_with_waist_combined(FILE) %<--------------static_period

info = h5info(FILE);

FREQ  = h5readatt(FILE, info.Groups(2).Groups(1).Groups(1).Name, 'Sample Rate');
PERIOD = 1/double(FREQ);

n_sensors = size(info.Groups(2).Groups,1);

for i=1:n_sensors
    label  = h5readatt(FILE, info.Groups(2).Groups(i).Groups(1).Name, 'Label 0');
    % left foot
    if regexp(label, regexptranslate('wildcard', 'left*'))
      LeftAb = (h5read(FILE, [info.Groups(2).Groups(i).Name,'/Accelerometer']))';
      LeftWb = (h5read(FILE, [info.Groups(2).Groups(i).Name,'/Gyroscope']))';
      LeftMb = (h5read(FILE, [info.Groups(2).Groups(i).Name,'/Magnetometer']))';
    end
    % right foot
    if regexp(label, regexptranslate('wildcard', 'right*'))
      RightAb = (h5read(FILE, [info.Groups(2).Groups(i).Name,'/Accelerometer']))';
      RightWb = (h5read(FILE, [info.Groups(2).Groups(i).Name,'/Gyroscope']))';
      RightMb = (h5read(FILE, [info.Groups(2).Groups(i).Name,'/Magnetometer']))';        
    end    
    % waist
    if regexp(label, regexptranslate('wildcard', 'waist*'))
      WaistAb = (h5read(FILE, [info.Groups(2).Groups(i).Name,'/Accelerometer']))';
      WaistWb = (h5read(FILE, [info.Groups(2).Groups(i).Name,'/Gyroscope']))';
      WaistMb = (h5read(FILE, [info.Groups(2).Groups(i).Name,'/Magnetometer']))';
        
    end
end


if(~exist('LeftAb','var') || isempty(LeftAb)) 
   error('Left foot data not found'); 
end

if(~exist('RightAb','var') || isempty(RightAb)) 
   error('Right foot data not found'); 
end

if(~exist('WaistAb','var') || isempty(WaistAb)) 
   error('Waist data not found'); 
end

% orientation options
    ORIGINAL = 0;
	LED_UP_RIGHT_FRWD = 1; % This one is normally used on feet
	LED_UP_LEFT_FRWD = 2;
	LED_LOW_RIGHT_FRWD = 3;
	LED_LOW_DOWN_FRWD = 4;
	LED_UP_RIGHT_BACK = 5; 
	LED_LOW_LEFT_BACK = 6;
    
    
[LeftWb,LeftAb] = set_orientation(LeftWb,LeftAb,LED_UP_RIGHT_FRWD,PERIOD);
[RightWb,RightAb] = set_orientation(RightWb,RightAb,LED_UP_RIGHT_FRWD,PERIOD);
[WaistWb,WaistAb] = set_orientation(WaistWb,WaistAb,LED_UP_RIGHT_FRWD,PERIOD);

    
    


