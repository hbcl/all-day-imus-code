function [ value, point_index, time ,valid] = findclearance( frwd, elev,  period,IF_DEBUG)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    if ~exist('IF_DEBUG','var')
		IF_DEBUG=0;
    end;

    C_min=0.004;% the minimum clearance allowed
    C_max=0.1;% the maximum clearance allowed
    G_min=0.001;% the minimum clearance to determine if the foot is on the ground
    P_off=0.35;% percent of ignored data not on just after left ground
    P_on=0.25; % percent of ignored data before attach ground in horizantal
    P_default=0.45; % default position of the clearance
    valid=1; % tell if the clearance if found correctly,0 for foot not leaving the ground, 1 is correct, 2, is too many clearance found, 3 is no value find take a set time value
    
    a0=-elev(end)./frwd(end);
    distance=a0*frwd(:)+elev(:)./sqrt(1+a0^2);
    
    index=find(distance>G_min);
    if(isempty(index))
        value=0;
        time=0;
        point_index=1;
        valid=0; 
        return
    end
    index_off=index(1);
    index_on=index(end);
    
    % find a suitable section for taking the data 
    frwd_length=frwd(index_on)-frwd(index_off);
    mid_point=find(frwd> frwd_length*P_default,1);
    mid_point1=find(frwd> frwd_length*P_off,1);
    mid_point2=find(frwd> frwd_length*(1-P_on),1);
    if isempty(mid_point1) || isempty(mid_point2)
        value=0;
        time=0;
        point_index=1;
        valid=2; %strange trajactory
        return
    end
    [fpeak,fpeak_loc]=max(distance(1:mid_point1));
    [rpeak,rpeak_loc]=max(distance(mid_point2+1:end));
    rpeak_loc=rpeak_loc+mid_point2;
%     section_start=find(frwd> frwd_length*P_off,1);
%     section_end=find(frwd> frwd_length*(1-P_on),1);
%     smooth_section=section_start:section_end;
    smooth_section=fpeak_loc:rpeak_loc;
    
    % first time to find the data
    [value,index]=min(distance(smooth_section));
    point_index=smooth_section(index);
    time=period*(point_index-index_off);
    
    if value==fpeak
        valid=3; % cannot find the front peak
    elseif value==rpeak
        valid=4; % cannot find the rear peak
    elseif value<C_min
        valid=5; % less than expect value
    elseif value>C_max
        valid=6; % less than expect value
    end
    
    
    
        
    
    
% %     find the peaks
%     [peak,index]=findpeaks(-distance);
%     peak=-peak;
%     
% %     remove the peak that is too small (readom errors) or not in the time
% %     limit
%     tmp=find( (peak>C_min) & (index>index_off+T_off/period) & ((index<index_on-T_on/period)));
%     index=index(tmp);
%     peak=peak(tmp);
% 
%      if isempty(index)
%         valid=3;
%         point_index=index_off+floor(T_default/period);
%         value=distance(point_index);
%     elseif length(index)>1
%         valid=2;
%         [value,tmp]=min(peak);
%         point_index=index(tmp);
%     else
%         value=peak(1);
%         point_index=index(1);
%     end
    if IF_DEBUG
        real=subplot(2,1,1);
        plot(frwd(:),-elev(:),'-',frwd(point_index),-elev(point_index),'x','markersize', 30);
        normed=subplot(2,1,2);
        plot(frwd(:),distance(:),'-',frwd(point_index),distance(point_index),'x','markersize', 30);
        axis equal
        linkaxes([real,normed],'x');
        title(sprintf('at %g s, error code %d',time, valid));
%         if (mod(ii,1)==0) 
%             title(sprintf('%d at %g',ii,time));
%             pause();
%             hold(normed, 'off');
%             hold(real, 'off');
%         else
%             hold(real, 'on');
%             hold(normed, 'on');
%         end
    end
    
end

