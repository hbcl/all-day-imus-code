function [num_strides,result] = compute_strides(W,A,PERIOD,USE_KF,W_FF,A_FF,T_FF,MAX_T_FF,FF)
% [result] = compute_pos(W,A,PERIOD,USE_KF,W_FF,A_FF,T_FF,MAX_T_FF,FF)
% 
% Computes IMU positions assuming zero velocity updates
% 
% result: a structure containing results fields
% inputs: 
%   W: Angular Velocity, finite difference form (Angular Velocity (rad/sec) * PERIOD = finite integral of angular velocity over a single sample) 
%   A: Acceleration (m/s/s)
%   PERIOD: Sampling Period (seconds)
% Optional Inputs
%   USE_KF: logical, use (default = 1) or do not use (=0) the Tilt Kalman Filter to correct IMU tilt. 
%   W_FF: Upper Threshold for Angular Velocity (deg/sec) for detecting footfalls 
%   A_FF: Upper Threshold for (Acceleration minus Gravity) (m/s/s) for detecting footfalls 
%   T_FF: Minimum Time that must elapse between two footfalls (default 0.4 sec)
%   MAX_T_FF: Maximum Time that can elapse During Rest Periods before looking for a new footfall (seconds; default 3*T_FF) 
%   FF: Logical array for all the samples in W and A, indicating whether they are Footfalls. 
%       Use this to override internal footfall computations, e.g. if
%       footfall detection is not working well, or for IMUs not on the
%       foot. 


% Set Up Optional Arguments
	if(~exist('USE_KF','var') | isempty(USE_KF))
		USE_KF = 1;
	end;
	if(~exist('W_FF','var') | isempty(W_FF))
		W_FF = [];
	end;
	if(~exist('A_FF','var') | isempty(A_FF))
		A_FF = [];
	end;
	if(~exist('T_FF','var') | isempty(T_FF))
        T_FF = []; % Minimum time allowed between two FFs
    end;
    if(~exist('MAX_T_FF','var') | isempty(MAX_T_FF))
        MAX_T_FF = []; % Maximum rest period allowed before searching for new FFs
    end;
    
% Inertial navigation mechanization
	N = size(W,1);
	t = (1:N)*PERIOD;
	% Compute tilt based on accelerometer readings
	[accel_phi,accel_theta] = acc_tilt(A);
	
	% Determine FFs
    if(~exist('FF','var') | isempty(FF))
        [num_strides,FF,stationary_periods] = foot_fall_count(W,A,PERIOD,W_FF,A_FF,T_FF,MAX_T_FF);
%         Filtering version, experimental for cutting off spikes.
%         FREQUENCY = 1/PERIOD;
%         cutoff = 5 ; % Hz cutoff of Butterworth filter for Footfall detection. 
%         [b,a] = butter(2, cutoff/(FREQUENCY/2), 'low');
%         [FF,stationary_periods] = foot_fall(filtfilt(b,a,W),filtfilt(b,a,A),PERIOD,W_FF,A_FF,T_FF,MAX_T_FF);
    else
        [num_strides,FF,stationary_periods] = foot_fall_count(W,A,PERIOD,W_FF,A_FF,T_FF,MAX_T_FF); % in this case, keep the FF that was put in, but still find stationary periods using "foot_fall"
%         Filtering version, experimental for cutting off spikes.
%         FREQUENCY = 1/PERIOD;
%         cutoff = 5 ; % Hz cutoff of Butterworth filter for Footfall detection. 
%         [b,a] = butter(2, cutoff/(FREQUENCY/2), 'low');
% 		[blah,stationary_periods] = foot_fall(filtfilt(b,a,W),filtfilt(b,a,A),PERIOD,W_FF,A_FF,T_FF,MAX_T_FF); % in this case, keep the FF that was put in, but still find stationary periods using "foot_fall"
	end;
	result.FF = FF;

	% Initialize variables
	quaternion = zeros(N,4);
	An = zeros(N,3);
	Anz = zeros(N,3);
	% Initialize quaternions
	quaternion(1,:) = kf_tilt(PERIOD);
	for(i = 2:N)
		% Compute attitude using quaternion representation
		quaternion(i,:) = qua_est(W(i,:),quaternion(i-1,:));
		% Transform accelerations from body to navigation frame
		rotation_matrix = qua2rot(quaternion(i,:)); An(i,:) = rotation_matrix*A(i,:)';
		if(USE_KF)
			% Apply KF compensation on tilt
			quaternion(i,:) = kf_tilt(PERIOD,quaternion(i,:),accel_theta(i),accel_phi(i),stationary_periods(i));
		end;
		% Apply Zero Velocity Updates
		% This script has not ben made into a function to make the code run faster
		% this call will update the Az variable wich contains the ZUPT updated navigation acceleration
		zupts;
	end;

	result.Anz = Anz;
	result.An = An;
	result.quaternion = quaternion;
	
	% Plot attitude
	euler = qua2eul(quaternion)*180/pi;
	result.euler = euler;

	% Set A/V/P to zero during bias drift estimation
	V = cumtrapz(Anz)*PERIOD;
	Vm = (trapz((V(:,1:2).^2)')).^.5;;
	result.V = V;
	result.Vm = Vm;
	% Use speed information to determine the walking section
	try
	result.FF_walking = detect_walking_section(result);
	catch
	result.FF_walking = result.FF ;
    end
    P = cumtrapz(V)*PERIOD;
	result.P = P;

	


