function [ output_args ] = plt_bar( left_strides,right_strides,distance )
% this function plots the stride length v.s. distance

ind = 1;
for i = 1:100:(length(left_strides.length)-100)
    a = left_strides.length(i:(i+100));
    lstep_len(ind) = mean(a);
    ind = ind+1;
end

 
ind = 1;
for i = 1:100:(length(right_strides.length)-100)
    a = right_strides.length(i:(i+100));
    rstep_len(ind) = mean(a);
    ind = ind+1;
end
ind = 1;
for i = 1:100:(length(left_strides.frwd_speed)-100)
    a = left_strides.frwd_speed(i:(i+100));
    lstep_s(ind) = mean(a);
    ind = ind+1;
end
ind = 1;
for i = 1:100:(length(right_strides.frwd_speed)-100)
    a = right_strides.frwd_speed(i:(i+100));
    rstep_s(ind) = mean(a);
    ind = ind+1;
end

 
ldist = linspace(0,length(distance),length(lstep_s));
rdist = linspace(0,length(distance),length(rstep_s));
ldistance = linspace(0,length(distance),length(lstep_len));
rdistance = linspace(0,length(distance),length(rstep_len));

 
figure;
subplot(2,1,1);bar(ldist,lstep_s,'r','LineWidth',0.4);
xlabel('distance [m]')
ylabel('Left forward speed [m/s]');

box off;
subplot(2,1,2);bar(rdist,rstep_s,'b','LineWidth',0.4);
ylabel('Right forward speed [m/s]');
xlabel('distance [m]')
box off;
 
figure;
subplot(2,1,1);bar(ldistance,lstep_len,'r','LineWidth',0.4);
xlabel('distance [m]')
ylabel('Left Stride Length [m]');

box off;
subplot(2,1,2);bar(rdistance,rstep_len,'b','LineWidth',0.4);
ylabel('Right Stride Length [m]');
xlabel('distance [m]')
box off;

end

