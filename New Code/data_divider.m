function [ Section, Histogram ] = data_divider( Wb, Ab, PERIOD)
% This function looks for periods of walking separated by static periods of
% at least 2 seconds and tells you how many strides in each walking section


% Summary of this function goes here
% this function does
%   1. picks up all the unstatic periods in different section and store it
%   in SECTION
%   2. generate a historgram shows the steps in each bout

% variables initialization
num_periods = 0;
num = 0;
data = struct;
j = 0;
static = 0;
BIAS = 1;
Threshold = 0.1;


% first round filter
diff_Ab=diff(Ab); % <---- this isn't a filter
% secind round filter
for i = 1:length(Ab)
    if abs(diff_Ab(i)) < Threshold
            j = j+1;
    else
        if j*PERIOD > 2 
       % looking for a place with no movement at least 2 seconds long
            static = 1;
            num = j;  % length of long static period in array index units
        end
        j = 0;
    end
      
    if static == 1 % i = array position right after long static period
        num_periods = num_periods+1; % number of long static periods
        section(num_periods,:) = [i-num+1 i+1]; % beginning and end indices of long static period
        static = 0;
    end
end
% adjust Section data;
Section(num_periods-1,2) = 0;
if num_periods < 2
    Section(1,1) = section(1,2);
    Section(1,2) = section(1,2)+2;
else  
    for i = 2:length(section)
      Section(i-1,2) = section(i,1);
      Section(i-1,1) = section(i-1,2);  
    end
end
% section = beginning and ending indices of non-static periods
 data.num_strides(num_periods-1,1)= 0;
% generate the histogram
for i = 1:length(Section)
   SECTION = [Section(i,1)*PERIOD-2,Section(i,2)*PERIOD]; % why the -2 ?
   [W,A] = getdata_new(Wb,Ab,PERIOD,SECTION,BIAS);
   [data.num_strides(i),data.walk_info(i)]=compute_strides(W,A,PERIOD,SECTION);
   sec(i) = (SECTION(1)+SECTION(2))/2;
%   strides= stride_segmentation(data.walk_info(i),PERIOD);
%   data.frwd_speed = strides.frwd_speed;
end
    figure; bar(sec,data.num_strides);
    xlabel('Time (sec)');
    ylabel('number of strides in each bout');

    Histogram = hist(data.num_strides,max(data.num_strides));
    figure;bar(Histogram);
    xlabel('Number of strides in each bout')
    ylabel('Number of appearances')

end

