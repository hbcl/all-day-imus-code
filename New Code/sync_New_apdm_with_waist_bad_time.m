% Author: Lauro Ojeda, 2012-2015
function [LeftWb,LeftAb,RightWb,RightAb,WaistWb,WaistAb,PERIOD,LeftMb,RightMb,WaistMb, static_period] = sync_New_apdm_with_waist_bad_time(L_FILE,R_FILE,W_FILE,SYNC,FORCE_SYNC_VALUE,label) %<--------------static_period
if(~exist('SYNC','var'))
	SYNC = 1; 
end
if(~exist('FORCE_SYNC_VALUE','var'))
	FORCE_SYNC_VALUE = 0;% Use a non-zero value (counts) combined with SYNC=1 in order to force data synchronization
end
if(~exist('label','var'))
	label = left;% Use a non-zero value (counts) combined with SYNC=1 in order to force data synchronization
end
% caseIdList = hdf5read(L_FILE,'/CaseIdList');
% groupName = caseIdList(1).data;
% L_time = hdf5read(L_FILE, [groupName '/Time']);
% FREQ = hdf5read(L_FILE, [groupName '/SampleRate']);
% PERIOD = 1/double(FREQ);
% 
% caseIdList = hdf5read(R_FILE,'/CaseIdList');
% groupName = caseIdList(1).data;
% R_time = hdf5read(R_FILE, [groupName '/Time']);
% L_SECTION = [1,size(L_time,1)]*PERIOD;
% R_SECTION = [1,size(R_time,1)]*PERIOD;


infoL = h5info(L_FILE);
L_time = h5read(L_FILE, [infoL.Groups(2).Groups(1).Name,'/Time']);

infoR = h5info(R_FILE);
R_time = h5read(R_FILE, [infoR.Groups(2).Groups(1).Name,'/Time']);

infoW = h5info(W_FILE);
W_time = h5read(W_FILE, [infoW.Groups(2).Groups(1).Name,'/Time']);

FREQ  = h5readatt(R_FILE, infoR.Groups(2).Groups(1).Groups(1).Name, 'Sample Rate');
PERIOD = 1/double(FREQ);
L_SECTION = [1,size(L_time,1)];
R_SECTION = [1,size(R_time,1)];
W_SECTION = [1,size(W_time,1)];


if(SYNC)
	%if(FORCE_SYNC_VALUE)
		% If a known value of the time shift is know, replace the variable FORCE_SYNC_VALUE 
		% with the correct value, you may use croscorrelations to find the correct shift value
	%	warning('Sync with user defined value');
%		shift = FORCE_SYNC_VALUE;
	%else
		display('Sync with sensor timer');
        
        figure; % this needs to be fixed
        if strcmp(label,'l') || strcmp(label,'left')
          L_time = L_time+R_time(1)-L_time(1);
        elseif strcmp(label,'r') || strcmp(label,'right')
          R_time = R_time+L_time(1)-R_time(1); 
%        elseif strcmp(label,'w') || strcmp(label,'waist')
          %W_time = W_time+(offset*1000000);  
        end
        
        breakhere=0;
        while (1)
        
        [LeftWb,LeftAb,PERIOD,LeftMb] = getdata_New_apdm(L_FILE,1);
        [RightWb,RightAb,PERIOD,RightMb] = getdata_New_apdm(R_FILE,1);
        [WaistWb,WaistAb,PERIOD,WaistMb] = getdata_New_apdm(W_FILE,1);
        
        
        [m,i] = max([L_time(1) R_time(1) W_time(1)]);
        if i == 1  % shift right and waist
            r_shift = find(R_time >= L_time(1),1);
            w_shift = find(W_time >=L_time(1),1);
            W_SECTION = [w_shift,size(W_time,1)];
            R_SECTION = [r_shift,size(R_time,1)];
        end
           
        if i == 2 % shift left and waist
            l_shift = find(L_time >= R_time(1),1);
            w_shift = find(W_time >=R_time(1),1);
            W_SECTION = [w_shift,size(W_time,1)];
            L_SECTION = [l_shift,size(L_time,1)];            
        end
            
        if i == 3 % shift left and right
            r_shift = find(R_time >= W_time(1),1);
            l_shift = find(L_time >=W_time(1),1);
            L_SECTION = [l_shift,size(L_time,1)];
            R_SECTION = [r_shift,size(R_time,1)];                       
            
        end
        
        LeftWbsection = LeftWb(L_SECTION(1):L_SECTION(2),:);
        RightWbsection = RightWb(R_SECTION(1):R_SECTION(2),:);
        WaistWbsection = WaistWb(W_SECTION(1):W_SECTION(2),:);
        
        tleft = (1:size(LeftWbsection,1))*PERIOD;
        tright = (1:size(RightWbsection,1))*PERIOD;
        twaist = (1:size(WaistWbsection,1))*PERIOD;
        plt_hour=0;
if plt_hour
    tscale = 3600;
else
    tscale = 1;
end

sh_xa(1) = subplot(3,1,1);
plot(tleft/tscale,LeftWbsection/PERIOD*180/pi);
grid on;
ylabel('Left W [deg/s]');
title('Body referenced inertial signals');
hold off;

sh_xa(2) = subplot(3,1,2);
plot(tright/tscale,RightWbsection/PERIOD*180/pi);
grid on;
ylabel('Right W [deg/s]');

sh_xa(3) = subplot(3,1,3);
plot(twaist/tscale,WaistWbsection/PERIOD*180/pi);
grid on;
ylabel('Waist W [deg/s]');

linkaxes(sh_xa,'x');
if plt_hour
xlabel('time [hr]');
else
xlabel('time [s]');   
end


if breakhere
   break 
end
if FORCE_SYNC_VALUE
   offset = FORCE_SYNC_VALUE;
   breakhere = 1;
end
%offset = input('Enter new offset (seconds)--> ');
%if isempty(offset)
%    break
%end

if strcmp(label,'l') || strcmp(label,'left')
  L_time = L_time+(offset*1000000);
elseif strcmp(label,'r') || strcmp(label,'right')
  R_time = R_time+(offset*1000000);  
%elseif strcmp(label,'w') || strcmp(label,'waist')
 % W_time = W_time+(offset*1000000);  
end


        end
        
        
%		if(L_time(1) <= R_time(1))
%			shift = -find(L_time >= R_time(1),1)
%		else
%			shift = find(R_time >= L_time(1),1)
%		end;
%	end;
%	if(shift<0)
%		display(sprintf('Shift Left IMU signals [%d]',-shift));
%		L_SECTION = [-shift,size(L_time,1)]*PERIOD; % To shift left side
%	else
%		display(sprintf('Shift Right IMU signals [%d]',shift));
%		R_SECTION = [shift,size(R_time,1)]*PERIOD; % To shift right side
    %end
else
		warning('Not Syncing');
end

% Load data 
[Wb,Ab,PERIOD,Mb] = getdata_New_apdm(L_FILE,1);
[LeftWb,LeftAb, static_period, LeftMb] = getdata(Wb,Ab,PERIOD,L_SECTION*PERIOD,[],Mb); %<--------------static_period
%set(gcf,'Name','Left IMU');
[Wb,Ab,PERIOD,Mb] = getdata_New_apdm(R_FILE,1);
[RightWb,RightAb, ~, RightMb] = getdata(Wb,Ab,PERIOD,R_SECTION*PERIOD,[],Mb); %<--------------static_period
%set(gcf,'Name','Right IMU');
[Wb,Ab,PERIOD,Mb] = getdata_New_apdm(W_FILE,1);
[WaistWb,WaistAb, ~, WaistMb] = getdata(Wb,Ab,PERIOD,W_SECTION*PERIOD,[],Mb);
%set(gcf,'Name','Waist IMU');
