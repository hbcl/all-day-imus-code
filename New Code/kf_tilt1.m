% Author: Lauro Ojeda, 2001-2015
function [quaternion,P,Q,R] = kf_tilt1(PERIOD)
% used with compute_pos2

	% Tilt error compensation using accelerometer - based tilt
	% A one state indirect KF (complementary filter) is used for the process and the measurements are applied only on low
	% motion conditions (is_stationary)
	% KF constants
	%persistent P Q R

		% Initialize KF
		P = 0; % Initial covariance error
		Q = 1.5e-5*PERIOD; % Process (gyros) covariance error
		R = 1.5e-1*PERIOD; % Measurement (accelerometer) covariance error
		quaternion = [1,0,0,0];
end

