% Author: Lauro Ojeda, 2003-2015
function [result] = compute_pos2(W,A,PERIOD,walking_filter,periodic_section,USE_KF,W_FF,A_FF,T_FF,MAX_T_FF,FF)
% same thing as compute_pos but without optional arguments
% [result] = compute_pos(W,A,PERIOD,USE_KF,W_FF,A_FF,T_FF,MAX_T_FF,FF)
% 
% Computes IMU positions assuming zero velocity updates
% 
% result: a structure containing results fields
% inputs: 
%   W: Angular Velocity, finite difference form (Angular Velocity (rad/sec) * PERIOD = finite integral of angular velocity over a single sample) 
%   A: Acceleration (m/s/s)
%   PERIOD: Sampling Period (seconds)
% Optional Inputs
%   USE_KF: logical, use (default = 1) or do not use (=0) the Tilt Kalman Filter to correct IMU tilt. 
%   W_FF: Upper Threshold for Angular Velocity (deg/sec) for detecting footfalls 
%   A_FF: Upper Threshold for (Acceleration minus Gravity) (m/s/s) for detecting footfalls 
%   T_FF: Minimum Time that must elapse between two footfalls (default 0.4 sec)
%   MAX_T_FF: Maximum Time that can elapse During Rest Periods before looking for a new footfall (seconds; default 3*T_FF) 
%   FF: Logical array for all the samples in W and A, indicating whether they are Footfalls. 
%       Use this to override internal footfall computations, e.g. if
%       footfall detection is not working well, or for IMUs not on the
%       foot. 
make_plot = 0;

% Set Up Optional Arguments
% 	if(~exist('USE_KF','var') | isempty(USE_KF))
% 		USE_KF = 1;
%     end
% 	if(~exist('W_FF','var') | isempty(W_FF))
% 		W_FF = 60;
%     end
% 	if(~exist('A_FF','var') | isempty(A_FF))
% 		A_FF = 2;
%     end
%     if(~exist('T_FF','var') | isempty(T_FF))
% 	  T_FF = floor(0.4/PERIOD); % Minimum time allowed between two FFs
%     else
%       T_FF = floor(T_FF/PERIOD); % convert Seconds into Samples
%     end
%   	if(~exist('walking_filter','var') | isempty(walking_filter))
% 		walking_filter = 0;
%     end
    
% Inertial navigation mechanization
	N = size(W,1);
	t = (1:N)*PERIOD;
	% Compute tilt based on accelerometer readings
	[accel_phi,accel_theta] = acc_tilt(A); %accel_phi = roll angle accel_theta = pitch
	
	% Determine FFs
  if(isempty(FF))
		[FFstart,FFend,stationary_periods] = foot_fall2(W,A,PERIOD,walking_filter,periodic_section,W_FF,A_FF,T_FF,MAX_T_FF);
  else
      FFstart = FF(1,:);
      FFend = FF(2,:);      
      % Now find low dynamic conditions over the whole time period to set
      % stationary_periods
      GRAVITY=9.8;
      Wm = (sum((W.^2)')).^.5*180/pi/PERIOD;
      Am = (sum((A.^2)')).^.5 - GRAVITY;
      low_motion=find(Wm<W_FF & abs(Am)<A_FF);
      N=size(W,1);
      % stationary_periods is used by the tilt compensation KF 
      stationary_periods = zeros(N,1);
      stationary_periods(low_motion) = 1;
      
  end

%	result.FF = FF; % FF = 0s and 1s showing footfall locations
    result.FFstart = FFstart;
    result.FFend = FFend;
    result.is_walking = periodic_section;

	% Initialize variables
	quaternion = zeros(N,4);
	An = zeros(N,3);
	Anz = zeros(N,3);
	% Initialize quaternions
	[quaternion(1,:),P,Q,R] = kf_tilt1(PERIOD); 
    
    last_footfall = 0;    
	for(i = 2:N)
		% Compute attitude using quaternion representation
		quaternion(i,:) = qua_est(W(i,:),quaternion(i-1,:)); % qua_est estimates the next step based on previous quaterion and angular velocity
		% Transform accelerations from body to navigation frame
		rotation_matrix = qua2rot(quaternion(i,:)); % make rotation matrix based on current orientation
		%inv_quat = [quaternion(i,1) -quaternion(i,2) -quaternion(i,3) -quaternion(i,4)];
        %this_accel = [0 A(i,1) A(i,2) A(i,3)];
        An(i,:) = rotation_matrix*A(i,:)'; % apply rotation to accelerations
        %accel_rot = quaternion(i,:).*this_accel.*inv_quat;
        %An(i,1) = accel_rot(2);
        %An(i,2) = accel_rot(3);
        %An(i,3) = accel_rot(4);
        if(USE_KF)
			% Apply KF compensation on tilt
            if i == 1170218
			[quaternion(i,:),P,Q,R] = kf_tilt2(PERIOD,quaternion(i,:),accel_theta(i),accel_phi(i),stationary_periods(i),P,Q,R);
            else
            [quaternion(i,:),P,Q,R] = kf_tilt2(PERIOD,quaternion(i,:),accel_theta(i),accel_phi(i),stationary_periods(i),P,Q,R);
            end
        end
		% Apply Zero Velocity Updates
		% This script has not ben made into a function to make the code run faster
		% this call will update the Az variable wich contains the ZUPT updated navigation acceleration
        if(i == 2)
	      last_footfall = 0;
        end
		Anz = zupts_func(An,Anz,FFstart,FFend,i,last_footfall);
        if(ismember(i,FFstart) || ismember(i,FFend))
            last_footfall = i;
        end
    end

	result.Anz = Anz;
	result.An = An;
	result.A = A;
	result.W = W;
	result.quaternion = quaternion;
	
	% Plot attitude
	euler = qua2eul(quaternion);
	result.euler = euler;
	
%	figure,
%	plot(t,euler*180/pi);
%	hold on;grid on;ylabel('Euler angles [deg]');xlabel('time [s]');
%	legend('Roll','Pitch','Heading');
%	plot(t(FF),euler(FF,:)*180/pi,'*');
%	hold off;
	% Set A/V/P to zero during bias drift estimation
	V = cumsum(Anz)*PERIOD;
	Vm = (sum((V(:,1:2).^2)')).^.5;
	result.V = V;
	result.Vm = Vm;
	% Use speed information to determine the walking section
%	try
%		result.FF_walking = detect_walking_section(result);
%	catch
%		result.FF_walking = result.FF; 
%	end;
	
%	figure,
%	plot(t,V,t,Vm,'k');
%	hold on;grid on;ylabel('V [m/s]');xlabel('time [s]');
%	plot(t(FF),Vm(FF),'*g');
%	plot(t(result.FF_walking),Vm(result.FF_walking),'.k');
%	legend('Vx','Vy','Vz','|V|','Footfall','Footfall during walk'); 
%	hold off;
	
	% Compute and plot positions
	P = cumsum(V)*PERIOD;
	result.P = P;
	if make_plot
	figure,
	plot3(P(:,1),-P(:,2),-P(:,3));
	axis equal; grid on; hold on;
	plot3(P(FFstart,1),-P(FFstart,2),-P(FFstart,3),'.k');
	xlabel('X [m]'); ylabel('Y [m]'); zlabel('Z [m]');
	hold off;
    end
