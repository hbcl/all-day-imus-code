function [tlw1,tlw2,tlw3,tla1,tla2,tla3] = dtw_template(LeftWb,LeftAb,left_strides,PERIOD)

[walking_bout,start_ind] = count_steps_in_walking_bout(left_strides);

[max1, ind] = max(walking_bout);

%strideslw1 = zeros(200,size(left_strides.frwd,2));
%strideslw2 = zeros(200,size(left_strides.frwd,2));
%strideslw3 = zeros(200,size(left_strides.frwd,2));
%stridesla1 = zeros(200,size(left_strides.frwd,2));
%stridesla2 = zeros(200,size(left_strides.frwd,2));
%stridesla3 = zeros(200,size(left_strides.frwd,2));

strideslw1 = zeros(200,max1);
strideslw2 = zeros(200,max1);
strideslw3 = zeros(200,max1);
stridesla1 = zeros(200,max1);
stridesla2 = zeros(200,max1);
stridesla3 = zeros(200,max1);

for i=1:max1%size(left_strides.frwd,2)
 lw1 = LeftWb(left_strides.start_end(1,start_ind(ind)+i-1):left_strides.start_end(2,start_ind(ind)+i-1),1);
 lw1resamp = resample(lw1,200,length(lw1));
 strideslw1(:,i) = lw1resamp/PERIOD*180/pi*1/2000;
 
 lw2 = LeftWb(left_strides.start_end(1,start_ind(ind)+i-1):left_strides.start_end(2,start_ind(ind)+i-1),2);
 lw2resamp = resample(lw2,200,length(lw2));
 strideslw2(:,i) = lw2resamp/PERIOD*180/pi*1/2000;
 
 lw3 = LeftWb(left_strides.start_end(1,start_ind(ind)+i-1):left_strides.start_end(2,start_ind(ind)+i-1),3);
 lw3resamp = resample(lw3,200,length(lw3));
 strideslw3(:,i) = lw3resamp/PERIOD*180/pi*1/2000;
 
 la1 = LeftAb(left_strides.start_end(1,start_ind(ind)+i-1):left_strides.start_end(2,start_ind(ind)+i-1),1);
 la1resamp = resample(la1,200,length(la1));
 stridesla1(:,i) = la1resamp/(16*9.8);
 
 la2 = LeftAb(left_strides.start_end(1,start_ind(ind)+i-1):left_strides.start_end(2,start_ind(ind)+i-1),2);
 la2resamp = resample(la2,200,length(la2));
 stridesla2(:,i) = la2resamp/(16*9.8);
 
 la3 = LeftAb(left_strides.start_end(1,start_ind(ind)+i-1):left_strides.start_end(2,start_ind(ind)+i-1),3);
 la3resamp = resample(la3,200,length(la3));
 stridesla3(:,i) = la3resamp/(16*9.8);
 
end

tlw1 = mean(strideslw1,2);
tlw2 = mean(strideslw2,2);
tlw3 = mean(strideslw3,2);
tla1 = mean(stridesla1,2);
tla2 = mean(stridesla2,2);
tla3 = mean(stridesla3,2); 

tla1(1:10) = tla1(10);
tla1(190:200) = tla1(190);
tla2(1:10) = tla2(10);
tla2(190:200) = tla2(190);
tla3(1:10) = tla3(10);
tla3(190:200) = tla3(190);


end