function [ array ] = arrayregulator( array,x)
%UNTITLED2 Summary of this function goes here
%   replace the exteme value in a array with the average of its leader and
%   followers's average.

    % returen empty array if the result is empty
    if isempty(~isnan(array))
        array=[];
        return;
    end
    
    error=x*std(array( ~isnan(array) ) );
    average=mean(array( ~isnan(array) ) );
    index=find(abs(array-average)>error | isnan(array));
    while ~isempty(index)
        if(index(1)==1)
            tmp=find(diff(index)>1,1);
            array(1)=array(tmp+1);
            
            for ii=2:length(index)
                array(index(ii))=array(index(ii)-1);
            end
        else
            for ii=1:length(index)
                array(index(ii))=array(index(ii)-1);
            end
        end
        
        
        error=x*std(array( ~isnan(array) ) );
        average=mean(array( ~isnan(array) ) );
        index=find(abs(array-average)>error | isnan(array));
    end
end

