function [ clearance ] = clearance( strides,PERIOD )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
clearance = struct;
for i = 1:length(strides.frwd(end,:))
    [clearance.value(i) ,clearance.point_index(i),clearance.time(i) ,clearance.valid(i)] = find_clearance(strides.frwd(:,i),strides.elev(:,i), PERIOD);
end
end

