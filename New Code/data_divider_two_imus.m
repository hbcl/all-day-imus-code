function [ SECTION, Histogram ] = data_divider_two_imus( Left_Wb, Left_Ab,Right_Wb, Right_Ab, PERIOD)
    disp('For the left foot:')
   [Left_Sec,Left_hist]= data_divider(Left_Wb,Left_Ab,PERIOD);
   disp('For the right foot:')
   [Right_Sec, Right_hist]= data_divider(Right_Wb,Right_Ab,PERIOD);
   if (length(Left_Sec) >length(Right_Sec))
       SECTION = Left_Sec;
       Histogram = Left_hist;
   else 
       SECTION = Right_Sec;
       Histogram = Right_hist;
   end
end

