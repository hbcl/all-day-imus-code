function [out ] = plt_len_spd( left_strides,right_strides )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if length(left_strides.frwd_speed)<length(left_strides.length)
    left_strides.length = left_strides.length(1:length(left_strides.frwd_speed));
else 
    left_strides.frwd_speed = left_strides.frwd_speed(1:length(left_strides.length));
end
 
if length(right_strides.frwd_speed)<length(right_strides.length)
    right_strides.length = right_strides.length(1:length(right_strides.frwd_speed));
else
    right_strides.frwd_speed = right_strides.frwd_speed(1:length(right_strides.length));
end


figure
subplot(2,1,1);plot(left_strides.frwd_speed,left_strides.length,'b.');
ylabel('Left Stride Length [m]');
xlabel('Left Stride Speed [m/sec]');
 
subplot(2,1,2);plot(right_strides.frwd_speed,right_strides.length,'r.');
ylabel('Right Stride Length [m]');
xlabel('Right Stride Speed [m/sec]');

figure,
hist3([left_strides.frwd_speed;left_strides.length]', [60, 60]);
set(get(gca,'child'),'FaceColor','interp','CDataMode','auto');
ylabel('Stride Length [m]');
xlabel('Stride Speed [m/sec]');

 
end

