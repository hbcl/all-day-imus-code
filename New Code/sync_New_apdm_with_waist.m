% Author: Lauro Ojeda, 2012-2015
function [LeftWb,LeftAb,RightWb,RightAb,WaistWb,WaistAb,PERIOD,LeftMb,RightMb,WaistMb, static_period] = sync_New_apdm_with_waist(L_FILE,R_FILE,W_FILE,SYNC,FORCE_SYNC_VALUE,make_plot) %<--------------static_period
if(~exist('SYNC','var'))
	SYNC = 1; 
end;
if(~exist('FORCE_SYNC_VALUE','var'))
	FORCE_SYNC_VALUE = 0;% Use a non-zero value (counts) combined with SYNC=1 in order to force data synchronization
end;
if(~exist('make_plot','var') || isempty(make_plot)) 
  make_plot = 1;
end
% caseIdList = hdf5read(L_FILE,'/CaseIdList');
% groupName = caseIdList(1).data;
% L_time = hdf5read(L_FILE, [groupName '/Time']);
% FREQ = hdf5read(L_FILE, [groupName '/SampleRate']);
% PERIOD = 1/double(FREQ);
% 
% caseIdList = hdf5read(R_FILE,'/CaseIdList');
% groupName = caseIdList(1).data;
% R_time = hdf5read(R_FILE, [groupName '/Time']);
% L_SECTION = [1,size(L_time,1)]*PERIOD;
% R_SECTION = [1,size(R_time,1)]*PERIOD;


infoL = h5info(L_FILE);
L_time = h5read(L_FILE, [infoL.Groups(2).Groups(1).Name,'/Time']);

infoR = h5info(R_FILE);
R_time = h5read(R_FILE, [infoR.Groups(2).Groups(1).Name,'/Time']);

infoW = h5info(W_FILE);
W_time = h5read(W_FILE, [infoW.Groups(2).Groups(1).Name,'/Time']);

FREQ  = h5readatt(R_FILE, infoR.Groups(2).Groups(1).Groups(1).Name, 'Sample Rate');
PERIOD = 1/double(FREQ);
L_SECTION = [1,size(L_time,1)]*PERIOD;
R_SECTION = [1,size(R_time,1)]*PERIOD;
W_SECTION = [1,size(W_time,1)]*PERIOD;


if(SYNC)
	if(FORCE_SYNC_VALUE)
		% If a known value of the time shift is know, replace the variable FORCE_SYNC_VALUE 
		% with the correct value, you may use croscorrelations to find the correct shift value
		warning('Sync with user defined value');
		shift = FORCE_SYNC_VALUE;
	else
		display('Sync with sensor timer');
        
        
        [m,i] = max([L_time(1) R_time(1) W_time(1)]);
        if i == 1  % shift right and waist
            r_shift = find(R_time >= L_time(1),1);
            w_shift = find(W_time >=L_time(1),1);
            W_SECTION = [w_shift,size(W_time,1)]*PERIOD;
            R_SECTION = [r_shift,size(R_time,1)]*PERIOD;
        end
           
        if i == 2 % shift left and waist
            l_shift = find(L_time >= R_time(1),1);
            w_shift = find(W_time >=R_time(1),1);
            W_SECTION = [w_shift,size(W_time,1)]*PERIOD;
            L_SECTION = [l_shift,size(L_time,1)]*PERIOD;            
        end
            
        if i == 3 % shift left and right
            r_shift = find(R_time >= W_time(1),1);
            l_shift = find(L_time >=W_time(1),1);
            L_SECTION = [l_shift,size(L_time,1)]*PERIOD;
            R_SECTION = [r_shift,size(R_time,1)]*PERIOD;                       
            
        end
        
        
%		if(L_time(1) <= R_time(1))
%			shift = -find(L_time >= R_time(1),1)
%		else
%			shift = find(R_time >= L_time(1),1)
%		end;
%	end;
%	if(shift<0)
%		display(sprintf('Shift Left IMU signals [%d]',-shift));
%		L_SECTION = [-shift,size(L_time,1)]*PERIOD; % To shift left side
%	else
%		display(sprintf('Shift Right IMU signals [%d]',shift));
%		R_SECTION = [shift,size(R_time,1)]*PERIOD; % To shift right side
    end
else
		warning('Not Syncing');
end

% Load data 
[Wb,Ab,PERIOD,Mb] = getdata_New_apdm(L_FILE,1);
[LeftWb,LeftAb, static_period, LeftMb] = getdata(Wb,Ab,PERIOD,L_SECTION,[],Mb,[],[],make_plot); %<--------------static_period
%set(gcf,'Name','Left IMU');
[Wb,Ab,PERIOD,Mb] = getdata_New_apdm(R_FILE,1);
[RightWb,RightAb, ~, RightMb] = getdata(Wb,Ab,PERIOD,R_SECTION,[],Mb,[],[],make_plot); %<--------------static_period
%set(gcf,'Name','Right IMU');
[Wb,Ab,PERIOD,Mb] = getdata_New_apdm(W_FILE,1);
[WaistWb,WaistAb, ~, WaistMb] = getdata(Wb,Ab,PERIOD,W_SECTION,[],Mb,[],[],make_plot);
%set(gcf,'Name','Waist IMU');
