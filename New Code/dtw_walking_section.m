function [left_section,right_section,threshl,threshr] = dtw_walking_section(LeftWb,LeftAb,left_strides,RightWb,RightAb,right_strides,PERIOD,teststart,testend,tlw1,tlw2,tlw3,tla1,tla2,tla3,trw1,trw2,trw3,tra1,tra2,tra3)

if(~exist('teststart','var') || isempty(teststart))
	teststart = 1;
end

if(~exist('testend','var') || isempty(testend))
	testend = size(LeftWb,1);
end

% make a template from existing strides (if it hasn't been already done)
if(~exist('tra3','var') || isempty(tra3))
[tlw1,tlw2,tlw3,tla1,tla2,tla3] = dtw_template(LeftWb,LeftAb,left_strides,PERIOD);
[trw1,trw2,trw3,tra1,tra2,tra3] = dtw_template(RightWb,RightAb,right_strides,PERIOD);
end
%tlw1 = LeftWb(3024210:3024370,1)/PERIOD*180/pi*1/2000;
%tlw2 = LeftWb(3024210:3024370,2)/PERIOD*180/pi*1/2000;
%tlw3 = LeftWb(3024210:3024370,3)/PERIOD*180/pi*1/2000;
%tla1 = LeftAb(3024210:3024370,1)/(16*9.8);
%tla2 = LeftAb(3024210:3024370,2)/(16*9.8);
%tla3 = LeftAb(3024210:3024370,3)/(16*9.8);

%trw1 = RightWb(3024140:3024300,1)/PERIOD*180/pi*1/2000;
%trw2 = RightWb(3024140:3024300,2)/PERIOD*180/pi*1/2000;
%trw3 = RightWb(3024140:3024300,3)/PERIOD*180/pi*1/2000;
%tra1 = RightAb(3024140:3024300,1)/(16*9.8);
%tra2 = RightAb(3024140:3024300,2)/(16*9.8);
%tra3 = RightAb(3024140:3024300,3)/(16*9.8);

%tla1(1:10) = tla1(10);
%tla1(190:200) = tla1(190);
%tla2(1:10) = tla2(10);
%tla2(190:200) = tla2(190);
%tla3(1:10) = tla3(10);
%tla3(190:200) = tla3(190);

% find the step start and end on each foot
[startpoint,endpoint,threshl] = dtw_one_foot(LeftWb,LeftAb,tlw1,tlw2,tlw3,tla1,tla2,tla3,PERIOD,teststart,testend);
[startpointr,endpointr,threshr] = dtw_one_foot(RightWb,RightAb,trw1,trw2,trw3,tra1,tra2,tra3,PERIOD,teststart,testend);

if isempty(startpoint) || isempty(startpointr)
    disp('no steps found')
    left_section = zeros(testend-teststart+1,1);
    right_section = zeros(testend-teststart+1,1);
else
% each step has to be within 1 second of a step on the other foot
stepdist = startpointr'-startpoint;
[row,col] = ind2sub(size(stepdist),find(abs(stepdist) < 1/PERIOD));
stepstart = startpoint(unique(row));
stepend = endpoint(unique(row));
stepstartr = startpointr(unique(col));
stependr = endpointr(unique(col));

% plots
f1 = figure('Name','DTW: left foot');
plot(LeftWb(teststart:testend,:))
hold on
plot(startpoint,zeros(length(startpoint),1),'r*')
plot(stepstart,zeros(length(stepstart),1),'k*')

f2 = figure('Name','DTW: right foot');
plot(RightWb(teststart:testend,:))
hold on
plot(startpointr,zeros(length(startpointr),1),'r*')
plot(stepstartr,zeros(length(stepstartr),1),'k*')

% walking sections are where steps are less than 4 seconds apart 
stepdif = stepstart(2:end)'-stepend(1:end-1);
walkind = find(stepdif > 4/PERIOD);
ps_start = [stepstart(1) stepstart(walkind+1)'];
ps_end = [stepend(walkind) stepend(end)];

left_section = zeros(testend-teststart+1,1);
for i=1:length(ps_start)
left_section(ps_start(i):ps_end(i)) = 1;
end

% same thing on right foot
stepdifr = stepstartr(2:end)'-stependr(1:end-1);
walkindr = find(stepdifr > 2/PERIOD);
ps_startr = [stepstartr(1) stepstartr(walkindr+1)'];
ps_endr = [stependr(walkindr) stependr(end)];

right_section = zeros(testend-teststart+1,1);
for i=1:length(ps_startr)
right_section(ps_startr(i):ps_endr(i)) = 1;
end

end % if isempty(startpoint) || isempty(startpointr) 
end