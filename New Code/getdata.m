% Author: Lauro Ojeda, 2008-2015
function [W,A, static_period, M]  =  getdata(W,A,PERIOD,SECTION_SECONDS,BIAS,M,msg_filename,apdm_date,make_plot)
% BIAS, is the static time at the beggining of the experiment
% leave BIAS the parameter empty to make the system detect the static time automatically
% use msg_filename and apdm_date to include pre-recorded messages in the plot
% msg_filename is a CSV table with entries Date, Time, Message
% apdm_date is the first part of the apdm filename, format yyyymmdd-HHMMSS  
GRAVITY = 9.8; %9.80297286843;
plt_hour = 0; % plot time in hours instead of seconds
%make_plot = 1;

if(exist('BIAS','var') && ~isempty(BIAS)) 
	static_period = (BIAS(1)/PERIOD:BIAS(2)/PERIOD);
    %static_period = (1:BIAS/PERIOD);
else
	static_period = detect_quite_time(W,PERIOD);
    %static_period = [];
end

if(~exist('make_plot','var') || isempty(make_plot)) 
  make_plot = 1;
end

recSec=[]; % seconds between start of data collection and when message was recorded
recMsgs={}; % text of the recorded message
recTimes={}; % time of day 
numRec=0;
if exist('msg_filename','var') && ~isempty(msg_filename)
    recordings = readtable(msg_filename);
    recFormatIn='yyyymmddHH:MM:SS';
    apdmFormatIn='yyyymmdd-HHMMSS';
    apdmDate = datevec(apdm_date,apdmFormatIn);
    for i = 1:size(recordings,1)
      % pull the date/time from the recording
      DateString = strcat(num2str(recordings{i,'Date'}),' ',string(recordings{i,'Time'}));
      recDate=datevec(DateString,recFormatIn);
      % and check if it's within this data period
      recTimeSec=etime(recDate,apdmDate);
      if recTimeSec > 0 && recTimeSec <= (size(W,1)*PERIOD)
        numRec=numRec+1;
        recSec(numRec)=recTimeSec;
        recMsgs{numRec} = char(strcat({' '},char(recordings{i,'Message'})));
        recTimes{numRec} = char(recordings{i,'Time'});
      end
    end
end

% Apply gyro static bias compensation
if(~isempty(static_period)) % skip if no static period found
W = W-ones(size(W,1),1)*mean(W(static_period,:));

% Normalizes accelerometer during static time using a 1-G compensation
static_acceleration = sum((A(static_period,:).^2)').^.5;
gravity_measurement = mean(static_acceleration);
A = A*GRAVITY/gravity_measurement;
end

if(exist('SECTION_SECONDS','var') & ~isempty(SECTION_SECONDS))
	number_of_sections = size(SECTION_SECONDS,1);
	SECTION_SAMPLES = [];
	for(ii=1:number_of_sections)
		SECTION_SAMPLES  =  [SECTION_SAMPLES,(floor(SECTION_SECONDS(ii,1)/PERIOD):floor(SECTION_SECONDS(ii,2)/PERIOD))];
	end;
    % check if SECTION_SAMPLES falls beyond the range of data
    too_high = SECTION_SAMPLES > size(W,1);
    SECTION_SAMPLES(too_high) = size(W,1);
	W = W(SECTION_SAMPLES,:);
	A = A(SECTION_SAMPLES,:);
	if(exist('M','var') && ~isempty(M)) 
        M = M(SECTION_SAMPLES,:); 
    end
end


if(exist('M','var') && ~isempty(M)) 
	include_magnetomter = 1;
else
	include_magnetomter = 0;
end

if make_plot
    figure
t = (1:size(W,1))*PERIOD;
if plt_hour
    tscale = 3600;
else
    tscale = 1;
end
zero = zeros(numRec,1);
sh_xa(1) = subplot(2 + include_magnetomter,1,1);
hold on;
plot(t/tscale,W/PERIOD*180/pi,'.k');
%plot(t(static_period),W(static_period,:)/PERIOD*180/pi,'.k');
legend('Static time');
plot(t/tscale,W/PERIOD*180/pi);
plot(recSec/tscale,zero,'b*','MarkerSize',14,'DisplayName','Recorded message'); % plot times where a message was recorded
labelpoints(recSec/tscale,zero,recTimes,'NW',0.2,1)
grid on;
ylabel('W [deg/s]');
title('Body referenced inertial signals');
hold off;

sh_xa(2) = subplot(2 + include_magnetomter,1,2);plot(t/tscale,A);
grid on;ylabel('A [m/s^2]');

if(include_magnetomter)
	sh_xa(3) = subplot(3,1,3);plot(t/tscale,M); grid on;ylabel('Mag');
end

linkaxes(sh_xa,'x');
if plt_hour
xlabel('time [hr]');
else
xlabel('time [s]');   
end
if numRec>0
  f = figure('menu','none','toolbar','none');
  ph = uipanel(f,'Units','normalized','position',[0.1 0.1 0.8 0.8],'title',...
    'Display window');
  lbh = uicontrol(ph,'style','listbox','Units','normalized','position',...
    [0 0 1 1],'FontSize',9);
  labels = cellfun(@strcat,recTimes,recMsgs,'UniformOutput',false);
  set(lbh,'string',sort(labels));
end
end
end
