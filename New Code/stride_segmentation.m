% Author: Lauro Ojeda, 2011-2015
function stepData = stride_segmentation(walk_info,PERIOD,FILTER,OUTLIER_SECTION_SECONDS)
	if(~exist('FILTER','var') | isempty(FILTER))
		FILTER = 0;
    end

	if(~exist('OUTLIER_SECTION_SECONDS','var') | isempty(OUTLIER_SECTION_SECONDS))
		OUTLIER_SECTION_SAMPLES= [];
	else
		number_of_sections = size(OUTLIER_SECTION_SECONDS,1);
		OUTLIER_SECTION_SAMPLES = [];
		for(ii=1:number_of_sections)
			OUTLIER_SECTION_SAMPLES =  [OUTLIER_SECTION_SAMPLES,(floor(OUTLIER_SECTION_SECONDS(ii,1)/PERIOD):floor(OUTLIER_SECTION_SECONDS(ii,2)/PERIOD))];
        end
    end


    if isempty(walk_info.FFstart)
          disp('Warning: no steps found.'); 
          stepData = [];
    else
	stepData = get_steps_new(walk_info,PERIOD,FILTER,OUTLIER_SECTION_SAMPLES); 
    end

