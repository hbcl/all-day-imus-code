% Author: Lauro Ojeda, 2012-2015
function [LeftWb,LeftAb,LeftMb,RightWb,RightAb,RightMb,PERIOD,WaistWb,WaistAb,WaistMb,HandWb,HandAb,HandMb] = sync_New_apdm_combined(FILE) %<--------------static_period
% use this function to extract data from IMUs
% if they're all together in one file
% At least right and left foot are necessary
% Waist and hand will be set if available

info = h5info(FILE);

FREQ  = h5readatt(FILE, info.Groups(2).Groups(1).Groups(1).Name, 'Sample Rate');
PERIOD = 1/double(FREQ);

n_sensors = size(info.Groups(2).Groups,1);

for i=1:n_sensors
    label  = h5readatt(FILE, info.Groups(2).Groups(i).Groups(1).Name, 'Label 0');
    % left foot
    if regexp(label, regexptranslate('wildcard', 'left*'))
      LeftAb = (h5read(FILE, [info.Groups(2).Groups(i).Name,'/Accelerometer']))';
      LeftWb = (h5read(FILE, [info.Groups(2).Groups(i).Name,'/Gyroscope']))';
      LeftMb = (h5read(FILE, [info.Groups(2).Groups(i).Name,'/Magnetometer']))';
    % right foot
    elseif regexp(label, regexptranslate('wildcard', 'right*'))
      RightAb = (h5read(FILE, [info.Groups(2).Groups(i).Name,'/Accelerometer']))';
      RightWb = (h5read(FILE, [info.Groups(2).Groups(i).Name,'/Gyroscope']))';
      RightMb = (h5read(FILE, [info.Groups(2).Groups(i).Name,'/Magnetometer']))';        
    % waist
    elseif regexp(label, regexptranslate('wildcard', 'waist*'))
      WaistAb = (h5read(FILE, [info.Groups(2).Groups(i).Name,'/Accelerometer']))';
      WaistWb = (h5read(FILE, [info.Groups(2).Groups(i).Name,'/Gyroscope']))';
      WaistMb = (h5read(FILE, [info.Groups(2).Groups(i).Name,'/Magnetometer']))';
    elseif regexp(label, regexptranslate('wildcard', 'hand*'))
      HandAb = (h5read(FILE, [info.Groups(2).Groups(i).Name,'/Accelerometer']))';
      HandWb = (h5read(FILE, [info.Groups(2).Groups(i).Name,'/Gyroscope']))';
      HandMb = (h5read(FILE, [info.Groups(2).Groups(i).Name,'/Magnetometer']))';
    else
        disp('Unidentified sensor')
        disp(label)
    end
    
end


if(~exist('LeftAb','var') || isempty(LeftAb)) 
   error('Left foot data not found'); 
end

if(~exist('RightAb','var') || isempty(RightAb)) 
   error('Right foot data not found'); 
end

if(~exist('WaistAb','var')) 
   WaistWb = [];
   WaistAb = [];
   WaistMb = [];
end

if(~exist('HandAb','var')) 
   HandWb = [];
   HandAb = [];
   HandMb = [];
end

% orientation options
    ORIGINAL = 0;
	LED_UP_RIGHT_FRWD = 1; % This one is normally used on feet
	LED_UP_LEFT_FRWD = 2;
	LED_LOW_RIGHT_FRWD = 3;
	LED_LOW_DOWN_FRWD = 4;
	LED_UP_RIGHT_BACK = 5; 
	LED_LOW_LEFT_BACK = 6;
    
    
[LeftWb,LeftAb] = set_orientation(LeftWb,LeftAb,LED_UP_RIGHT_FRWD,PERIOD);
[RightWb,RightAb] = set_orientation(RightWb,RightAb,LED_UP_RIGHT_FRWD,PERIOD);
if(~isempty(WaistAb) && ~isempty(WaistWb)) 
[WaistWb,WaistAb] = set_orientation(WaistWb,WaistAb,LED_UP_RIGHT_FRWD,PERIOD);
end

if(~isempty(HandAb) && ~isempty(HandWb)) 
[HandWb,HandAb] = set_orientation(HandWb,HandAb,LED_UP_RIGHT_FRWD,PERIOD);
end
    


