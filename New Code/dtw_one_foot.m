function [startpoint,endpoint,thresh] = dtw_one_foot(W,A,tlw1,tlw2,tlw3,tla1,tla2,tla3,PERIOD,teststart,testend)
 
testsamp1 = W(teststart:testend,1)'/PERIOD*180/pi*1/2000;
testsamp2 = W(teststart:testend,2)'/PERIOD*180/pi*1/2000;
testsamp3 = W(teststart:testend,3)'/PERIOD*180/pi*1/2000;
testsampa1 = A(teststart:testend,1)'/(16*9.8);
testsampa2 = A(teststart:testend,2)'/(16*9.8);
testsampa3 = A(teststart:testend,3)'/(16*9.8);

dw1 = sqrt((testsamp1-tlw1).^2);
dw2 = sqrt((testsamp2-tlw2).^2);
dw3 = sqrt((testsamp3-tlw3).^2);
da1 = sqrt((testsampa1-tla1).^2);
da2 = sqrt((testsampa2-tla2).^2);
da3 = sqrt((testsampa3-tla3).^2);

d = dw1+dw2+dw3+da1+da2+da3;

c = zeros(size(d));
c(1,:) = d(1,:);
c(:,1) = cumsum(d(:,1));

prevstart = 1:size(c,2);
for i=2:size(d,1)
  thisstart = zeros(size(c,2),1);
  thisstart(1) = 1;
  for j=2:size(c,2)
      [m,ind] = min([c(i-1,j) c(i-1,j-1) c(i,j-1)]);
      start1 = [prevstart(j) prevstart(j-1) thisstart(j-1)];
      %start2 = [startmat(i-1,j) startmat(i-1,j-1) startmat(i,j-1)];
      %if start1(ind) ~= start2(ind)
      %   disp('bad') 
      %end
      c(i,j) = d(i,j) + m;
      thisstart(j) = start1(ind);
      %startmat(i,j) = start2(ind);
      %indmat(i,j) = ind;
  end
  prevstart = thisstart;
end

%[pks, locs] = findpeaks(-c(end,:),'MinPeakDistance',0.5/PERIOD,'MinPeakProminence',4);
[pks, locs] = findpeaks(-c(end,:),'MinPeakDistance',0.5/PERIOD);
figure
plot(c(end,:));
prompt = 'Input a threshold ';
thresh = input(prompt);
%thresh = 20;
inds = find(pks > -thresh);
minx = locs(inds);
miny = -pks(inds);

pks = miny;
endpoint = minx;
startpoint = thisstart(minx);
% delete overlapping steps
max_overlap = 12;
if isempty(startpoint)
    disp('no steps found')
else
prevstep = startpoint(1):endpoint(1);
prevind = 1;

stepind = [];


for i=2:length(endpoint)
    thisstep = startpoint(i):endpoint(i);
    overlap = length(intersect(prevstep,thisstep));
    if overlap > max_overlap % if steps overlap, only keep one
        if pks(i) < c(end,prevind) % replace prev step with this one
            prevstep = thisstep;
            prevind = i;
        end
    else % steps don't overlap so keep both
        stepind(end+1) = prevind;
        prevstep = thisstep;
        prevind = i;
    end
end
stepind(end+1) = prevind;

startpoint = startpoint(stepind);
endpoint = endpoint(stepind);
end %if isempty(startpoint)

end

