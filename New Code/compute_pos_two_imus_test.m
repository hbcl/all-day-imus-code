% Author: Lauro Ojeda, 2011-2015
function [left_walk_info,right_walk_info] = compute_pos_two_imus_test (leftWb,leftAb,rightWb,rightAb,PERIOD,walking_filter,periodic_section)

 		USE_KF = 1;
 		W_FF = 60;
 		A_FF = 2;
 		T_FF = floor(0.4/PERIOD);
 		FFL = [];
 		FFR = [];


MAX_T_FF = floor(0.8/PERIOD);


%left_walk_info = struct;
right_walk_info = struct;

%left_walk_info.FFstart = zeros(100,1);
%left_walk_info.FFend = zeros(100,1);
%left_walk_info.W = leftWb;
%left_walk_info.A = leftAb;
%left_walk_info.P = zeros(size(leftWb));
%left_walk_info.period = PERIOD;

left_walk_info = compute_pos2(leftWb,leftAb,PERIOD,walking_filter,periodic_section,USE_KF,W_FF,A_FF,T_FF,MAX_T_FF,FFL);
    
% % Inertial navigation mechanization
% 	N = size(leftWb,1);
% 	t = (1:N)*PERIOD;
% 	% Compute tilt based on accelerometer readings
% 	[accel_phi,accel_theta] = acc_tilt(leftAb); %accel_phi = roll angle accel_theta = pitch
% 	
% 	% Determine FFs
%     %[FFstart,FFend,stationary_periods] = foot_fall2(leftWb,leftAb,PERIOD,walking_filter,periodic_section,W_FF,A_FF,T_FF,MAX_T_FF);
% 
% GRAVITY=9.80297286843;
% Wm = (sum((leftWb.^2)')).^.5*180/pi/PERIOD;
% Am = (sum((leftAb.^2)')).^.5 - GRAVITY;
% 
% N=size(leftWb,1);
% 
% ps_inds = find(periodic_section > 0);
% if isempty(ps_inds)
%    disp('Warning: no walking section found.');
%    stationary_periods = zeros(N,1);
%    FFstart = [];
%    FFend = [];
% else
% 
% dif_ps = diff(periodic_section);
% ps_start = find(dif_ps==1)+1;
% ps_end = find(dif_ps==-1);
% if periodic_section(1) == 1
% ps_start = [1; ps_start];
% end
% if periodic_section(end) == 1
% ps_end = [ps_end; N];
% end
% 
% 
% FFcount = 0;
% 
% 
% % count the number of FFs to set the array size 
% %FFstart_all = cell(length(ps_start),1);
% %FFend_all = cell(length(ps_start),1);
% 
% FFstart_all = cell(10,1);
% FFend_all = cell(10,1);
% 
% 
% for j = 1:10%length(ps_start) % for each walking section
% 
% % Find low dynamic conditions
% low_motion=find(Wm(ps_start(j):ps_end(j))<W_FF & abs(Am(ps_start(j):ps_end(j)))<A_FF);
% 
% % Detect low dynamic segments separated at least T_FF
% diff_low_motion = diff(low_motion);
% valid_FF = find(diff_low_motion>T_FF);  % size = 1 x number of footfalls-1
% %start_FF = [];
% %end_FF = [];
% %if size(low_motion,2) > valid_FF
% start_FF = low_motion([1,valid_FF+1]);  % size = 1 x number of footfalls
% end_FF = low_motion([valid_FF,size(low_motion,2)]);
% %end
% % valid_FF = index numbers in diff_low_motion
% % start_FF = index numbers in stationary_periods with low motion separated
% % by at least T_FF
% 
% 
% % % if there's a half step at the beginning or end, include that too
% % valid_FF_small = find(diff_low_motion>(T_FF/2));
% % if (~isempty(valid_FF_small) && ~isempty(start_FF) && ~isempty(end_FF))
% %   if (low_motion(valid_FF_small(1)+1)>start_FF(1) && low_motion(valid_FF_small(1)+1)<end_FF(1))
% %     start_FF = [start_FF(1) low_motion(valid_FF_small(1)+1) start_FF(2:end)];
% %     end_FF = [low_motion(valid_FF_small(1))  end_FF];
% %   end
% %   if (low_motion(valid_FF_small(end)) > start_FF(end) && low_motion(valid_FF_small(end)) < end_FF(end))
% %    start_FF = [start_FF  low_motion(valid_FF_small(end)+1)];
% %    end_FF = [end_FF(1:length(end_FF)-1) low_motion(valid_FF_small(end)) end_FF(end)];
% %   end
% % end
% % Find best FF point
% FF_size = size(start_FF,2);
% %FF_index = [];
% 
% if FF_size == 0
%     FFcount = FFcount+1;
%     % this is needed because 1 gets subtracted at the end of the
%     % following for loop
% end
%     
% %if FF_size > 2
%    
% %    FF_lengths = end_FF(2:end-1) - start_FF(2:end-1);
% %    FFstart_all{j} = zeros(length(find(FF_lengths > MAX_T_FF))+FF_size,1);
% %    FFend_all{j} = zeros(length(find(FF_lengths > MAX_T_FF))+FF_size,1);
% %else
%     FFstart_all{j} = zeros(FF_size-1,1);
%     FFend_all{j} = zeros(FF_size-1,1);
% %end
% 
% %for i = 1:FF_size-1
% %   FF_cut1 = start_FF(i)+floor((end_FF(i)-start_FF(i))/2);
% %   FFstart_all{j}(i) = FF_cut1+ps_start(j)-1;
% %   FFend_all{j}(i) = FF_cut1+ps_start(j)-1;
% %   FFcount = FFcount+1;
% %end
% 
% 
% for i = 1:FF_size
% 
% 	FF_cut2 = -1; % only gets set if 2 FFs are needed
%     if (i == 1 && j == 1) % use end of low motion segment at beginning of experiment
%         FF_cut1 = end_FF(i);
%         % or take half the length of a normal footfall
%         % this was added for use during short walks in a lab
%         % if used for all-day analysis, it only affects first and last
%         % walking section
%         if (FF_size > 2)
%             FF_cut1 = end_FF(i) - floor(mean(end_FF(2:end-1)-start_FF(2:end-1))/2);
%         % but don't let it come before the start of the footfall section
%             if FF_cut1 < start_FF(i)
%               FF_cut1 = start_FF(i);   
%             end
%         end
%     elseif (i == FF_size && j == length(ps_start)) % use beginning of low motion segment at end
%         FF_cut1 = start_FF(i);
%          if (FF_size > 2)
%             FF_cut1 = start_FF(i) + floor(mean(end_FF(2:end-1)-start_FF(2:end-1))/2);
%             if FF_cut1 > end_FF(i)
%               FF_cut1 = end_FF(i);   
%             end
%          end
%     % end of section added for short walks experiment        
%     elseif (end_FF(i) - start_FF(i)) > MAX_T_FF % split into 2 FFs
%         if (i == 1)
%             FF_cut1 = end_FF(i)-floor(MAX_T_FF/2);
%         elseif (i == FF_size)
%             FF_cut1 = start_FF(i)+floor(MAX_T_FF/2);
%         else
%             FF_cut1 = start_FF(i)+floor(MAX_T_FF/2); 
%             FF_cut2 = end_FF(i)-floor(MAX_T_FF/2);
%         end
%     else % for most steps, use middle of low motion segment
%         FF_cut1 = start_FF(i)+floor((end_FF(i)-start_FF(i))/2);
%     end
%     
%     
%     
%     if FF_cut2 == -1
%         if i < FF_size % last FF is end but not start
%           FFstart_all{j}(i) = FF_cut1+ps_start(j)-1;
%           FFcount = FFcount + 1;
%         end
%         if i > 1 % first FF is start but not end
%           FFend_all{j}(i-1) = FF_cut1+ps_start(j)-1;
%         end
%     else % multiple FFs
%         if i < FF_size
%         FFstart_all{j}(i) = FF_cut2+ps_start(j)-1;
%         FFcount = FFcount + 1;
%         end
%         if i > 1
%         FFend_all{j}(i-1) = FF_cut1+ps_start(j)-1;
%         end
%     end
%     
% end
% 
% end 
% 
% 
% % Now find low dynamic conditions over the whole time period to set
% % stationary_periods
% low_motion=find(Wm<W_FF & abs(Am)<A_FF);
% N=size(leftWb,1);
% % stationary_periods is used by the tilt compensation KF 
% stationary_periods = zeros(N,1);
% stationary_periods(low_motion) = 1;
% 
% % put FFstart and FFend into arrays
% FFstart = zeros(FFcount,1);
% FFend = zeros(FFcount,1);
% ffind = 0;
% for j = 1:10%length(ps_start)
%    FFstart(ffind+1:ffind+length(FFstart_all{j})) = FFstart_all{j};
%    FFend(ffind+1:ffind+length(FFend_all{j})) = FFend_all{j};
%    ffind = ffind+length(FFstart_all{j});
% end
% 
% 
% end % if isempty(ps_inds)
% 
% 
% 
%     
%     
%     
% 
%     left_walk_info.FFstart = FFstart;
%     left_walk_info.FFend = FFend;
%     left_walk_info.is_walking = periodic_section;
% 
% 	% Initialize variables
% 	quaternion = zeros(N,4);
% 	An = zeros(N,3);
% 	Anz = zeros(N,3);
% 	% Initialize quaternions
% 	[quaternion(1,:),P,Q,R] = kf_tilt1(PERIOD); 
%     
%     last_footfall = 0;    
% 	for(i = 2:N)
% 		% Compute attitude using quaternion representation
% 		quaternion(i,:) = qua_est(leftWb(i,:),quaternion(i-1,:)); % qua_est estimates the next step based on previous quaterion and angular velocity
% 		% Transform accelerations from body to navigation frame
% 		rotation_matrix = qua2rot(quaternion(i,:)); % make rotation matrix based on current orientation
% 		%inv_quat = [quaternion(i,1) -quaternion(i,2) -quaternion(i,3) -quaternion(i,4)];
%         %this_accel = [0 A(i,1) A(i,2) A(i,3)];
%         An(i,:) = rotation_matrix*leftAb(i,:)'; % apply rotation to accelerations
%         %accel_rot = quaternion(i,:).*this_accel.*inv_quat;
%         %An(i,1) = accel_rot(2);
%         %An(i,2) = accel_rot(3);
%         %An(i,3) = accel_rot(4);
%         if(USE_KF)
% 			% Apply KF compensation on tilt
%             [quaternion(i,:),P,Q,R] = kf_tilt2(PERIOD,quaternion(i,:),accel_theta(i),accel_phi(i),stationary_periods(i),P,Q,R);
%         end
% 		% Apply Zero Velocity Updates
% 		% This script has not ben made into a function to make the code run faster
% 		% this call will update the Az variable wich contains the ZUPT updated navigation acceleration
%         if(i == 2)
% 	      last_footfall = 0;
%         end
% 		Anz = zupts_func(An,Anz,FFstart,FFend,i,last_footfall);
%         if(ismember(i,FFstart) || ismember(i,FFend))
%             last_footfall = i;
%         end
%     end
% 
% 	left_walk_info.Anz = Anz;
% 	left_walk_info.An = An;
% 	left_walk_info.A = leftAb;
% 	left_walk_info.W = leftWb;
% 	left_walk_info.quaternion = quaternion;
% 	
% 	% Plot attitude
% 	euler = qua2eul(quaternion);
% 	left_walk_info.euler = euler;
% 	
% 	% Set A/V/P to zero during bias drift estimation
% 	V = cumsum(Anz)*PERIOD;
% 	Vm = (sum((V(:,1:2).^2)')).^.5;
% 	left_walk_info.V = V;
% 	left_walk_info.Vm = Vm;

	






right_walk_info.FFstart = zeros(100,1);
right_walk_info.FFend = zeros(100,1);
right_walk_info.W = rightWb;
right_walk_info.A = rightAb;
right_walk_info.P = zeros(size(rightWb));
right_walk_info.period = PERIOD;

if walking_filter
%left_walk_info.is_walking = periodic_section;
right_walk_info.is_walking = periodic_section;
else
    %left_walk_info.is_walking = zeros(length(leftWb),1);
    right_walk_info.is_walking = zeros(length(rightWb),1);
end








