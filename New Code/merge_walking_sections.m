function periodic_section_new = merge_walking_sections(periodic_section,dif_samples)
  periodic_section_new = periodic_section;
  ps_inds = find(periodic_section);
  dif_ps_inds = diff(ps_inds);
  sec = find(dif_ps_inds > 1);
 
  
  for i=1:length(sec)
     if dif_ps_inds(sec(i)) <= dif_samples 
        periodic_section_new(ps_inds(sec(i)):ps_inds(sec(i)+1)) = 1;
     end
  end


end