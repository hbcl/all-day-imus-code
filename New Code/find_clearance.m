function [ value, point_index, time ,valid] = find_clearance( frwd, elev,  period)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    C_min=0.01;% the minimum clearance allowed
    G_min=0.001;% the minimum clearance to determine if the foot is on the ground
    P_off=0.4;% percent of ignored data not on just after left ground
    P_on=0.3; % percent of ignored data before attach ground
    T_default=0.45; % value used when peak can't be found in the time when it left ground. 
    valid=1; % tell if the clearance if found correctly,0 for foot not leaving the ground, 1 is correct, 2, is too many clearance found, 3 is no value find take a set time value
    
    a0=-elev(end)./frwd(end);
    distance=abs(a0*frwd(:)+elev(:))./sqrt(1+a0^2);
    
    index_off=find(distance>G_min,1);
    distance_fliped=fliplr(distance);
    index_on=length(distance)-find(distance_fliped>G_min,1);
    if(isempty(index_off))
        value=0;
        time=0;
        point_index=1;
        valid=0;
        return
    end
    
    frwd_length=frwd(index_on)-frwd(index_off);
    section_start=find(frwd> frwd_length*P_off,1);
    section_end=find(frwd> frwd_length*(1-P_on),1);
    
    smooth_section=section_start:section_end;
    
    [value,index]=min(distance(smooth_section));
    point_index=smooth_section(index);
    time=period*(point_index-index_off);
        
    
end

