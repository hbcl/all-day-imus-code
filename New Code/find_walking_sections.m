function periodic_section = find_walking_sections(leftW,leftA,rightW,rightA,waistW,waistA,PERIOD)

% Filter to look at only periods of walking
w_min = 300; % magnitude of w should be above this for walking
a_min = 20;

GRAVITY = 9.8;
leftWm = (sum(leftW.^2,2)).^.5*180/pi/PERIOD;
leftAm = (sum(leftA.^2,2)).^.5 - GRAVITY;

rightWm = (sum(rightW.^2,2)).^.5*180/pi/PERIOD;
rightAm = (sum(rightA.^2,2)).^.5 - GRAVITY;

window_size = 512;
sliding_step = 1/PERIOD; % step the window forward 1 second at a time
L = window_size * PERIOD; % length of sample in seconds
N=size(leftW,1);
periodic_section = zeros(N,1);
x_hz = (1/PERIOD) * (0:(window_size/2))/window_size;

if(~exist('waistA','var') || isempty(waistA))
    use_waist = 0;
else
    use_waist = 1;
end
    

for i=1:sliding_step:N-window_size
    if use_waist    
        if i > 3571/PERIOD && i < 3611/PERIOD
            one=1;
        end
            

    % determine the most sensitive axis
    sum1w = sum(abs(waistA(i:i+window_size-1,1)));
    sum2w = sum(abs(waistA(i:i+window_size-1,2)));
    sum3w = sum(abs(waistA(i:i+window_size-1,3)));
    [m, dimw] = max([sum1w sum2w sum3w]);
    if size(dimw) > 1 
       dimw = dimw(1);
    end
    windoww = waistA(i:i+window_size-1,dimw);
    yw = fft(windoww);
    y2w = abs(yw/window_size); % two-sided spectrum
    y1w = y2w(1:window_size/2+1); % single-sided spectrum
    y1w(2:end-1) = 2*y1w(2:end-1);
    x_hzw = (1/PERIOD) * (0:(window_size/2))/window_size;
    %plot(x_hzw,y1w);
    % look for frequency peak around 0.6 - 2 Hz
    %[mw, indexw] = max(y1w(2:end));    
    [peakwval peakwloc] = findpeaks(y1w);
    [negpeakwval negpeakwloc] = findpeaks(-1*y1w);
    [mw indexw] = max(peakwval);
    walkw = 0;

    if ~isempty(peakwval) && indexw < length(negpeakwval)
    if peakwval(indexw) > -2*negpeakwval(indexw) && peakwval(indexw) > -2*negpeakwval(indexw+1) && x_hzw(peakwloc(indexw)) > 1.2 && x_hzw(peakwloc(indexw)) <= 4
       walkw = 1;
    end
    end
    
    else
      walkw = 1;
    end % if use_waist
    
    windowlw1 = leftW(i:i+window_size-1,1);
    windowlw2 = leftW(i:i+window_size-1,2);
    windowlw3 = leftW(i:i+window_size-1,3);
    windowla1 = leftA(i:i+window_size-1,1);
    windowla2 = leftA(i:i+window_size-1,2);
    windowla3 = leftA(i:i+window_size-1,3);
    
    windowrw1 = rightW(i:i+window_size-1,1);
    windowrw2 = rightW(i:i+window_size-1,2);
    windowrw3 = rightW(i:i+window_size-1,3);
    windowra1 = rightA(i:i+window_size-1,1);
    windowra2 = rightA(i:i+window_size-1,2);
    windowra3 = rightA(i:i+window_size-1,3);

    ylw1 = fft_eval(windowlw1);
    ylw2 = fft_eval(windowlw2);
    ylw3 = fft_eval(windowlw3);
    yla1 = fft_eval(windowla1);
    yla2 = fft_eval(windowla2);
    yla3 = fft_eval(windowla3);
    
    yrw1 = fft_eval(windowrw1);
    yrw2 = fft_eval(windowrw2);
    yrw3 = fft_eval(windowrw3);
    yra1 = fft_eval(windowra1);
    yra2 = fft_eval(windowra2);
    yra3 = fft_eval(windowra3);
     
     
%    plot(x_hz,ylw1);
    % look for frequency peak around 0.6 - 2 Hz
    walklw1 = is_periodic(x_hz,ylw1);  
    walklw2 = is_periodic(x_hz,ylw2);
    walklw3 = is_periodic(x_hz,ylw3);
    walkla1 = is_periodic(x_hz,yla1);
    walkla2 = is_periodic(x_hz,yla2);
    walkla3 = is_periodic(x_hz,yla3);
    
    walkrw1 = is_periodic(x_hz,yrw1);  
    walkrw2 = is_periodic(x_hz,yrw2);
    walkrw3 = is_periodic(x_hz,yrw3);
    walkra1 = is_periodic(x_hz,yra1);
    walkra2 = is_periodic(x_hz,yra2);
    walkra3 = is_periodic(x_hz,yra3);
    
    % determine the most sensitive axis on left imu
    suml1 = sum(abs(leftW(i:i+window_size-1,1)));
    suml2 = sum(abs(leftW(i:i+window_size-1,2)));
    suml3 = sum(abs(leftW(i:i+window_size-1,3)));
    [m, diml] = max([suml1 suml2 suml3]);
    if size(diml) > 1 
       diml = diml(1);
    end
    if (diml == 1)
        walkl = walklw1;
    end
    if (diml == 2)
        walkl = walklw2;
    end
    if (diml == 3)
        walkl = walklw3;
    end
    
    
    % determine the most sensitive axis on right imu
    sumr1 = sum(abs(rightW(i:i+window_size-1,1)));
    sumr2 = sum(abs(rightW(i:i+window_size-1,2)));
    sumr3 = sum(abs(rightW(i:i+window_size-1,3)));
    [m, dimr] = max([sumr1 sumr2 sumr3]);
    if size(dimr) > 1 
       dimr = dimr(1);
    end
    if (dimr == 1)
        walkr = walkrw1;
    end
    if (dimr == 2)
        walkr = walkrw2;
    end
    if (dimr == 3)
        walkr = walkrw3;
    end    
    
    
    
    if walkl && walkr && walkw && ...
       max(leftAm(i:i+window_size-1,:)) > a_min && max(leftWm(i:i+window_size-1,:)) > w_min && ...
       max(rightAm(i:i+window_size-1,:)) > a_min && max(rightWm(i:i+window_size-1,:)) > w_min
       %x_hzw(indexw) > 1.2 && x_hzw(indexw) <=4 && mw > 16*min(y1w(2:17)) % waist part
        if i+sliding_step > N
            periodic_section(i:N) = 1;
        else
        periodic_section(i:i+window_size) = 1;
        end
    end

    
end

end




function y1 = fft_eval(window)
    window_size = length(window);
    y = fft(window);
    y2 = abs(y/window_size); % two-sided spectrum
    y1 = y2(1:window_size/2+1); % single-sided spectrum
    y1(2:end-1) = 2*y1(2:end-1);
end


function walk = is_periodic(x_hz,y1)
walk = 0;
[m, index] = max(y1);
if x_hz(index) > 0.6 && x_hz(index) <= 2 && m > 10*mean(y1)
  walk = 1;
end

end
