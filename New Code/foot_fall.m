% Author: Lauro Ojeda, 2003-2015
function [FFstart,FFend,stationary_periods] = foot_fall(W,A,PERIOD,walking_filter,periodic_section,W_FF,A_FF,T_FF)
PLOT_DETAILS = 1;
GRAVITY=9.8; %9.80297286843;
% FF determination settings
if(~exist('W_FF','var') | isempty(W_FF))
	W_FF = 60; % 60 Threshold rate used to determine FFs
end
if(~exist('A_FF','var') | isempty(A_FF))
	A_FF = 2; % 2 Threshold acceleration used to determine FFs
end
if(~exist('T_FF','var') | isempty(T_FF))
	T_FF = floor(0.4/PERIOD); % Minimum time allowed between two FFs
%else
%    T_FF = floor(T_FF/PERIOD); % convert Seconds into Samples
end
if(~exist('MAX_T_FF','var') | isempty(MAX_T_FF))
    MAX_T_FF = floor(0.8/PERIOD); %3 Maximum rest period allowed before splitting into 2 FFs
    % should be average length of time foot is on ground while walking
end
%    MAX_T_FF = floor(MAX_T_FF/PERIOD) ; % if input, it is in units of Seconds
%end;


Wm = (sum((W.^2)')).^.5*180/pi/PERIOD;
Am = (sum((A.^2)')).^.5 - GRAVITY;

N=size(W,1);

if walking_filter
ps_inds = find(periodic_section > 0);
if isempty(ps_inds)
   disp('Warning: no walking section found.');
   stationary_periods = zeros(N,1);
   FFstart = [];
   FFend = [];
   return;
end

dif_ps_inds = diff(ps_inds);
if dif_ps_inds(1) == 1 
   ps_start(1) = ps_inds(1);
   walking = 1;
else
    walking = 0;
end

count = 1;
for i=1:length(dif_ps_inds)
    if (walking)
        if (dif_ps_inds(i) ~= 1) % not walking
            ps_end(count) = ps_inds(i); % ps_inds(i+1)?
            walking = 0;
            count = count+1;
        end
    else
        if (dif_ps_inds(i) == 1) % start new walking section
            ps_start(count) = ps_inds(i);
            walking = 1;
        end
        
    end
end
if (walking)  
ps_end(count) = ps_inds(length(dif_ps_inds));    
end

else % walking_filter = 0
    ps_start = 1;
    ps_end = N;

end


FFcount = 1;

FFstart = [];
FFend = [];
FFcount = 1;
FFstart = [];
FFend = [];

for j = 1:length(ps_start) % for each walking section
N = ps_end(j) - ps_start(j);



% Find low dynamic conditions
low_motion=find(Wm(ps_start(j):ps_end(j))<W_FF & abs(Am(ps_start(j):ps_end(j)))<A_FF);
% stationary_periods is used by the tilt compensation KF 
%stationary_periods = zeros(N,1);
%stationary_periods(low_motion) = 1;

% if there's a low motion segment of length 1, skip it
%i = 2;
%while i < length(low_motion)
%  if low_motion(i)-1 > low_motion(i-1) && low_motion(i)+1 < low_motion(i+1)
%    low_motion(i) = [];
%  else
%    i = i+1;
%  end
%end

% Detect low dynamic segments separated at least T_FF
diff_low_motion = diff(low_motion);
valid_FF = find(diff_low_motion>T_FF);  % size = 1 x number of footfalls-1
start_FF = [];
end_FF = [];
if size(low_motion,2) > valid_FF
start_FF = low_motion([1,valid_FF+1]);  % size = 1 x number of footfalls
end_FF = low_motion([valid_FF,size(low_motion,2)]);
end
% valid_FF = index numbers in diff_low_motion
% start_FF = index numbers in stationary_periods with low motion separated
% by at least T_FF


% if there's a half step at the beginning or end, include that too
valid_FF_small = find(diff_low_motion>(T_FF/2));
if (~isempty(valid_FF_small) && ~isempty(start_FF) && ~isempty(end_FF))
  if (low_motion(valid_FF_small(1)+1)>start_FF(1) && low_motion(valid_FF_small(1)+1)<end_FF(1))
    start_FF = [start_FF(1) low_motion(valid_FF_small(1)+1) start_FF(2:end)];
    end_FF = [low_motion(valid_FF_small(1))  end_FF];
  end
  if (low_motion(valid_FF_small(end)) > start_FF(end) && low_motion(valid_FF_small(end)) < end_FF(end))
   start_FF = [start_FF  low_motion(valid_FF_small(end)+1)];
   end_FF = [end_FF(1:length(end_FF)-1) low_motion(valid_FF_small(end)) end_FF(end)];
  end
end
% Find best FF point
FF_size = size(start_FF,2);
FF_index = [];

if FF_size == 0
    FFcount = FFcount+1;
    % this is needed because 1 gets subtracted at the end of the
    % following for loop
end

for i = 1:FF_size
	%Wm_cut = Wm(start_FF(i):end_FF(i));
	%N_cut = size(Wm_cut,2);
	%R_seg = floor(N_cut/MAX_T_FF);
	% Segment long low dynamic intervals to find more than one FF
    % forces a time limit for steps
%	Wm_seg = reshape(Wm_cut(1:R_seg*MAX_T_FF),MAX_T_FF,R_seg);
%	[aux,min_idx] = min(Wm_seg);
%	[aux,min_idx2] = min(Wm_cut(MAX_T_FF*R_seg+1:N_cut));
%	min_idx = [min_idx,min_idx2];
%	FF_cut = min_idx+((0:size(min_idx,2)-1)*MAX_T_FF)+start_FF(i);


       %[aux,min_idx] = min(Wm_cut); % use minimum of low motion segment
       %FF_cut = min_idx+start_FF(i);
	
    if (i == 1 && j == 1) % use end of low motion segment at beginning of experiment
        FF_cut = end_FF(i);
        % or take half the length of a normal footfall
        % this was added for use during short walks in a lab
        % if used for all-day analysis, it only affects first and last
        % walking section
        if (FF_size > 2)
            FF_cut = end_FF(i) - floor(mean(end_FF(2:end-1)-start_FF(2:end-1))/2);
        % but don't let it come before the start of the footfall section
            if FF_cut < start_FF(i)
              FF_cut = start_FF(i);   
            end
        end
    elseif (i == FF_size && j == length(ps_start)) % use beginning of low motion segment at end
        FF_cut = start_FF(i);
         if (FF_size > 2)
            FF_cut = start_FF(i) + floor(mean(end_FF(2:end-1)-start_FF(2:end-1))/2);
            if FF_cut > end_FF(i)
              FF_cut = end_FF(i);   
            end
         end
    % end of section added for short walks experiment        
    elseif (end_FF(i) - start_FF(i)) > MAX_T_FF % split into 2 FFs
        if (i == 1)
            FF_cut = end_FF(i)-floor(MAX_T_FF/2);
        elseif (i == FF_size)
            FF_cut = start_FF(i)+floor(MAX_T_FF/2);
        else
            FF_cut = [start_FF(i)+floor(MAX_T_FF/2); end_FF(i)-floor(MAX_T_FF/2)];
        end
    else % for most steps, use middle of low motion segment
        FF_cut = start_FF(i)+floor((end_FF(i)-start_FF(i))/2);
    end
    
    
    
    if length(FF_cut) == 1
        try
        FFstart(FFcount) = FF_cut+ps_start(j)-1;
        catch
            disp('bad')
        end
        if i > 1 % first FF is start but not end
        FFend(FFcount-1) = FF_cut+ps_start(j)-1;
        end
        FFcount = FFcount + 1;
    else % multiple FFs
        FFstart(FFcount) = FF_cut(2)+ps_start(j)-1;
        FFend(FFcount-1) = FF_cut(1)+ps_start(j)-1;
        FFcount = FFcount + 1;
    end
    
    
    
    
    %FF_index = [FF_index;FF_cut'];
        
    %FF_index(FF_index > N) = N;
end
if length(FFstart) > length(FFend)
FFstart(end) = []; % last FF cannot be start of step
end
FFcount = FFcount-1;
% Force the last sample to be a FF
%FF_index = [FF_index-1;N];
% Create FF boolean vector
%if isempty(FF_index)
  %disp('No footfalls found');  
%else
%  if ~isempty(find(FF_index <= 0,1))
%     disp('Something bad happened'); 
%  end
%  FF = zeros(N,1); 
%  FF(FF_index) = 1; 

% Make FF a logical variable
%FF = FF == 1;

%running = 0;
%[pks1, ~] = findpeaks(A(:,1),'MinPeakDistance',T_FF);
%[pks2, ~] = findpeaks(A(:,2),'MinPeakDistance',T_FF);
%[pks3, ~] = findpeaks(A(:,3),'MinPeakDistance',T_FF);
%if mean(pks1) > 100 || mean(pks2) > 100 || mean(pks3) > 100
%    running = 1; 
%    disp(running);
%end

%if running 
%  [~, FF_index] = findpeaks(-Wm,'MinPeakDistance',T_FF); 
%  FF_index = FF_index';
%end

%for k = 1:size(FF_index,1)-1
%    FFstart(FFcount) = FF_index(k)+ps_start(j)-1;
%    FFend(FFcount) = FF_index(k+1)+ps_start(j)-1;
%    FFcount = FFcount + 1;
%end
end 


% Now find low dynamic conditions over the whole time period to set
% stationary_periods
low_motion=find(Wm<W_FF & abs(Am)<A_FF);
N=size(W,1);
% stationary_periods is used by the tilt compensation KF 
stationary_periods = zeros(N,1);
stationary_periods(low_motion) = 1;

%t=(1:N)*PERIOD;
% Make plots
%clear sh;

%figure;

%if(PLOT_DETAILS)
%subplot(2,1,1);
%hold on;
%plot(t(low_motion),Wm(low_motion),'.y');
%plot(t(start_FF),Wm(start_FF),'og');
%plot(t(end_FF),Wm(end_FF),'or');
%hold off;
%subplot(2,1,2);
%hold on;
%plot(t(low_motion),Am(low_motion),'.y');
%plot(t(start_FF),Am(start_FF),'og');
%plot(t(end_FF),Am(end_FF),'or');
%hold off;
%end;

%sh(1) = subplot(2,1,1);
%plot(t,Wm);grid on;ylabel('W [deg/sec]');
%hold on;
%title('Foot fall detection');
%plot(t(FF),Wm(FF),'*k');
%legend('Signal','Foot-fall');
%hold off;

%sh(2) = subplot(2,1,2);plot(t,Am);
%grid on;ylabel('A [m/sec^2]');
%hold on;
%plot(t(FF),Am(FF),'*k');
%xlabel('t [sec]');
%linkaxes(sh,'x');
%hold off;

end
