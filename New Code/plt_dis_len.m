function [ distance ] = plt_dis_len( left_strides,right_strides )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
segment = 500;
num = max(length(left_strides.length),length(right_strides.length));
distance(1)= (right_strides.length(1)+left_strides.length(1))/2;
for i = 2:num
   if i > length(left_strides.length)
        distance(i) = right_strides.length(i)+distance(i-1);
   elseif i > length(right_strides.length)
        distance(i) = left_strides.length(i)+distance(i-1);
   else distance(i) = (right_strides.length(i)+left_strides.length(i))/2+distance(i-1);
   end
   if (length(distance)==length(right_strides.length))
        figure; plot(distance,right_strides.length);
        xlabel('distance (m)');ylabel('right stride length (m)');
   elseif (length(distance)==length(left_strides.length))
        figure; plot(distance,left_strides.length);
        xlabel('distance (m)');ylabel('left stride length (m)');
   end
end

ind = ceil(distance(num)/segment);
for j = 1:ind
   index(j) = find((distance == (j*segment-min(abs(j*segment-distance))))|(distance == (min(abs(j*segment-distance))+j*segment)));   
end


if(length(left_strides.length)<index(1))
    left_len(1) = mean(left_strides.length);
    left_len_err(1) = std(left_strides.length);
else
    left_len(1) = mean(left_strides.length(1:index(1)));
    left_len_err(1) = std(left_strides.length(1:index(1)));
end
if(length(right_strides.length)<index(1))
    right_len(1) = mean(right_strides.length);
    right_len_err(1) = std(right_strides.length);
else
    right_len(1) = mean(right_strides.length(1:index(1)));
    right_len_err(1) = std(right_strides.length(1:index(1)));
end

for j = 2:ind
    if (length(left_strides.length)<index(j))
         left_len(j)=mean(left_strides.length(index(j-1):length(left_strides.length)));
         left_len_err(j)=std(left_strides.length(index(j-1):length(left_strides.length)));
         break;
    else left_len(j) = mean(left_strides.length(index(j-1):index(j)));
         left_len_err(j) = std(left_strides.length(index(j-1):index(j)));
    end
end    
    
for j = 2:ind
   if (length(right_strides.length)<index(j))
         right_len(j)=mean(right_strides.length(index(j-1):length(right_strides.length)));
         right_len_err(j)=std(right_strides.length(index(j-1):length(right_strides.length)));
         break;
   else  right_len(j) = mean(right_strides.length(index(j-1):index(j)));
         right_len_err(j) = std(right_strides.length(index(j-1):index(j)));
   end
end

for j = 1:ind
    if(length(left_len)<j)
        left_len(j) = 0;
        left_len_err(j) = 0;
    elseif (length(right_len)<j)
        right_len(j) = 0;
        right_len_err(j) = 0;
    end
end
 len = [left_len' right_len'];
 len_err = [left_len_err' right_len_err'];
 barweb(len, len_err);
 xlabel('distance (*500 m)');
 ylabel('stride length (m)');
 legend('left foot', 'right_foot')
end

