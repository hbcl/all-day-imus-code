function [dist] = find_dist(leftP,rightP,rotation)
rightP=rotateP(rightP,rotation,'z');

dist = 0;
for i=1:size(leftP,1)
    dist = dist+norm(rightP(i,:)-leftP(i,:));
end


end