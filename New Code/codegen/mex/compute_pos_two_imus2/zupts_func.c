/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * zupts_func.c
 *
 * Code generation for function 'zupts_func'
 *
 */

/* Include files */
#include <math.h>
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "compute_pos_two_imus2.h"
#include "zupts_func.h"
#include "compute_pos_two_imus2_emxutil.h"
#include "sum.h"
#include "eml_int_forloop_overflow_check.h"
#include "compute_pos_two_imus2_data.h"

/* Variable Definitions */
static emlrtRSInfo qc_emlrtRSI = { 9,  /* lineNo */
  "zupts_func",                        /* fcnName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/zupts_func.m"/* pathName */
};

static emlrtRSInfo rc_emlrtRSI = { 10, /* lineNo */
  "zupts_func",                        /* fcnName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/zupts_func.m"/* pathName */
};

static emlrtRSInfo wc_emlrtRSI = { 25, /* lineNo */
  "colon",                             /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/ops/colon.m"/* pathName */
};

static emlrtRSInfo xc_emlrtRSI = { 98, /* lineNo */
  "colon",                             /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/ops/colon.m"/* pathName */
};

static emlrtRTEInfo jc_emlrtRTEI = { 10,/* lineNo */
  2,                                   /* colNo */
  "zupts_func",                        /* fName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/zupts_func.m"/* pName */
};

static emlrtRTEInfo kc_emlrtRTEI = { 17,/* lineNo */
  23,                                  /* colNo */
  "zupts_func",                        /* fName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/zupts_func.m"/* pName */
};

static emlrtRTEInfo lc_emlrtRTEI = { 98,/* lineNo */
  9,                                   /* colNo */
  "colon",                             /* fName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/ops/colon.m"/* pName */
};

static emlrtRTEInfo mc_emlrtRTEI = { 21,/* lineNo */
  41,                                  /* colNo */
  "zupts_func",                        /* fName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/zupts_func.m"/* pName */
};

static emlrtRTEInfo nc_emlrtRTEI = { 21,/* lineNo */
  25,                                  /* colNo */
  "zupts_func",                        /* fName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/zupts_func.m"/* pName */
};

static emlrtRTEInfo oc_emlrtRTEI = { 21,/* lineNo */
  6,                                   /* colNo */
  "zupts_func",                        /* fName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/zupts_func.m"/* pName */
};

static emlrtRTEInfo pc_emlrtRTEI = { 1,/* lineNo */
  16,                                  /* colNo */
  "zupts_func",                        /* fName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/zupts_func.m"/* pName */
};

static emlrtECInfo h_emlrtECI = { -1,  /* nDims */
  21,                                  /* lineNo */
  2,                                   /* colNo */
  "zupts_func",                        /* fName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/zupts_func.m"/* pName */
};

static emlrtECInfo i_emlrtECI = { 2,   /* nDims */
  21,                                  /* lineNo */
  22,                                  /* colNo */
  "zupts_func",                        /* fName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/zupts_func.m"/* pName */
};

static emlrtBCInfo tc_emlrtBCI = { 1,  /* iFirst */
  3621350,                             /* iLast */
  17,                                  /* lineNo */
  26,                                  /* colNo */
  "An",                                /* aName */
  "zupts_func",                        /* fName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/zupts_func.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo e_emlrtDCI = { 17,  /* lineNo */
  26,                                  /* colNo */
  "zupts_func",                        /* fName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/zupts_func.m",/* pName */
  1                                    /* checkKind */
};

/* Function Definitions */
void zupts_func(const emlrtStack *sp, const real_T An[10864050], real_T Anz
                [10864050], const emxArray_real_T *FFstart, const
                emxArray_real_T *FFend, real_T i, real_T last_footfall)
{
  int32_T ns;
  boolean_T tf;
  int32_T k;
  boolean_T exitg1;
  real_T apnd;
  emxArray_real_T *step_range;
  emxArray_int32_T *r6;
  real_T absxk;
  emxArray_real_T *b_An;
  emxArray_real_T *b_step_range;
  boolean_T guard1 = false;
  int32_T exponent;
  boolean_T p;
  real_T cdiff;
  int32_T b_exponent;
  real_T velocity_error[3];
  int32_T c_step_range[2];
  int32_T c_An[2];
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  emlrtMEXProfilingFunctionEntry(zupts_func_complete_name, isMexOutdated);

  /*  Author: Lauro Ojeda, 2003-2015 */
  /*  Zero velocity Updates on accelerometer values */
  /*  It assumes that the error is linear over the step interval */
  /* Applies ZUPTs on foot falls only */
  emlrtMEXProfilingStatement(1, isMexOutdated);
  st.site = &qc_emlrtRSI;
  b_st.site = &sc_emlrtRSI;
  ns = FFstart->size[0] * FFstart->size[1];
  tf = false;
  c_st.site = &uc_emlrtRSI;
  if ((1 <= ns) && (ns > 2147483646)) {
    d_st.site = &w_emlrtRSI;
    check_forloop_overflow_error(&d_st);
  }

  k = 0;
  exitg1 = false;
  while ((!exitg1) && (k <= ns - 1)) {
    c_st.site = &tc_emlrtRSI;
    apnd = FFstart->data[k];
    absxk = muDoubleScalarAbs(apnd / 2.0);
    if ((!muDoubleScalarIsInf(absxk)) && (!muDoubleScalarIsNaN(absxk))) {
      if (absxk <= 2.2250738585072014E-308) {
        absxk = 4.94065645841247E-324;
      } else {
        frexp(absxk, &exponent);
        absxk = ldexp(1.0, exponent - 53);
      }
    } else {
      absxk = rtNaN;
    }

    if ((muDoubleScalarAbs(FFstart->data[k] - i) < absxk) ||
        (muDoubleScalarIsInf(i) && muDoubleScalarIsInf(apnd) && (FFstart->data[k]
          > 0.0))) {
      p = true;
    } else {
      p = false;
    }

    if (p) {
      tf = true;
      exitg1 = true;
    } else {
      k++;
    }
  }

  emxInit_real_T(sp, &step_range, 2, &jc_emlrtRTEI, true);
  emxInit_int32_T(sp, &r6, 1, &pc_emlrtRTEI, true);
  emxInit_real_T(sp, &b_An, 2, &kc_emlrtRTEI, true);
  emxInit_real_T(sp, &b_step_range, 1, &nc_emlrtRTEI, true);
  guard1 = false;
  if (tf) {
    guard1 = true;
  } else {
    st.site = &qc_emlrtRSI;
    b_st.site = &sc_emlrtRSI;
    ns = FFend->size[0] * FFend->size[1];
    tf = false;
    c_st.site = &uc_emlrtRSI;
    if ((1 <= ns) && (ns > 2147483646)) {
      d_st.site = &w_emlrtRSI;
      check_forloop_overflow_error(&d_st);
    }

    k = 0;
    exitg1 = false;
    while ((!exitg1) && (k <= ns - 1)) {
      c_st.site = &tc_emlrtRSI;
      apnd = FFend->data[k];
      absxk = muDoubleScalarAbs(apnd / 2.0);
      if ((!muDoubleScalarIsInf(absxk)) && (!muDoubleScalarIsNaN(absxk))) {
        if (absxk <= 2.2250738585072014E-308) {
          absxk = 4.94065645841247E-324;
        } else {
          frexp(absxk, &b_exponent);
          absxk = ldexp(1.0, b_exponent - 53);
        }
      } else {
        absxk = rtNaN;
      }

      if ((muDoubleScalarAbs(FFend->data[k] - i) < absxk) ||
          (muDoubleScalarIsInf(i) && muDoubleScalarIsInf(apnd) && (FFend->data[k]
            > 0.0))) {
        p = true;
      } else {
        p = false;
      }

      if (p) {
        tf = true;
        exitg1 = true;
      } else {
        k++;
      }
    }

    if (tf) {
      guard1 = true;
    }
  }

  if (guard1) {
    emlrtMEXProfilingStatement(2, isMexOutdated);
    emlrtMEXProfilingStatement(0, isMexOutdated);
    st.site = &rc_emlrtRSI;
    b_st.site = &wc_emlrtRSI;
    if (muDoubleScalarIsNaN(last_footfall + 1.0) || muDoubleScalarIsNaN(i)) {
      k = step_range->size[0] * step_range->size[1];
      step_range->size[0] = 1;
      step_range->size[1] = 1;
      emxEnsureCapacity_real_T(&b_st, step_range, k, &jc_emlrtRTEI);
      step_range->data[0] = rtNaN;
    } else if (i < last_footfall + 1.0) {
      step_range->size[0] = 1;
      step_range->size[1] = 0;
    } else if ((muDoubleScalarIsInf(last_footfall + 1.0) || muDoubleScalarIsInf
                (i)) && (last_footfall + 1.0 == i)) {
      k = step_range->size[0] * step_range->size[1];
      step_range->size[0] = 1;
      step_range->size[1] = 1;
      emxEnsureCapacity_real_T(&b_st, step_range, k, &jc_emlrtRTEI);
      step_range->data[0] = rtNaN;
    } else if (muDoubleScalarFloor(last_footfall + 1.0) == last_footfall + 1.0)
    {
      k = step_range->size[0] * step_range->size[1];
      step_range->size[0] = 1;
      ns = (int32_T)muDoubleScalarFloor(i - (last_footfall + 1.0));
      step_range->size[1] = ns + 1;
      emxEnsureCapacity_real_T(&b_st, step_range, k, &jc_emlrtRTEI);
      for (k = 0; k <= ns; k++) {
        step_range->data[k] = (last_footfall + 1.0) + (real_T)k;
      }
    } else {
      c_st.site = &xc_emlrtRSI;
      absxk = muDoubleScalarFloor((i - (last_footfall + 1.0)) + 0.5);
      apnd = (last_footfall + 1.0) + absxk;
      cdiff = apnd - i;
      if (muDoubleScalarAbs(cdiff) < 4.4408920985006262E-16 * muDoubleScalarMax
          (muDoubleScalarAbs(last_footfall + 1.0), muDoubleScalarAbs(i))) {
        absxk++;
        apnd = i;
      } else if (cdiff > 0.0) {
        apnd = (last_footfall + 1.0) + (absxk - 1.0);
      } else {
        absxk++;
      }

      if (absxk >= 0.0) {
        exponent = (int32_T)absxk;
      } else {
        exponent = 0;
      }

      k = step_range->size[0] * step_range->size[1];
      step_range->size[0] = 1;
      step_range->size[1] = exponent;
      emxEnsureCapacity_real_T(&c_st, step_range, k, &lc_emlrtRTEI);
      if (exponent > 0) {
        step_range->data[0] = last_footfall + 1.0;
        if (exponent > 1) {
          step_range->data[exponent - 1] = apnd;
          ns = (exponent - 1) / 2;
          for (k = 0; k <= ns - 2; k++) {
            step_range->data[1 + k] = (last_footfall + 1.0) + (1.0 + (real_T)k);
            step_range->data[(exponent - k) - 2] = apnd - (1.0 + (real_T)k);
          }

          if (ns << 1 == exponent - 1) {
            step_range->data[ns] = ((last_footfall + 1.0) + apnd) / 2.0;
          } else {
            step_range->data[ns] = (last_footfall + 1.0) + (real_T)ns;
            step_range->data[ns + 1] = apnd - (real_T)ns;
          }
        }
      }
    }

    /* Skip short duration FF */
    emlrtMEXProfilingStatement(3, isMexOutdated);
    if (step_range->size[1] < 2) {
      emlrtMEXProfilingStatement(4, isMexOutdated);
    } else {
      emlrtMEXProfilingStatement(6, isMexOutdated);

      /* Compute final error */
      emlrtMEXProfilingStatement(7, isMexOutdated);
      k = b_An->size[0] * b_An->size[1];
      b_An->size[0] = step_range->size[1];
      b_An->size[1] = 3;
      emxEnsureCapacity_real_T(sp, b_An, k, &kc_emlrtRTEI);
      for (k = 0; k < 3; k++) {
        ns = step_range->size[1];
        for (exponent = 0; exponent < ns; exponent++) {
          absxk = step_range->data[exponent];
          if (absxk != (int32_T)muDoubleScalarFloor(absxk)) {
            emlrtIntegerCheckR2012b(absxk, &e_emlrtDCI, sp);
          }

          b_exponent = (int32_T)absxk;
          if ((b_exponent < 1) || (b_exponent > 3621350)) {
            emlrtDynamicBoundsCheckR2012b(b_exponent, 1, 3621350, &tc_emlrtBCI,
              sp);
          }

          b_An->data[exponent + b_An->size[0] * k] = An[(b_exponent + 3621350 *
            k) - 1];
        }
      }

      b_sum(b_An, velocity_error);

      /* Compute the accelerometer error assuming it is linear */
      emlrtMEXProfilingStatement(8, isMexOutdated);

      /* Apply error corrections in accelerations */
      emlrtMEXProfilingStatement(9, isMexOutdated);
      exponent = step_range->size[1];
      ns = step_range->size[1];
      velocity_error[0] /= (real_T)ns;
      velocity_error[1] /= (real_T)ns;
      velocity_error[2] /= (real_T)ns;
      k = b_An->size[0] * b_An->size[1];
      b_An->size[0] = exponent;
      b_An->size[1] = 3;
      emxEnsureCapacity_real_T(sp, b_An, k, &mc_emlrtRTEI);
      for (k = 0; k < exponent; k++) {
        b_An->data[k] = velocity_error[0];
        b_An->data[k + b_An->size[0]] = velocity_error[1];
        b_An->data[k + (b_An->size[0] << 1)] = velocity_error[2];
      }

      k = b_step_range->size[0];
      b_step_range->size[0] = step_range->size[1];
      emxEnsureCapacity_real_T(sp, b_step_range, k, &nc_emlrtRTEI);
      ns = step_range->size[1];
      for (k = 0; k < ns; k++) {
        b_step_range->data[k] = step_range->data[k];
      }

      c_step_range[0] = b_step_range->size[0];
      c_step_range[1] = 3;
      c_An[0] = b_An->size[0];
      c_An[1] = b_An->size[1];
      if ((c_step_range[0] != c_An[0]) || (3 != c_An[1])) {
        emlrtSizeEqCheckNDR2012b(&c_step_range[0], &c_An[0], &i_emlrtECI, sp);
      }

      k = r6->size[0];
      r6->size[0] = step_range->size[1];
      emxEnsureCapacity_int32_T(sp, r6, k, &oc_emlrtRTEI);
      ns = step_range->size[1];
      for (k = 0; k < ns; k++) {
        r6->data[k] = (int32_T)step_range->data[k] - 1;
      }

      k = b_step_range->size[0];
      b_step_range->size[0] = step_range->size[1];
      emxEnsureCapacity_real_T(sp, b_step_range, k, &nc_emlrtRTEI);
      ns = step_range->size[1];
      for (k = 0; k < ns; k++) {
        b_step_range->data[k] = step_range->data[k];
      }

      c_An[0] = r6->size[0];
      c_An[1] = 3;
      c_step_range[0] = b_step_range->size[0];
      c_step_range[1] = 3;
      emlrtSubAssignSizeCheckR2012b(&c_An[0], 2, &c_step_range[0], 2,
        &h_emlrtECI, sp);
      for (k = 0; k < 3; k++) {
        ns = step_range->size[1];
        for (exponent = 0; exponent < ns; exponent++) {
          Anz[r6->data[exponent] + 3621350 * k] = An[((int32_T)step_range->
            data[exponent] + 3621350 * k) - 1] - b_An->data[exponent +
            b_An->size[0] * k];
        }
      }

      emlrtMEXProfilingStatement(10, isMexOutdated);
    }
  }

  emxFree_real_T(&b_step_range);
  emxFree_real_T(&b_An);
  emxFree_int32_T(&r6);
  emxFree_real_T(&step_range);
  emlrtMEXProfilingStatement(11, isMexOutdated);
  emlrtMEXProfilingFunctionExit(isMexOutdated);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

/* End of code generation (zupts_func.c) */
