/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * diff.c
 *
 * Code generation for function 'diff'
 *
 */

/* Include files */
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "compute_pos_two_imus2.h"
#include "diff.h"
#include "compute_pos_two_imus2_emxutil.h"

/* Variable Definitions */
static emlrtRTEInfo hc_emlrtRTEI = { 1,/* lineNo */
  14,                                  /* colNo */
  "diff",                              /* fName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/datafun/diff.m"/* pName */
};

static emlrtRTEInfo vc_emlrtRTEI = { 51,/* lineNo */
  19,                                  /* colNo */
  "diff",                              /* fName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/datafun/diff.m"/* pName */
};

/* Function Definitions */
void b_diff(const emlrtStack *sp, const emxArray_real_T *x, emxArray_real_T *y)
{
  int32_T dimSize;
  int32_T i4;
  boolean_T b0;
  int32_T ySize_idx_1;
  int32_T iyLead;
  real_T work_data_idx_0;
  int32_T m;
  real_T tmp1;
  real_T tmp2;
  dimSize = x->size[1];
  if (x->size[1] == 0) {
    y->size[0] = 1;
    y->size[1] = 0;
  } else {
    i4 = x->size[1] - 1;
    if (muIntScalarMin_sint32(i4, 1) < 1) {
      y->size[0] = 1;
      y->size[1] = 0;
    } else {
      b0 = (x->size[1] != 1);
      if (!b0) {
        emlrtErrorWithMessageIdR2018a(sp, &vc_emlrtRTEI,
          "Coder:toolbox:autoDimIncompatibility",
          "Coder:toolbox:autoDimIncompatibility", 0);
      }

      ySize_idx_1 = x->size[1] - 1;
      iyLead = y->size[0] * y->size[1];
      y->size[0] = 1;
      y->size[1] = ySize_idx_1;
      emxEnsureCapacity_real_T(sp, y, iyLead, &hc_emlrtRTEI);
      if (y->size[1] != 0) {
        ySize_idx_1 = 1;
        iyLead = 0;
        work_data_idx_0 = x->data[0];
        for (m = 2; m <= dimSize; m++) {
          tmp1 = x->data[ySize_idx_1];
          tmp2 = work_data_idx_0;
          work_data_idx_0 = tmp1;
          tmp1 -= tmp2;
          ySize_idx_1++;
          y->data[iyLead] = tmp1;
          iyLead++;
        }
      }
    }
  }
}

void diff(const real_T x[3621350], real_T y[3621349])
{
  int32_T ixLead;
  int32_T iyLead;
  real_T work;
  int32_T m;
  real_T tmp2;
  ixLead = 1;
  iyLead = 0;
  work = x[0];
  for (m = 0; m < 3621349; m++) {
    tmp2 = work;
    work = x[ixLead];
    y[iyLead] = x[ixLead] - tmp2;
    ixLead++;
    iyLead++;
  }
}

/* End of code generation (diff.c) */
