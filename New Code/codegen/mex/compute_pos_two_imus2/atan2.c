/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * atan2.c
 *
 * Code generation for function 'atan2'
 *
 */

/* Include files */
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "compute_pos_two_imus2.h"
#include "atan2.h"

/* Function Definitions */
void b_atan2(const real_T y[3621350], const real_T x[3621350], real_T r[3621350])
{
  int32_T k;
  for (k = 0; k < 3621350; k++) {
    r[k] = muDoubleScalarAtan2(y[k], x[k]);
  }
}

/* End of code generation (atan2.c) */
