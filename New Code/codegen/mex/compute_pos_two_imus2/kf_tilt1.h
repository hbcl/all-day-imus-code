/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * kf_tilt1.h
 *
 * Code generation for function 'kf_tilt1'
 *
 */

#ifndef KF_TILT1_H
#define KF_TILT1_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "compute_pos_two_imus2_types.h"

/* Function Declarations */
extern void kf_tilt1(real_T PERIOD, real_T *Q, real_T *R);

#endif

/* End of code generation (kf_tilt1.h) */
