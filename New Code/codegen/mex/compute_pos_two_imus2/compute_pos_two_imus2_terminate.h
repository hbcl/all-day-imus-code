/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * compute_pos_two_imus2_terminate.h
 *
 * Code generation for function 'compute_pos_two_imus2_terminate'
 *
 */

#ifndef COMPUTE_POS_TWO_IMUS2_TERMINATE_H
#define COMPUTE_POS_TWO_IMUS2_TERMINATE_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "compute_pos_two_imus2_types.h"

/* Function Declarations */
extern void compute_pos_two_imus2_atexit(void);
extern void compute_pos_two_imus2_terminate(void);

#endif

/* End of code generation (compute_pos_two_imus2_terminate.h) */
