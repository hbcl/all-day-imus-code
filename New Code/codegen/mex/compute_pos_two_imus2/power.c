/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * power.c
 *
 * Code generation for function 'power'
 *
 */

/* Include files */
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "compute_pos_two_imus2.h"
#include "power.h"
#include "compute_pos_two_imus2_data.h"

/* Variable Definitions */
static emlrtRSInfo ab_emlrtRSI = { 65, /* lineNo */
  "applyBinaryScalarFunction",         /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/eml/+coder/+internal/applyBinaryScalarFunction.m"/* pathName */
};

static emlrtRSInfo bb_emlrtRSI = { 188,/* lineNo */
  "flatIter",                          /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/eml/+coder/+internal/applyBinaryScalarFunction.m"/* pathName */
};

static emlrtRTEInfo rc_emlrtRTEI = { 67,/* lineNo */
  5,                                   /* colNo */
  "fltpower",                          /* fName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/ops/power.m"/* pName */
};

/* Function Declarations */
static boolean_T fltpower_domain_error(const real_T a[3621350]);

/* Function Definitions */
static boolean_T fltpower_domain_error(const real_T a[3621350])
{
  boolean_T p;
  int32_T k;
  p = false;
  for (k = 0; k < 3621350; k++) {
    if (p || (a[k] < 0.0)) {
      p = true;
    } else {
      p = false;
    }
  }

  return p;
}

void b_power(const emlrtStack *sp, const real_T a[3621350], real_T y[3621350])
{
  int32_T k;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &x_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  b_st.site = &y_emlrtRSI;
  c_st.site = &ab_emlrtRSI;
  for (k = 0; k < 3621350; k++) {
    d_st.site = &bb_emlrtRSI;
    e_st.site = &cb_emlrtRSI;
    if (a[k] < 0.0) {
      emlrtErrorWithMessageIdR2018a(&e_st, &qc_emlrtRTEI,
        "Coder:toolbox:ElFunDomainError", "Coder:toolbox:ElFunDomainError", 3, 4,
        4, "sqrt");
    }

    y[k] = muDoubleScalarSqrt(a[k]);
  }

  if (fltpower_domain_error(a)) {
    emlrtErrorWithMessageIdR2018a(&st, &rc_emlrtRTEI,
      "Coder:toolbox:power_domainError", "Coder:toolbox:power_domainError", 0);
  }
}

void c_power(const real_T a[3621350], real_T y[3621350])
{
  int32_T k;
  for (k = 0; k < 3621350; k++) {
    y[k] = a[k] * a[k];
  }
}

void power(const real_T a[10864050], real_T y[10864050])
{
  int32_T k;
  for (k = 0; k < 10864050; k++) {
    y[k] = a[k] * a[k];
  }
}

/* End of code generation (power.c) */
