/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * compute_pos2.h
 *
 * Code generation for function 'compute_pos2'
 *
 */

#ifndef COMPUTE_POS2_H
#define COMPUTE_POS2_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "compute_pos_two_imus2_types.h"

/* Function Declarations */
extern void compute_pos2(compute_pos_two_imus2StackData *SD, const emlrtStack
  *sp, const real_T W[10864050], const real_T A[10864050], real_T PERIOD, real_T
  walking_filter, const real_T periodic_section[3621350], real_T T_FF, real_T
  MAX_T_FF, struct0_T *result);

#endif

/* End of code generation (compute_pos2.h) */
