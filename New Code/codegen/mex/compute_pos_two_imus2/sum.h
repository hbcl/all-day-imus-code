/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sum.h
 *
 * Code generation for function 'sum'
 *
 */

#ifndef SUM_H
#define SUM_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "compute_pos_two_imus2_types.h"

/* Function Declarations */
extern void b_sum(const emxArray_real_T *x, real_T y[3]);
extern void c_sum(const real_T x[7242700], real_T y[3621350]);
extern void sum(const real_T x[10864050], real_T y[3621350]);

#endif

/* End of code generation (sum.h) */
