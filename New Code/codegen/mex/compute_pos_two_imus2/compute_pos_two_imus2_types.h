/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * compute_pos_two_imus2_types.h
 *
 * Code generation for function 'compute_pos_two_imus2'
 *
 */

#ifndef COMPUTE_POS_TWO_IMUS2_TYPES_H
#define COMPUTE_POS_TWO_IMUS2_TYPES_H

/* Include files */
#include "rtwtypes.h"

/* Type Definitions */
#ifndef struct_emxArray_real_T
#define struct_emxArray_real_T

struct emxArray_real_T
{
  real_T *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};

#endif                                 /*struct_emxArray_real_T*/

#ifndef typedef_emxArray_real_T
#define typedef_emxArray_real_T

typedef struct emxArray_real_T emxArray_real_T;

#endif                                 /*typedef_emxArray_real_T*/

#ifndef struct_sLTHyNzUzPPpIRh3eVyVNbC_tag
#define struct_sLTHyNzUzPPpIRh3eVyVNbC_tag

struct sLTHyNzUzPPpIRh3eVyVNbC_tag
{
  emxArray_real_T *f1;
};

#endif                                 /*struct_sLTHyNzUzPPpIRh3eVyVNbC_tag*/

#ifndef typedef_cell_wrap_0
#define typedef_cell_wrap_0

typedef struct sLTHyNzUzPPpIRh3eVyVNbC_tag cell_wrap_0;

#endif                                 /*typedef_cell_wrap_0*/

#ifndef struct_sV4KoXP8F8JNpmJQK8OrsIH_tag
#define struct_sV4KoXP8F8JNpmJQK8OrsIH_tag

struct sV4KoXP8F8JNpmJQK8OrsIH_tag
{
  emxArray_real_T *FFstart;
  emxArray_real_T *FFend;
  real_T is_walking[3621350];
  real_T Anz[10864050];
  real_T An[10864050];
  real_T A[10864050];
  real_T W[10864050];
  real_T quaternion[14485400];
  real_T euler[10864050];
  real_T V[10864050];
  real_T Vm[3621350];
  real_T P[10864050];
};

#endif                                 /*struct_sV4KoXP8F8JNpmJQK8OrsIH_tag*/

#ifndef typedef_struct0_T
#define typedef_struct0_T

typedef struct sV4KoXP8F8JNpmJQK8OrsIH_tag struct0_T;

#endif                                 /*typedef_struct0_T*/

#ifndef typedef_compute_pos_two_imus2StackData
#define typedef_compute_pos_two_imus2StackData

typedef struct {
  struct {
    real_T dv3[10864050];
    real_T dv4[10864050];
    real_T dv5[3621350];
    real_T Wm[3621350];
    real_T dv6[3621350];
    real_T Am[3621350];
    real_T dv7[3621350];
    real_T dif_ps[3621349];
    boolean_T periodic_section[3621350];
    boolean_T b_dif_ps[3621349];
  } f0;

  struct {
    real_T quaternion[14485400];
    real_T An[10864050];
    real_T Anz[10864050];
    real_T z1[7242700];
    real_T b_z1[7242700];
    real_T accel_theta[3621350];
    real_T dv0[3621350];
    real_T x[3621350];
    real_T stationary_periods[3621350];
    real_T phi_tmp[3621350];
    real_T the[3621350];
    real_T dv1[3621350];
    real_T b_stationary_periods[3621350];
    boolean_T bv0[3621350];
  } f1;

  struct {
    struct0_T left_walk_info;
    struct0_T right_walk_info;
  } f2;
} compute_pos_two_imus2StackData;

#endif                                 /*typedef_compute_pos_two_imus2StackData*/

#ifndef struct_emxArray_boolean_T
#define struct_emxArray_boolean_T

struct emxArray_boolean_T
{
  boolean_T *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};

#endif                                 /*struct_emxArray_boolean_T*/

#ifndef typedef_emxArray_boolean_T
#define typedef_emxArray_boolean_T

typedef struct emxArray_boolean_T emxArray_boolean_T;

#endif                                 /*typedef_emxArray_boolean_T*/

#ifndef struct_emxArray_int32_T
#define struct_emxArray_int32_T

struct emxArray_int32_T
{
  int32_T *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};

#endif                                 /*struct_emxArray_int32_T*/

#ifndef typedef_emxArray_int32_T
#define typedef_emxArray_int32_T

typedef struct emxArray_int32_T emxArray_int32_T;

#endif                                 /*typedef_emxArray_int32_T*/
#endif

/* End of code generation (compute_pos_two_imus2_types.h) */
