/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * find.c
 *
 * Code generation for function 'find'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "compute_pos_two_imus2.h"
#include "find.h"
#include "compute_pos_two_imus2_emxutil.h"
#include "indexShapeCheck.h"

/* Variable Definitions */
static emlrtRSInfo r_emlrtRSI = { 153, /* lineNo */
  "eml_find",                          /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/elmat/find.m"/* pathName */
};

static emlrtRTEInfo m_emlrtRTEI = { 153,/* lineNo */
  13,                                  /* colNo */
  "find",                              /* fName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/elmat/find.m"/* pName */
};

static emlrtRTEInfo n_emlrtRTEI = { 153,/* lineNo */
  9,                                   /* colNo */
  "find",                              /* fName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/elmat/find.m"/* pName */
};

static emlrtRTEInfo uc_emlrtRTEI = { 387,/* lineNo */
  1,                                   /* colNo */
  "find_first_indices",                /* fName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/elmat/find.m"/* pName */
};

/* Function Definitions */
void b_eml_find(const emlrtStack *sp, const boolean_T x[3621349],
                emxArray_int32_T *i)
{
  int32_T idx;
  int32_T ii;
  boolean_T exitg1;
  emlrtStack st;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &r_emlrtRSI;
  idx = 0;
  ii = i->size[0];
  i->size[0] = 3621349;
  emxEnsureCapacity_int32_T(&st, i, ii, &m_emlrtRTEI);
  ii = 0;
  exitg1 = false;
  while ((!exitg1) && (ii < 3621349)) {
    if (x[ii]) {
      idx++;
      i->data[idx - 1] = ii + 1;
      if (idx >= 3621349) {
        exitg1 = true;
      } else {
        ii++;
      }
    } else {
      ii++;
    }
  }

  indexShapeCheck();
  ii = i->size[0];
  if (1 > idx) {
    i->size[0] = 0;
  } else {
    i->size[0] = idx;
  }

  emxEnsureCapacity_int32_T(&st, i, ii, &n_emlrtRTEI);
}

void c_eml_find(const emlrtStack *sp, const emxArray_boolean_T *x,
                emxArray_int32_T *i)
{
  int32_T nx;
  int32_T idx;
  int32_T ii;
  boolean_T exitg1;
  emlrtStack st;
  st.prev = sp;
  st.tls = sp->tls;
  nx = x->size[1];
  st.site = &r_emlrtRSI;
  idx = 0;
  ii = i->size[0] * i->size[1];
  i->size[0] = 1;
  i->size[1] = x->size[1];
  emxEnsureCapacity_int32_T(&st, i, ii, &m_emlrtRTEI);
  ii = 0;
  exitg1 = false;
  while ((!exitg1) && (ii <= nx - 1)) {
    if (x->data[ii]) {
      idx++;
      i->data[idx - 1] = ii + 1;
      if (idx >= nx) {
        exitg1 = true;
      } else {
        ii++;
      }
    } else {
      ii++;
    }
  }

  if (idx > x->size[1]) {
    emlrtErrorWithMessageIdR2018a(&st, &uc_emlrtRTEI,
      "Coder:builtins:AssertionFailed", "Coder:builtins:AssertionFailed", 0);
  }

  if (x->size[1] == 1) {
    if (idx == 0) {
      i->size[0] = 1;
      i->size[1] = 0;
    }
  } else if (1 > idx) {
    i->size[1] = 0;
  } else {
    ii = i->size[0] * i->size[1];
    i->size[1] = idx;
    emxEnsureCapacity_int32_T(&st, i, ii, &n_emlrtRTEI);
  }
}

void eml_find(const emlrtStack *sp, const boolean_T x[3621350], emxArray_int32_T
              *i)
{
  int32_T idx;
  int32_T ii;
  boolean_T exitg1;
  emlrtStack st;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &r_emlrtRSI;
  idx = 0;
  ii = i->size[0];
  i->size[0] = 3621350;
  emxEnsureCapacity_int32_T(&st, i, ii, &m_emlrtRTEI);
  ii = 0;
  exitg1 = false;
  while ((!exitg1) && (ii < 3621350)) {
    if (x[ii]) {
      idx++;
      i->data[idx - 1] = ii + 1;
      if (idx >= 3621350) {
        exitg1 = true;
      } else {
        ii++;
      }
    } else {
      ii++;
    }
  }

  indexShapeCheck();
  ii = i->size[0];
  if (1 > idx) {
    i->size[0] = 0;
  } else {
    i->size[0] = idx;
  }

  emxEnsureCapacity_int32_T(&st, i, ii, &n_emlrtRTEI);
}

/* End of code generation (find.c) */
