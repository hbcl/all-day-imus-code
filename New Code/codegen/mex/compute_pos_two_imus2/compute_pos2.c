/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * compute_pos2.c
 *
 * Code generation for function 'compute_pos2'
 *
 */

/* Include files */
#include <math.h>
#include <string.h>
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "compute_pos_two_imus2.h"
#include "compute_pos2.h"
#include "qua2rot.h"
#include "compute_pos_two_imus2_emxutil.h"
#include "foot_fall2.h"
#include "power.h"
#include "sum.h"
#include "atan2.h"
#include "asin.h"
#include "eml_int_forloop_overflow_check.h"
#include "zupts_func.h"
#include "kf_tilt2.h"
#include "kf_tilt1.h"
#include "find.h"
#include "abs.h"
#include "compute_pos_two_imus2_data.h"

/* Variable Definitions */
static emlrtRSInfo c_emlrtRSI = { 48,  /* lineNo */
  "compute_pos2",                      /* fcnName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/compute_pos2.m"/* pathName */
};

static emlrtRSInfo d_emlrtRSI = { 52,  /* lineNo */
  "compute_pos2",                      /* fcnName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/compute_pos2.m"/* pathName */
};

static emlrtRSInfo e_emlrtRSI = { 84,  /* lineNo */
  "compute_pos2",                      /* fcnName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/compute_pos2.m"/* pathName */
};

static emlrtRSInfo f_emlrtRSI = { 86,  /* lineNo */
  "compute_pos2",                      /* fcnName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/compute_pos2.m"/* pathName */
};

static emlrtRSInfo g_emlrtRSI = { 97,  /* lineNo */
  "compute_pos2",                      /* fcnName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/compute_pos2.m"/* pathName */
};

static emlrtRSInfo h_emlrtRSI = { 99,  /* lineNo */
  "compute_pos2",                      /* fcnName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/compute_pos2.m"/* pathName */
};

static emlrtRSInfo i_emlrtRSI = { 108, /* lineNo */
  "compute_pos2",                      /* fcnName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/compute_pos2.m"/* pathName */
};

static emlrtRSInfo j_emlrtRSI = { 109, /* lineNo */
  "compute_pos2",                      /* fcnName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/compute_pos2.m"/* pathName */
};

static emlrtRSInfo k_emlrtRSI = { 121, /* lineNo */
  "compute_pos2",                      /* fcnName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/compute_pos2.m"/* pathName */
};

static emlrtRSInfo l_emlrtRSI = { 132, /* lineNo */
  "compute_pos2",                      /* fcnName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/compute_pos2.m"/* pathName */
};

static emlrtRSInfo m_emlrtRSI = { 10,  /* lineNo */
  "acc_tilt",                          /* fcnName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/acc_tilt.m"/* pathName */
};

static emlrtRSInfo n_emlrtRSI = { 11,  /* lineNo */
  "acc_tilt",                          /* fcnName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/acc_tilt.m"/* pathName */
};

static emlrtRSInfo o_emlrtRSI = { 15,  /* lineNo */
  "acc_tilt",                          /* fcnName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/acc_tilt.m"/* pathName */
};

static emlrtRSInfo p_emlrtRSI = { 16,  /* lineNo */
  "acc_tilt",                          /* fcnName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/acc_tilt.m"/* pathName */
};

static emlrtRSInfo fc_emlrtRSI = { 11, /* lineNo */
  "qua_est",                           /* fcnName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/qua_est.m"/* pathName */
};

static emlrtRSInfo gc_emlrtRSI = { 40, /* lineNo */
  "applyBinaryScalarFunction",         /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/eml/+coder/+internal/applyBinaryScalarFunction.m"/* pathName */
};

static emlrtRSInfo vc_emlrtRSI = { 31, /* lineNo */
  "safeEq",                            /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/eml/+coder/+internal/safeEq.m"/* pathName */
};

static emlrtRTEInfo emlrtRTEI = { 10,  /* lineNo */
  1,                                   /* colNo */
  "acc_tilt",                          /* fName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/acc_tilt.m"/* pName */
};

static emlrtRTEInfo b_emlrtRTEI = { 11,/* lineNo */
  13,                                  /* colNo */
  "acc_tilt",                          /* fName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/acc_tilt.m"/* pName */
};

static emlrtRTEInfo c_emlrtRTEI = { 11,/* lineNo */
  29,                                  /* colNo */
  "acc_tilt",                          /* fName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/acc_tilt.m"/* pName */
};

static emlrtRTEInfo d_emlrtRTEI = { 15,/* lineNo */
  1,                                   /* colNo */
  "acc_tilt",                          /* fName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/acc_tilt.m"/* pName */
};

static emlrtRTEInfo e_emlrtRTEI = { 16,/* lineNo */
  11,                                  /* colNo */
  "acc_tilt",                          /* fName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/acc_tilt.m"/* pName */
};

static emlrtRTEInfo f_emlrtRTEI = { 16,/* lineNo */
  28,                                  /* colNo */
  "acc_tilt",                          /* fName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/acc_tilt.m"/* pName */
};

static emlrtRTEInfo g_emlrtRTEI = { 16,/* lineNo */
  27,                                  /* colNo */
  "acc_tilt",                          /* fName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/acc_tilt.m"/* pName */
};

static emlrtRTEInfo h_emlrtRTEI = { 70,/* lineNo */
  5,                                   /* colNo */
  "compute_pos2",                      /* fName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/compute_pos2.m"/* pName */
};

static emlrtRTEInfo i_emlrtRTEI = { 71,/* lineNo */
  5,                                   /* colNo */
  "compute_pos2",                      /* fName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/compute_pos2.m"/* pName */
};

static emlrtRTEInfo j_emlrtRTEI = { 52,/* lineNo */
  4,                                   /* colNo */
  "compute_pos2",                      /* fName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/compute_pos2.m"/* pName */
};

static emlrtRTEInfo k_emlrtRTEI = { 52,/* lineNo */
  12,                                  /* colNo */
  "compute_pos2",                      /* fName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/compute_pos2.m"/* pName */
};

static emlrtECInfo emlrtECI = { -1,    /* nDims */
  16,                                  /* lineNo */
  1,                                   /* colNo */
  "acc_tilt",                          /* fName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/acc_tilt.m"/* pName */
};

static emlrtECInfo b_emlrtECI = { -1,  /* nDims */
  11,                                  /* lineNo */
  1,                                   /* colNo */
  "acc_tilt",                          /* fName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/acc_tilt.m"/* pName */
};

static emlrtBCInfo emlrtBCI = { 1,     /* iFirst */
  3621350,                             /* iLast */
  11,                                  /* lineNo */
  13,                                  /* colNo */
  "accel_theta",                       /* aName */
  "acc_tilt",                          /* fName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/acc_tilt.m",/* pName */
  3                                    /* checkKind */
};

static emlrtBCInfo b_emlrtBCI = { 1,   /* iFirst */
  3621350,                             /* iLast */
  16,                                  /* lineNo */
  11,                                  /* colNo */
  "accel_phi",                         /* aName */
  "acc_tilt",                          /* fName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/acc_tilt.m",/* pName */
  3                                    /* checkKind */
};

/* Function Definitions */
void compute_pos2(compute_pos_two_imus2StackData *SD, const emlrtStack *sp,
                  const real_T W[10864050], const real_T A[10864050], real_T
                  PERIOD, real_T walking_filter, const real_T periodic_section
                  [3621350], real_T T_FF, real_T MAX_T_FF, struct0_T *result)
{
  int32_T k;
  emxArray_int32_T *valid_angles;
  emxArray_int32_T *ii;
  int32_T loop_ub;
  emxArray_real_T *r0;
  int32_T ns;
  emxArray_real_T *FFstart;
  emxArray_real_T *FFend;
  real_T Q;
  real_T R;
  real_T P;
  int32_T last_footfall;
  real_T absxk;
  real_T z1_tmp;
  real_T b_z1_tmp;
  real_T a;
  real_T mag;
  real_T dv2[16];
  real_T d0;
  real_T d1;
  real_T d2;
  real_T quaternion[4];
  real_T rotation_matrix[9];
  boolean_T tf;
  boolean_T exitg1;
  boolean_T guard1 = false;
  int32_T exponent;
  boolean_T p;
  int32_T b_exponent;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  emlrtStack f_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  f_st.prev = &e_st;
  f_st.tls = e_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  emlrtMEXProfilingFunctionEntry(compute_pos2_complete_name, isMexOutdated);

  /*  Author: Lauro Ojeda, 2003-2015 */
  /*  same thing as compute_pos but without optional arguments */
  /*  [result] = compute_pos(W,A,PERIOD,USE_KF,W_FF,A_FF,T_FF,MAX_T_FF,FF) */
  /*   */
  /*  Computes IMU positions assuming zero velocity updates */
  /*   */
  /*  result: a structure containing results fields */
  /*  inputs:  */
  /*    W: Angular Velocity, finite difference form (Angular Velocity (rad/sec) * PERIOD = finite integral of angular velocity over a single sample)  */
  /*    A: Acceleration (m/s/s) */
  /*    PERIOD: Sampling Period (seconds) */
  /*  Optional Inputs */
  /*    USE_KF: logical, use (default = 1) or do not use (=0) the Tilt Kalman Filter to correct IMU tilt.  */
  /*    W_FF: Upper Threshold for Angular Velocity (deg/sec) for detecting footfalls  */
  /*    A_FF: Upper Threshold for (Acceleration minus Gravity) (m/s/s) for detecting footfalls  */
  /*    T_FF: Minimum Time that must elapse between two footfalls (default 0.4 sec) */
  /*    MAX_T_FF: Maximum Time that can elapse During Rest Periods before looking for a new footfall (seconds; default 3*T_FF)  */
  /*    FF: Logical array for all the samples in W and A, indicating whether they are Footfalls.  */
  /*        Use this to override internal footfall computations, e.g. if */
  /*        footfall detection is not working well, or for IMUs not on the */
  /*        foot.  */
  /*  Set Up Optional Arguments */
  /*  	if(~exist('USE_KF','var') | isempty(USE_KF)) */
  /*  		USE_KF = 1; */
  /*      end */
  /*  	if(~exist('W_FF','var') | isempty(W_FF)) */
  /*  		W_FF = 60; */
  /*      end */
  /*  	if(~exist('A_FF','var') | isempty(A_FF)) */
  /*  		A_FF = 2; */
  /*      end */
  /*      if(~exist('T_FF','var') | isempty(T_FF)) */
  /*  	  T_FF = floor(0.4/PERIOD); % Minimum time allowed between two FFs */
  /*      else */
  /*        T_FF = floor(T_FF/PERIOD); % convert Seconds into Samples */
  /*      end */
  /*    	if(~exist('walking_filter','var') | isempty(walking_filter)) */
  /*  		walking_filter = 0; */
  /*      end */
  /*  Inertial navigation mechanization */
  emlrtMEXProfilingStatement(0, isMexOutdated);

  /*  Compute tilt based on accelerometer readings */
  emlrtMEXProfilingStatement(4, isMexOutdated);
  st.site = &c_emlrtRSI;
  emlrtMEXProfilingFunctionEntry(acc_tilt_complete_name, isMexOutdated);

  /*  Author: Lauro Ojeda, 2001-2015 */
  /* Compute tilt form acceleration */
  emlrtMEXProfilingStatement(2, isMexOutdated);
  emlrtMEXProfilingStatement(3, isMexOutdated);

  /* Compute pitch  */
  emlrtMEXProfilingStatement(4, isMexOutdated);
  for (k = 0; k < 3621350; k++) {
    SD->f1.accel_theta[k] = A[k] / 9.80297286843;
  }

  emlrtMEXProfilingStatement(5, isMexOutdated);
  b_st.site = &m_emlrtRSI;
  b_abs(SD->f1.accel_theta, SD->f1.dv0);
  for (k = 0; k < 3621350; k++) {
    SD->f1.bv0[k] = (SD->f1.dv0[k] <= 1.0);
  }

  emxInit_int32_T(&b_st, &valid_angles, 1, &emlrtRTEI, true);
  emxInit_int32_T(&b_st, &ii, 1, &l_emlrtRTEI, true);
  c_st.site = &q_emlrtRSI;
  eml_find(&c_st, SD->f1.bv0, ii);
  k = valid_angles->size[0];
  valid_angles->size[0] = ii->size[0];
  emxEnsureCapacity_int32_T(&b_st, valid_angles, k, &emlrtRTEI);
  loop_ub = ii->size[0];
  for (k = 0; k < loop_ub; k++) {
    valid_angles->data[k] = ii->data[k];
  }

  emlrtMEXProfilingStatement(6, isMexOutdated);
  k = ii->size[0];
  ii->size[0] = valid_angles->size[0];
  emxEnsureCapacity_int32_T(&st, ii, k, &b_emlrtRTEI);
  loop_ub = valid_angles->size[0];
  for (k = 0; k < loop_ub; k++) {
    ns = valid_angles->data[k];
    if ((ns < 1) || (ns > 3621350)) {
      emlrtDynamicBoundsCheckR2012b(ns, 1, 3621350, &emlrtBCI, &st);
    }

    ii->data[k] = ns;
  }

  emxInit_real_T(&st, &r0, 1, &f_emlrtRTEI, true);
  k = r0->size[0];
  r0->size[0] = valid_angles->size[0];
  emxEnsureCapacity_real_T(&st, r0, k, &c_emlrtRTEI);
  loop_ub = valid_angles->size[0];
  for (k = 0; k < loop_ub; k++) {
    r0->data[k] = SD->f1.accel_theta[valid_angles->data[k] - 1];
  }

  b_st.site = &n_emlrtRSI;
  b_asin(&b_st, r0);
  k = ii->size[0];
  ns = r0->size[0];
  if (k != ns) {
    emlrtSubAssignSizeCheck1dR2017a(k, ns, &b_emlrtECI, &st);
  }

  loop_ub = r0->size[0];
  for (k = 0; k < loop_ub; k++) {
    SD->f1.accel_theta[ii->data[k] - 1] = r0->data[k];
  }

  /* Compute roll angle */
  emlrtMEXProfilingStatement(7, isMexOutdated);
  for (k = 0; k < 3621350; k++) {
    SD->f1.x[k] = A[3621350 + k] / muDoubleScalarCos(SD->f1.accel_theta[k]) /
      9.80297286843;
  }

  emlrtMEXProfilingStatement(8, isMexOutdated);
  b_st.site = &o_emlrtRSI;
  b_abs(SD->f1.x, SD->f1.dv0);
  for (k = 0; k < 3621350; k++) {
    SD->f1.bv0[k] = (SD->f1.dv0[k] <= 1.0);
  }

  c_st.site = &q_emlrtRSI;
  eml_find(&c_st, SD->f1.bv0, ii);
  k = valid_angles->size[0];
  valid_angles->size[0] = ii->size[0];
  emxEnsureCapacity_int32_T(&b_st, valid_angles, k, &d_emlrtRTEI);
  loop_ub = ii->size[0];
  for (k = 0; k < loop_ub; k++) {
    valid_angles->data[k] = ii->data[k];
  }

  emlrtMEXProfilingStatement(9, isMexOutdated);
  k = ii->size[0];
  ii->size[0] = valid_angles->size[0];
  emxEnsureCapacity_int32_T(&st, ii, k, &e_emlrtRTEI);
  loop_ub = valid_angles->size[0];
  for (k = 0; k < loop_ub; k++) {
    ns = valid_angles->data[k];
    if ((ns < 1) || (ns > 3621350)) {
      emlrtDynamicBoundsCheckR2012b(ns, 1, 3621350, &b_emlrtBCI, &st);
    }

    ii->data[k] = ns;
  }

  k = r0->size[0];
  r0->size[0] = valid_angles->size[0];
  emxEnsureCapacity_real_T(&st, r0, k, &f_emlrtRTEI);
  loop_ub = valid_angles->size[0];
  for (k = 0; k < loop_ub; k++) {
    r0->data[k] = SD->f1.x[valid_angles->data[k] - 1];
  }

  emxFree_int32_T(&valid_angles);
  b_st.site = &p_emlrtRSI;
  b_asin(&b_st, r0);
  k = r0->size[0];
  emxEnsureCapacity_real_T(&st, r0, k, &g_emlrtRTEI);
  loop_ub = r0->size[0];
  for (k = 0; k < loop_ub; k++) {
    r0->data[k] = -r0->data[k];
  }

  k = ii->size[0];
  ns = r0->size[0];
  if (k != ns) {
    emlrtSubAssignSizeCheck1dR2017a(k, ns, &emlrtECI, &st);
  }

  loop_ub = r0->size[0];
  for (k = 0; k < loop_ub; k++) {
    SD->f1.x[ii->data[k] - 1] = r0->data[k];
  }

  emxFree_int32_T(&ii);
  emxFree_real_T(&r0);
  emxInit_real_T(&st, &FFstart, 2, &j_emlrtRTEI, true);
  emxInit_real_T(&st, &FFend, 2, &k_emlrtRTEI, true);
  emlrtMEXProfilingFunctionExit(isMexOutdated);

  /* accel_phi = roll angle accel_theta = pitch */
  /*  Determine FFs */
  emlrtMEXProfilingStatement(5, isMexOutdated);
  emlrtMEXProfilingStatement(6, isMexOutdated);
  st.site = &d_emlrtRSI;
  foot_fall2(SD, &st, W, A, PERIOD, walking_filter, periodic_section, T_FF,
             MAX_T_FF, FFstart, FFend, SD->f1.stationary_periods);
  emlrtMEXProfilingStatement(17, isMexOutdated);

  /* 	result.FF = FF; % FF = 0s and 1s showing footfall locations */
  emlrtMEXProfilingStatement(18, isMexOutdated);
  k = result->FFstart->size[0] * result->FFstart->size[1];
  result->FFstart->size[0] = FFstart->size[0];
  result->FFstart->size[1] = FFstart->size[1];
  emxEnsureCapacity_real_T(sp, result->FFstart, k, &h_emlrtRTEI);
  loop_ub = FFstart->size[0] * FFstart->size[1];
  for (k = 0; k < loop_ub; k++) {
    result->FFstart->data[k] = FFstart->data[k];
  }

  emlrtMEXProfilingStatement(19, isMexOutdated);
  k = result->FFend->size[0] * result->FFend->size[1];
  result->FFend->size[0] = FFend->size[0];
  result->FFend->size[1] = FFend->size[1];
  emxEnsureCapacity_real_T(sp, result->FFend, k, &i_emlrtRTEI);
  loop_ub = FFend->size[0] * FFend->size[1];
  for (k = 0; k < loop_ub; k++) {
    result->FFend->data[k] = FFend->data[k];
  }

  emlrtMEXProfilingStatement(20, isMexOutdated);
  memcpy(&result->is_walking[0], &periodic_section[0], 3621350U * sizeof(real_T));

  /*  Initialize variables */
  emlrtMEXProfilingStatement(21, isMexOutdated);
  memset(&SD->f1.quaternion[0], 0, 14485400U * sizeof(real_T));
  emlrtMEXProfilingStatement(22, isMexOutdated);
  memset(&SD->f1.An[0], 0, 10864050U * sizeof(real_T));
  emlrtMEXProfilingStatement(23, isMexOutdated);
  memset(&SD->f1.Anz[0], 0, 10864050U * sizeof(real_T));

  /*  Initialize quaternions */
  emlrtMEXProfilingStatement(24, isMexOutdated);
  kf_tilt1(PERIOD, &Q, &R);
  SD->f1.quaternion[0] = 1.0;
  SD->f1.quaternion[3621350] = 0.0;
  SD->f1.quaternion[7242700] = 0.0;
  SD->f1.quaternion[10864050] = 0.0;
  P = 0.0;
  emlrtMEXProfilingStatement(25, isMexOutdated);
  last_footfall = 0;
  emlrtMEXProfilingStatement(26, isMexOutdated);
  for (loop_ub = 0; loop_ub < 3621349; loop_ub++) {
    /*  Compute attitude using quaternion representation */
    emlrtMEXProfilingStatement(27, isMexOutdated);
    st.site = &e_emlrtRSI;
    emlrtMEXProfilingFunctionEntry(qua_est_complete_name, isMexOutdated);

    /*  Author: Lauro Ojeda, 2003-2015 */
    /*  Bibliography: */
    /*  - Ken Shoemake: quaternion_uaternions */
    /*  pp 7 */
    /*  Given a rotationation expressed in quaternion format */
    /*  R*quaternion = rotationation about the X, Y and Z navigation axis */
    /*  quaternion*R = rotationation about the X, Y and Z body axis */
    /*  - David Titterton and John Weston, Strapdown Inertial Navigation Technology, 2nd Edition */
    /*  pp 320 */
    emlrtMEXProfilingStatement(1, isMexOutdated);
    absxk = W[loop_ub + 1];
    z1_tmp = W[loop_ub + 3621351];
    b_z1_tmp = W[loop_ub + 7242701];
    a = (absxk * absxk + z1_tmp * z1_tmp) + b_z1_tmp * b_z1_tmp;
    b_st.site = &fc_emlrtRSI;
    c_st.site = &x_emlrtRSI;
    d_st.site = &y_emlrtRSI;
    e_st.site = &gc_emlrtRSI;
    f_st.site = &cb_emlrtRSI;
    if (a < 0.0) {
      emlrtErrorWithMessageIdR2018a(&f_st, &qc_emlrtRTEI,
        "Coder:toolbox:ElFunDomainError", "Coder:toolbox:ElFunDomainError", 3, 4,
        4, "sqrt");
    }

    mag = muDoubleScalarSqrt(a);
    emlrtMEXProfilingStatement(2, isMexOutdated);
    if (mag != 0.0) {
      emlrtMEXProfilingStatement(3, isMexOutdated);
      a = muDoubleScalarSin(mag / 2.0) / mag;
      emlrtMEXProfilingStatement(6, isMexOutdated);
    } else {
      emlrtMEXProfilingStatement(4, isMexOutdated);
      emlrtMEXProfilingStatement(5, isMexOutdated);
      a = 0.5;
      emlrtMEXProfilingStatement(6, isMexOutdated);
    }

    emlrtMEXProfilingStatement(7, isMexOutdated);
    emlrtMEXProfilingStatement(8, isMexOutdated);
    emlrtMEXProfilingStatement(9, isMexOutdated);
    emlrtMEXProfilingStatement(10, isMexOutdated);
    emlrtMEXProfilingStatement(11, isMexOutdated);
    emlrtMEXProfilingStatement(12, isMexOutdated);
    emlrtMEXProfilingStatement(13, isMexOutdated);
    emlrtMEXProfilingFunctionExit(isMexOutdated);
    dv2[0] = SD->f1.quaternion[loop_ub];
    d0 = SD->f1.quaternion[3621350 + loop_ub];
    dv2[4] = -d0;
    d1 = SD->f1.quaternion[7242700 + loop_ub];
    dv2[8] = -d1;
    d2 = SD->f1.quaternion[10864050 + loop_ub];
    dv2[12] = -d2;
    dv2[1] = d0;
    dv2[5] = SD->f1.quaternion[loop_ub];
    dv2[9] = -d2;
    dv2[13] = d1;
    dv2[2] = d1;
    dv2[6] = d2;
    dv2[10] = SD->f1.quaternion[loop_ub];
    dv2[14] = -SD->f1.quaternion[3621350 + loop_ub];
    dv2[3] = d2;
    dv2[7] = -SD->f1.quaternion[7242700 + loop_ub];
    dv2[11] = d0;
    dv2[15] = SD->f1.quaternion[loop_ub];
    quaternion[0] = muDoubleScalarCos(mag / 2.0);
    quaternion[1] = a * absxk;
    quaternion[2] = a * z1_tmp;
    quaternion[3] = a * b_z1_tmp;
    for (k = 0; k < 4; k++) {
      SD->f1.quaternion[(loop_ub + 3621350 * k) + 1] = ((dv2[k] * quaternion[0]
        + dv2[k + 4] * quaternion[1]) + dv2[k + 8] * quaternion[2]) + dv2[k + 12]
        * quaternion[3];
    }

    /*  qua_est estimates the next step based on previous quaterion and angular velocity */
    /*  Transform accelerations from body to navigation frame */
    emlrtMEXProfilingStatement(28, isMexOutdated);
    quaternion[0] = SD->f1.quaternion[loop_ub + 1];
    absxk = SD->f1.quaternion[loop_ub + 3621351];
    quaternion[1] = absxk;
    z1_tmp = SD->f1.quaternion[loop_ub + 7242701];
    quaternion[2] = z1_tmp;
    b_z1_tmp = SD->f1.quaternion[loop_ub + 10864051];
    quaternion[3] = b_z1_tmp;
    st.site = &f_emlrtRSI;
    qua2rot(quaternion, rotation_matrix);

    /*  make rotation matrix based on current orientation */
    /* inv_quat = [quaternion(i,1) -quaternion(i,2) -quaternion(i,3) -quaternion(i,4)]; */
    /* this_accel = [0 A(i,1) A(i,2) A(i,3)]; */
    emlrtMEXProfilingStatement(29, isMexOutdated);
    for (k = 0; k < 3; k++) {
      SD->f1.An[(loop_ub + 3621350 * k) + 1] = (rotation_matrix[k] * A[loop_ub +
        1] + rotation_matrix[k + 3] * A[loop_ub + 3621351]) + rotation_matrix[k
        + 6] * A[loop_ub + 7242701];
    }

    /*  apply rotation to accelerations */
    /* accel_rot = quaternion(i,:).*this_accel.*inv_quat; */
    /* An(i,1) = accel_rot(2); */
    /* An(i,2) = accel_rot(3); */
    /* An(i,3) = accel_rot(4); */
    emlrtMEXProfilingStatement(30, isMexOutdated);

    /*  Apply KF compensation on tilt */
    emlrtMEXProfilingStatement(31, isMexOutdated);
    if (2 + loop_ub == 1170218) {
      emlrtMEXProfilingStatement(32, isMexOutdated);
      quaternion[0] = SD->f1.quaternion[1170217];
      quaternion[1] = SD->f1.quaternion[4791567];
      quaternion[2] = SD->f1.quaternion[8412917];
      quaternion[3] = SD->f1.quaternion[12034267];
      st.site = &g_emlrtRSI;
      kf_tilt2(&st, quaternion, SD->f1.accel_theta[1170217], SD->f1.x[1170217],
               SD->f1.stationary_periods[1170217], &P, &Q, &R);
      SD->f1.quaternion[1170217] = quaternion[0];
      SD->f1.quaternion[4791567] = quaternion[1];
      SD->f1.quaternion[8412917] = quaternion[2];
      SD->f1.quaternion[12034267] = quaternion[3];
      emlrtMEXProfilingStatement(35, isMexOutdated);
    } else {
      emlrtMEXProfilingStatement(33, isMexOutdated);
      emlrtMEXProfilingStatement(34, isMexOutdated);
      quaternion[0] = SD->f1.quaternion[loop_ub + 1];
      quaternion[1] = absxk;
      quaternion[2] = z1_tmp;
      quaternion[3] = b_z1_tmp;
      st.site = &h_emlrtRSI;
      kf_tilt2(&st, quaternion, SD->f1.accel_theta[loop_ub + 1], SD->
               f1.x[loop_ub + 1], SD->f1.stationary_periods[loop_ub + 1], &P, &Q,
               &R);
      SD->f1.quaternion[loop_ub + 1] = quaternion[0];
      SD->f1.quaternion[loop_ub + 3621351] = quaternion[1];
      SD->f1.quaternion[loop_ub + 7242701] = quaternion[2];
      SD->f1.quaternion[loop_ub + 10864051] = quaternion[3];
      emlrtMEXProfilingStatement(35, isMexOutdated);
    }

    emlrtMEXProfilingStatement(36, isMexOutdated);

    /*  Apply Zero Velocity Updates */
    /*  This script has not ben made into a function to make the code run faster */
    /*  this call will update the Az variable wich contains the ZUPT updated navigation acceleration */
    emlrtMEXProfilingStatement(37, isMexOutdated);
    if (2 + loop_ub == 2) {
      emlrtMEXProfilingStatement(38, isMexOutdated);
      last_footfall = 0;
      emlrtMEXProfilingStatement(39, isMexOutdated);
    }

    emlrtMEXProfilingStatement(40, isMexOutdated);
    st.site = &i_emlrtRSI;
    zupts_func(&st, SD->f1.An, SD->f1.Anz, FFstart, FFend, 2.0 + (real_T)loop_ub,
               last_footfall);
    emlrtMEXProfilingStatement(41, isMexOutdated);
    st.site = &j_emlrtRSI;
    b_st.site = &sc_emlrtRSI;
    ns = FFstart->size[0] * FFstart->size[1];
    tf = false;
    c_st.site = &uc_emlrtRSI;
    if ((1 <= ns) && (ns > 2147483646)) {
      d_st.site = &w_emlrtRSI;
      check_forloop_overflow_error(&d_st);
    }

    k = 0;
    exitg1 = false;
    while ((!exitg1) && (k <= ns - 1)) {
      c_st.site = &tc_emlrtRSI;
      absxk = FFstart->data[k];
      d_st.site = &vc_emlrtRSI;
      absxk = muDoubleScalarAbs(absxk / 2.0);
      if ((!muDoubleScalarIsInf(absxk)) && (!muDoubleScalarIsNaN(absxk))) {
        if (absxk <= 2.2250738585072014E-308) {
          absxk = 4.94065645841247E-324;
        } else {
          frexp(absxk, &exponent);
          absxk = ldexp(1.0, exponent - 53);
        }
      } else {
        absxk = rtNaN;
      }

      p = (muDoubleScalarAbs(FFstart->data[k] - (2.0 + (real_T)loop_ub)) < absxk);
      if (p) {
        tf = true;
        exitg1 = true;
      } else {
        k++;
      }
    }

    guard1 = false;
    if (tf) {
      guard1 = true;
    } else {
      st.site = &j_emlrtRSI;
      b_st.site = &sc_emlrtRSI;
      ns = FFend->size[0] * FFend->size[1];
      tf = false;
      c_st.site = &uc_emlrtRSI;
      if ((1 <= ns) && (ns > 2147483646)) {
        d_st.site = &w_emlrtRSI;
        check_forloop_overflow_error(&d_st);
      }

      k = 0;
      exitg1 = false;
      while ((!exitg1) && (k <= ns - 1)) {
        c_st.site = &tc_emlrtRSI;
        absxk = FFend->data[k];
        d_st.site = &vc_emlrtRSI;
        absxk = muDoubleScalarAbs(absxk / 2.0);
        if ((!muDoubleScalarIsInf(absxk)) && (!muDoubleScalarIsNaN(absxk))) {
          if (absxk <= 2.2250738585072014E-308) {
            absxk = 4.94065645841247E-324;
          } else {
            frexp(absxk, &b_exponent);
            absxk = ldexp(1.0, b_exponent - 53);
          }
        } else {
          absxk = rtNaN;
        }

        p = (muDoubleScalarAbs(FFend->data[k] - (2.0 + (real_T)loop_ub)) < absxk);
        if (p) {
          tf = true;
          exitg1 = true;
        } else {
          k++;
        }
      }

      if (tf) {
        guard1 = true;
      }
    }

    if (guard1) {
      emlrtMEXProfilingStatement(42, isMexOutdated);
      last_footfall = loop_ub + 2;
      emlrtMEXProfilingStatement(43, isMexOutdated);
    }

    emlrtMEXProfilingStatement(44, isMexOutdated);
    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  emxFree_real_T(&FFend);
  emxFree_real_T(&FFstart);
  emlrtMEXProfilingStatement(45, isMexOutdated);
  memcpy(&result->Anz[0], &SD->f1.Anz[0], 10864050U * sizeof(real_T));
  emlrtMEXProfilingStatement(46, isMexOutdated);
  memcpy(&result->An[0], &SD->f1.An[0], 10864050U * sizeof(real_T));
  emlrtMEXProfilingStatement(47, isMexOutdated);
  memcpy(&result->A[0], &A[0], 10864050U * sizeof(real_T));
  emlrtMEXProfilingStatement(48, isMexOutdated);
  memcpy(&result->W[0], &W[0], 10864050U * sizeof(real_T));
  emlrtMEXProfilingStatement(49, isMexOutdated);
  memcpy(&result->quaternion[0], &SD->f1.quaternion[0], 14485400U * sizeof
         (real_T));

  /*  Plot attitude */
  emlrtMEXProfilingStatement(50, isMexOutdated);
  st.site = &k_emlrtRSI;
  emlrtMEXProfilingFunctionEntry(qua2eul_complete_name, isMexOutdated);

  /*  Author: Lauro Ojeda, 2003-2015 */
  /*  Bibliography: */
  /*  David Titterton and John Weston, Strapdown Inertial Navigation Technology, 2nd Edition */
  /*  pp 45 */
  /*  Converts quaternion to Euler representation */
  emlrtMEXProfilingStatement(1, isMexOutdated);
  emlrtMEXProfilingStatement(2, isMexOutdated);
  emlrtMEXProfilingStatement(3, isMexOutdated);
  emlrtMEXProfilingStatement(4, isMexOutdated);
  emlrtMEXProfilingStatement(5, isMexOutdated);
  c_power(*(real_T (*)[3621350])&SD->f1.quaternion[0], SD->f1.stationary_periods);
  c_power(*(real_T (*)[3621350])&SD->f1.quaternion[3621350], SD->f1.accel_theta);
  c_power(*(real_T (*)[3621350])&SD->f1.quaternion[7242700], SD->f1.x);
  c_power(*(real_T (*)[3621350])&SD->f1.quaternion[10864050], SD->f1.phi_tmp);
  emlrtMEXProfilingStatement(6, isMexOutdated);
  for (k = 0; k < 3621350; k++) {
    SD->f1.the[k] = 2.0 * (SD->f1.quaternion[k] * SD->f1.quaternion[7242700 + k]
      - SD->f1.quaternion[10864050 + k] * SD->f1.quaternion[3621350 + k]);
  }

  b_st.site = &oc_emlrtRSI;
  c_asin(&b_st, SD->f1.the);
  emlrtMEXProfilingStatement(7, isMexOutdated);
  emlrtMEXProfilingStatement(8, isMexOutdated);
  for (k = 0; k < 3621350; k++) {
    SD->f1.dv1[k] = 2.0 * (SD->f1.quaternion[k] * SD->f1.quaternion[3621350 + k]
      + SD->f1.quaternion[7242700 + k] * SD->f1.quaternion[10864050 + k]);
    SD->f1.b_stationary_periods[k] = ((SD->f1.stationary_periods[k] -
      SD->f1.accel_theta[k]) - SD->f1.x[k]) + SD->f1.phi_tmp[k];
  }

  b_atan2(SD->f1.dv1, SD->f1.b_stationary_periods, SD->f1.dv0);
  for (k = 0; k < 3621350; k++) {
    SD->f1.dv1[k] = 2.0 * (SD->f1.quaternion[k] * SD->f1.quaternion[10864050 + k]
      + SD->f1.quaternion[3621350 + k] * SD->f1.quaternion[7242700 + k]);
    SD->f1.accel_theta[k] = ((SD->f1.stationary_periods[k] + SD->
      f1.accel_theta[k]) - SD->f1.x[k]) - SD->f1.phi_tmp[k];
    result->euler[k] = SD->f1.dv0[k];
    result->euler[3621350 + k] = SD->f1.the[k];
  }

  b_atan2(SD->f1.dv1, SD->f1.accel_theta, SD->f1.stationary_periods);
  memcpy(&result->euler[7242700], &SD->f1.stationary_periods[0], 3621350U *
         sizeof(real_T));
  emlrtMEXProfilingFunctionExit(isMexOutdated);
  emlrtMEXProfilingStatement(51, isMexOutdated);

  /* 	figure, */
  /* 	plot(t,euler*180/pi); */
  /* 	hold on;grid on;ylabel('Euler angles [deg]');xlabel('time [s]'); */
  /* 	legend('Roll','Pitch','Heading'); */
  /* 	plot(t(FF),euler(FF,:)*180/pi,'*'); */
  /* 	hold off; */
  /*  Set A/V/P to zero during bias drift estimation */
  emlrtMEXProfilingStatement(52, isMexOutdated);
  for (k = 0; k < 3; k++) {
    for (loop_ub = 0; loop_ub < 3621349; loop_ub++) {
      ns = loop_ub + 3621350 * k;
      last_footfall = ns + 1;
      SD->f1.Anz[last_footfall] += SD->f1.Anz[ns];
    }
  }

  for (k = 0; k < 10864050; k++) {
    SD->f1.Anz[k] *= PERIOD;
  }

  emlrtMEXProfilingStatement(53, isMexOutdated);
  for (k = 0; k < 7242700; k++) {
    loop_ub = k % 3621350 + 3621350 * (k / 3621350);
    SD->f1.z1[k] = SD->f1.Anz[loop_ub] * SD->f1.Anz[loop_ub];
  }

  for (k = 0; k < 3621350; k++) {
    loop_ub = k << 1;
    SD->f1.b_z1[loop_ub] = SD->f1.z1[k];
    SD->f1.b_z1[1 + loop_ub] = SD->f1.z1[k + 3621350];
  }

  c_sum(SD->f1.b_z1, SD->f1.dv0);
  st.site = &l_emlrtRSI;
  b_power(&st, SD->f1.dv0, result->Vm);
  emlrtMEXProfilingStatement(54, isMexOutdated);
  memcpy(&result->V[0], &SD->f1.Anz[0], 10864050U * sizeof(real_T));
  emlrtMEXProfilingStatement(55, isMexOutdated);

  /*  Use speed information to determine the walking section */
  /* 	try */
  /* 		result.FF_walking = detect_walking_section(result); */
  /* 	catch */
  /* 		result.FF_walking = result.FF;  */
  /* 	end; */
  /* 	figure, */
  /* 	plot(t,V,t,Vm,'k'); */
  /* 	hold on;grid on;ylabel('V [m/s]');xlabel('time [s]'); */
  /* 	plot(t(FF),Vm(FF),'*g'); */
  /* 	plot(t(result.FF_walking),Vm(result.FF_walking),'.k'); */
  /* 	legend('Vx','Vy','Vz','|V|','Footfall','Footfall during walk');  */
  /* 	hold off; */
  /*  Compute and plot positions */
  emlrtMEXProfilingStatement(56, isMexOutdated);
  for (k = 0; k < 3; k++) {
    for (loop_ub = 0; loop_ub < 3621349; loop_ub++) {
      ns = loop_ub + 3621350 * k;
      last_footfall = ns + 1;
      SD->f1.Anz[last_footfall] += SD->f1.Anz[ns];
    }
  }

  for (k = 0; k < 10864050; k++) {
    result->P[k] = SD->f1.Anz[k] * PERIOD;
  }

  emlrtMEXProfilingStatement(57, isMexOutdated);
  emlrtMEXProfilingStatement(58, isMexOutdated);
  emlrtMEXProfilingFunctionExit(isMexOutdated);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

/* End of code generation (compute_pos2.c) */
