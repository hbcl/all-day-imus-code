/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_compute_pos_two_imus2_api.c
 *
 * Code generation for function '_coder_compute_pos_two_imus2_api'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "compute_pos_two_imus2.h"
#include "_coder_compute_pos_two_imus2_api.h"
#include "compute_pos_two_imus2_emxutil.h"
#include "compute_pos_two_imus2_data.h"

/* Variable Definitions */
static emlrtRTEInfo ic_emlrtRTEI = { 1,/* lineNo */
  1,                                   /* colNo */
  "_coder_compute_pos_two_imus2_api",  /* fName */
  ""                                   /* pName */
};

/* Function Declarations */
static real_T (*b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[10864050];
static const mxArray *b_emlrt_marshallOut(const emxArray_real_T *u);
static real_T c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *PERIOD,
  const char_T *identifier);
static const mxArray *c_emlrt_marshallOut(const real_T u[10864050]);
static real_T d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId);
static real_T (*e_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *periodic_section, const char_T *identifier))[3621350];
static real_T (*emlrt_marshallIn(const emlrtStack *sp, const mxArray *leftWb,
  const char_T *identifier))[10864050];
static const mxArray *emlrt_marshallOut(const struct0_T *u);
static real_T (*f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[3621350];
static real_T (*g_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[10864050];
static real_T h_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId);
static real_T (*i_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[3621350];

/* Function Definitions */
static real_T (*b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[10864050]
{
  real_T (*y)[10864050];
  y = g_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}
  static const mxArray *b_emlrt_marshallOut(const emxArray_real_T *u)
{
  const mxArray *y;
  const mxArray *m4;
  real_T *pData;
  int32_T i6;
  int32_T i;
  y = NULL;
  m4 = emlrtCreateNumericArray(2, *(int32_T (*)[2])u->size, mxDOUBLE_CLASS,
    mxREAL);
  pData = emlrtMxGetPr(m4);
  i6 = 0;
  i = 0;
  while (i < u->size[1]) {
    for (i = 0; i < u->size[0]; i++) {
      pData[i6] = u->data[i];
      i6++;
    }

    i = 1;
  }

  emlrtAssign(&y, m4);
  return y;
}

static real_T c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *PERIOD,
  const char_T *identifier)
{
  real_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = d_emlrt_marshallIn(sp, emlrtAlias(PERIOD), &thisId);
  emlrtDestroyArray(&PERIOD);
  return y;
}

static const mxArray *c_emlrt_marshallOut(const real_T u[10864050])
{
  const mxArray *y;
  const mxArray *m5;
  static const int32_T iv8[2] = { 3621350, 3 };

  real_T *pData;
  int32_T i7;
  int32_T i;
  int32_T b_i;
  y = NULL;
  m5 = emlrtCreateNumericArray(2, iv8, mxDOUBLE_CLASS, mxREAL);
  pData = emlrtMxGetPr(m5);
  i7 = 0;
  for (i = 0; i < 3; i++) {
    for (b_i = 0; b_i < 3621350; b_i++) {
      pData[i7] = u[b_i + 3621350 * i];
      i7++;
    }
  }

  emlrtAssign(&y, m5);
  return y;
}

static real_T d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId)
{
  real_T y;
  y = h_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

static real_T (*e_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *periodic_section, const char_T *identifier))[3621350]
{
  real_T (*y)[3621350];
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = f_emlrt_marshallIn(sp, emlrtAlias(periodic_section), &thisId);
  emlrtDestroyArray(&periodic_section);
  return y;
}
  static real_T (*emlrt_marshallIn(const emlrtStack *sp, const mxArray *leftWb,
  const char_T *identifier))[10864050]
{
  real_T (*y)[10864050];
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = b_emlrt_marshallIn(sp, emlrtAlias(leftWb), &thisId);
  emlrtDestroyArray(&leftWb);
  return y;
}

static const mxArray *emlrt_marshallOut(const struct0_T *u)
{
  const mxArray *y;
  static const char * sv0[12] = { "FFstart", "FFend", "is_walking", "Anz", "An",
    "A", "W", "quaternion", "euler", "V", "Vm", "P" };

  const mxArray *b_y;
  const mxArray *m1;
  static const int32_T iv5[1] = { 3621350 };

  real_T *pData;
  int32_T i5;
  int32_T i;
  const mxArray *m2;
  static const int32_T iv6[2] = { 3621350, 4 };

  real_T *b_pData;
  int32_T b_i;
  const mxArray *m3;
  static const int32_T iv7[2] = { 1, 3621350 };

  real_T *c_pData;
  y = NULL;
  emlrtAssign(&y, emlrtCreateStructMatrix(1, 1, 12, sv0));
  emlrtSetFieldR2017b(y, 0, "FFstart", b_emlrt_marshallOut(u->FFstart), 0);
  emlrtSetFieldR2017b(y, 0, "FFend", b_emlrt_marshallOut(u->FFend), 1);
  b_y = NULL;
  m1 = emlrtCreateNumericArray(1, iv5, mxDOUBLE_CLASS, mxREAL);
  pData = emlrtMxGetPr(m1);
  i5 = 0;
  for (i = 0; i < 3621350; i++) {
    pData[i5] = u->is_walking[i];
    i5++;
  }

  emlrtAssign(&b_y, m1);
  emlrtSetFieldR2017b(y, 0, "is_walking", b_y, 2);
  emlrtSetFieldR2017b(y, 0, "Anz", c_emlrt_marshallOut(u->Anz), 3);
  emlrtSetFieldR2017b(y, 0, "An", c_emlrt_marshallOut(u->An), 4);
  emlrtSetFieldR2017b(y, 0, "A", c_emlrt_marshallOut(u->A), 5);
  emlrtSetFieldR2017b(y, 0, "W", c_emlrt_marshallOut(u->W), 6);
  b_y = NULL;
  m2 = emlrtCreateNumericArray(2, iv6, mxDOUBLE_CLASS, mxREAL);
  b_pData = emlrtMxGetPr(m2);
  i5 = 0;
  for (i = 0; i < 4; i++) {
    for (b_i = 0; b_i < 3621350; b_i++) {
      b_pData[i5] = u->quaternion[b_i + 3621350 * i];
      i5++;
    }
  }

  emlrtAssign(&b_y, m2);
  emlrtSetFieldR2017b(y, 0, "quaternion", b_y, 7);
  emlrtSetFieldR2017b(y, 0, "euler", c_emlrt_marshallOut(u->euler), 8);
  emlrtSetFieldR2017b(y, 0, "V", c_emlrt_marshallOut(u->V), 9);
  b_y = NULL;
  m3 = emlrtCreateNumericArray(2, iv7, mxDOUBLE_CLASS, mxREAL);
  c_pData = emlrtMxGetPr(m3);
  i5 = 0;
  for (i = 0; i < 3621350; i++) {
    c_pData[i5] = u->Vm[i];
    i5++;
  }

  emlrtAssign(&b_y, m3);
  emlrtSetFieldR2017b(y, 0, "Vm", b_y, 10);
  emlrtSetFieldR2017b(y, 0, "P", c_emlrt_marshallOut(u->P), 11);
  return y;
}

static real_T (*f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[3621350]
{
  real_T (*y)[3621350];
  y = i_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}
  static real_T (*g_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[10864050]
{
  real_T (*ret)[10864050];
  static const int32_T dims[2] = { 3621350, 3 };

  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 2U, dims);
  ret = (real_T (*)[10864050])emlrtMxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

static real_T h_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId)
{
  real_T ret;
  static const int32_T dims = 0;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 0U, &dims);
  ret = *(real_T *)emlrtMxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

static real_T (*i_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[3621350]
{
  real_T (*ret)[3621350];
  static const int32_T dims[1] = { 3621350 };

  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 1U, dims);
  ret = (real_T (*)[3621350])emlrtMxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}
  void compute_pos_two_imus2_api(compute_pos_two_imus2StackData *SD, const
  mxArray * const prhs[7], int32_T nlhs, const mxArray *plhs[2])
{
  real_T (*leftWb)[10864050];
  real_T (*leftAb)[10864050];
  real_T (*rightWb)[10864050];
  real_T (*rightAb)[10864050];
  real_T PERIOD;
  real_T walking_filter;
  real_T (*periodic_section)[3621350];
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  emlrtHeapReferenceStackEnterFcnR2012b(&st);
  emxInitStruct_struct0_T(&st, &SD->f2.left_walk_info, &ic_emlrtRTEI, true);
  emxInitStruct_struct0_T(&st, &SD->f2.right_walk_info, &ic_emlrtRTEI, true);

  /* Marshall function inputs */
  leftWb = emlrt_marshallIn(&st, emlrtAlias(prhs[0]), "leftWb");
  leftAb = emlrt_marshallIn(&st, emlrtAlias(prhs[1]), "leftAb");
  rightWb = emlrt_marshallIn(&st, emlrtAlias(prhs[2]), "rightWb");
  rightAb = emlrt_marshallIn(&st, emlrtAlias(prhs[3]), "rightAb");
  PERIOD = c_emlrt_marshallIn(&st, emlrtAliasP(prhs[4]), "PERIOD");
  walking_filter = c_emlrt_marshallIn(&st, emlrtAliasP(prhs[5]),
    "walking_filter");
  periodic_section = e_emlrt_marshallIn(&st, emlrtAlias(prhs[6]),
    "periodic_section");

  /* Invoke the target function */
  compute_pos_two_imus2(SD, &st, *leftWb, *leftAb, *rightWb, *rightAb, PERIOD,
                        walking_filter, *periodic_section,
                        &SD->f2.left_walk_info, &SD->f2.right_walk_info);

  /* Marshall function outputs */
  plhs[0] = emlrt_marshallOut(&SD->f2.left_walk_info);
  emxFreeStruct_struct0_T(&SD->f2.left_walk_info);
  if (nlhs > 1) {
    plhs[1] = emlrt_marshallOut(&SD->f2.right_walk_info);
  }

  emxFreeStruct_struct0_T(&SD->f2.right_walk_info);
  emlrtHeapReferenceStackLeaveFcnR2012b(&st);
}

/* End of code generation (_coder_compute_pos_two_imus2_api.c) */
