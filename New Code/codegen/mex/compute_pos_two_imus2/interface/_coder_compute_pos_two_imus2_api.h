/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_compute_pos_two_imus2_api.h
 *
 * Code generation for function '_coder_compute_pos_two_imus2_api'
 *
 */

#ifndef _CODER_COMPUTE_POS_TWO_IMUS2_API_H
#define _CODER_COMPUTE_POS_TWO_IMUS2_API_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "compute_pos_two_imus2_types.h"

/* Function Declarations */
extern void compute_pos_two_imus2_api(compute_pos_two_imus2StackData *SD, const
  mxArray * const prhs[7], int32_T nlhs, const mxArray *plhs[2]);

#endif

/* End of code generation (_coder_compute_pos_two_imus2_api.h) */
