/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_compute_pos_two_imus2_mex.c
 *
 * Code generation for function '_coder_compute_pos_two_imus2_mex'
 *
 */

/* Include files */
#include "compute_pos_two_imus2.h"
#include "_coder_compute_pos_two_imus2_mex.h"
#include "compute_pos_two_imus2_terminate.h"
#include "_coder_compute_pos_two_imus2_api.h"
#include "compute_pos_two_imus2_initialize.h"
#include "compute_pos_two_imus2_data.h"

/* Function Declarations */
static void c_compute_pos_two_imus2_mexFunc(compute_pos_two_imus2StackData *SD,
  int32_T nlhs, mxArray *plhs[2], int32_T nrhs, const mxArray *prhs[7]);

/* Function Definitions */
static void c_compute_pos_two_imus2_mexFunc(compute_pos_two_imus2StackData *SD,
  int32_T nlhs, mxArray *plhs[2], int32_T nrhs, const mxArray *prhs[7])
{
  const mxArray *outputs[2];
  int32_T b_nlhs;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;

  /* Check for proper number of arguments. */
  if (nrhs != 7) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:WrongNumberOfInputs", 5, 12, 7, 4,
                        21, "compute_pos_two_imus2");
  }

  if (nlhs > 2) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:TooManyOutputArguments", 3, 4, 21,
                        "compute_pos_two_imus2");
  }

  /* Call the function. */
  compute_pos_two_imus2_api(SD, prhs, nlhs, outputs);

  /* Copy over outputs to the caller. */
  if (nlhs < 1) {
    b_nlhs = 1;
  } else {
    b_nlhs = nlhs;
  }

  emlrtReturnArrays(b_nlhs, plhs, outputs);
}

void mexFunction(int32_T nlhs, mxArray *plhs[], int32_T nrhs, const mxArray
                 *prhs[])
{
  compute_pos_two_imus2StackData *c_compute_pos_two_imus2StackDat = NULL;
  c_compute_pos_two_imus2StackDat = (compute_pos_two_imus2StackData *)
    emlrtMxCalloc(1, (size_t)1U * sizeof(compute_pos_two_imus2StackData));
  mexAtExit(compute_pos_two_imus2_atexit);

  /* Module initialization. */
  compute_pos_two_imus2_initialize();

  /* Dispatch the entry-point. */
  c_compute_pos_two_imus2_mexFunc(c_compute_pos_two_imus2StackDat, nlhs, plhs,
    nrhs, prhs);

  /* Module termination. */
  compute_pos_two_imus2_terminate();
  emlrtMxFree(c_compute_pos_two_imus2StackDat);
}

emlrtCTX mexFunctionCreateRootTLS(void)
{
  emlrtCreateRootTLS(&emlrtRootTLSGlobal, &emlrtContextGlobal, NULL, 1);
  return emlrtRootTLSGlobal;
}

/* End of code generation (_coder_compute_pos_two_imus2_mex.c) */
