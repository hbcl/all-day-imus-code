/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sum.c
 *
 * Code generation for function 'sum'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "compute_pos_two_imus2.h"
#include "sum.h"

/* Function Definitions */
void b_sum(const emxArray_real_T *x, real_T y[3])
{
  int32_T vlen;
  int32_T i;
  int32_T xpageoffset;
  int32_T k;
  vlen = x->size[0];
  for (i = 0; i < 3; i++) {
    xpageoffset = i * x->size[0];
    y[i] = x->data[xpageoffset];
    for (k = 2; k <= vlen; k++) {
      y[i] += x->data[(xpageoffset + k) - 1];
    }
  }
}

void c_sum(const real_T x[7242700], real_T y[3621350])
{
  int32_T i;
  int32_T xpageoffset;
  for (i = 0; i < 3621350; i++) {
    xpageoffset = i << 1;
    y[i] = x[xpageoffset] + x[xpageoffset + 1];
  }
}

void sum(const real_T x[10864050], real_T y[3621350])
{
  int32_T i;
  int32_T xpageoffset;
  for (i = 0; i < 3621350; i++) {
    xpageoffset = i * 3;
    y[i] = x[xpageoffset];
    y[i] += x[xpageoffset + 1];
    y[i] += x[xpageoffset + 2];
  }
}

/* End of code generation (sum.c) */
