/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * compute_pos_two_imus2_data.c
 *
 * Code generation for function 'compute_pos_two_imus2_data'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "compute_pos_two_imus2.h"
#include "compute_pos_two_imus2_data.h"

/* Variable Definitions */
emlrtCTX emlrtRootTLSGlobal = NULL;
const volatile char_T *emlrtBreakCheckR2012bFlagVar = NULL;
boolean_T isMexOutdated;
const char * c_compute_pos_two_imus2_complet;
const char * compute_pos2_complete_name;
const char * acc_tilt_complete_name;
const char * foot_fall2_complete_name;
const char * kf_tilt1_complete_name;
const char * qua_est_complete_name;
const char * qua2rot_complete_name;
const char * kf_tilt2_complete_name;
const char * qua2eul_complete_name;
const char * eul2qua_complete_name;
const char * zupts_func_complete_name;
emlrtContext emlrtContextGlobal = { true,/* bFirstTime */
  false,                               /* bInitialized */
  131482U,                             /* fVersionInfo */
  NULL,                                /* fErrorFunction */
  "compute_pos_two_imus2",             /* fFunctionName */
  NULL,                                /* fRTCallStack */
  false,                               /* bDebugMode */
  { 2045744189U, 2170104910U, 2743257031U, 4284093946U },/* fSigWrd */
  NULL                                 /* fSigMem */
};

emlrtRSInfo q_emlrtRSI = { 41,         /* lineNo */
  "find",                              /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/elmat/find.m"/* pathName */
};

emlrtRSInfo s_emlrtRSI = { 397,        /* lineNo */
  "find_first_indices",                /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/elmat/find.m"/* pathName */
};

emlrtRSInfo t_emlrtRSI = { 43,         /* lineNo */
  "indexShapeCheck",                   /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/eml/+coder/+internal/indexShapeCheck.m"/* pathName */
};

emlrtRSInfo u_emlrtRSI = { 16,         /* lineNo */
  "asin",                              /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/elfun/asin.m"/* pathName */
};

emlrtRSInfo v_emlrtRSI = { 31,         /* lineNo */
  "applyScalarFunctionInPlace",        /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/eml/+coder/+internal/applyScalarFunctionInPlace.m"/* pathName */
};

emlrtRSInfo w_emlrtRSI = { 21,         /* lineNo */
  "eml_int_forloop_overflow_check",    /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m"/* pathName */
};

emlrtRSInfo x_emlrtRSI = { 55,         /* lineNo */
  "power",                             /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/ops/power.m"/* pathName */
};

emlrtRSInfo y_emlrtRSI = { 64,         /* lineNo */
  "fltpower",                          /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/ops/power.m"/* pathName */
};

emlrtRSInfo cb_emlrtRSI = { 124,       /* lineNo */
  "scalar_float_power",                /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/ops/power.m"/* pathName */
};

emlrtRSInfo vb_emlrtRSI = { 74,        /* lineNo */
  "applyScalarFunction",               /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/eml/+coder/+internal/applyScalarFunction.m"/* pathName */
};

emlrtRSInfo wb_emlrtRSI = { 377,       /* lineNo */
  "find_first_indices",                /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/elmat/find.m"/* pathName */
};

emlrtRSInfo xb_emlrtRSI = { 108,       /* lineNo */
  "diff",                              /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/datafun/diff.m"/* pathName */
};

emlrtRSInfo yb_emlrtRSI = { 106,       /* lineNo */
  "diff",                              /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/datafun/diff.m"/* pathName */
};

emlrtRSInfo ac_emlrtRSI = { 17,        /* lineNo */
  "ifWhileCond",                       /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/eml/+coder/+internal/ifWhileCond.m"/* pathName */
};

emlrtRSInfo bc_emlrtRSI = { 30,        /* lineNo */
  "checkNoNaNs",                       /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/eml/+coder/+internal/ifWhileCond.m"/* pathName */
};

emlrtRSInfo dc_emlrtRSI = { 26,        /* lineNo */
  "cat",                               /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/eml/+coder/+internal/cat.m"/* pathName */
};

emlrtRSInfo ec_emlrtRSI = { 101,       /* lineNo */
  "cat_impl",                          /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/eml/+coder/+internal/cat.m"/* pathName */
};

emlrtRSInfo hc_emlrtRSI = { 17,        /* lineNo */
  "qua2rot",                           /* fcnName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/qua2rot.m"/* pathName */
};

emlrtRSInfo ic_emlrtRSI = { 22,        /* lineNo */
  "qua2rot",                           /* fcnName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/qua2rot.m"/* pathName */
};

emlrtRSInfo jc_emlrtRSI = { 27,        /* lineNo */
  "qua2rot",                           /* fcnName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/qua2rot.m"/* pathName */
};

emlrtRSInfo kc_emlrtRSI = { 45,        /* lineNo */
  "mpower",                            /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/ops/mpower.m"/* pathName */
};

emlrtRSInfo nc_emlrtRSI = { 12,        /* lineNo */
  "qua2eul",                           /* fcnName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/qua2eul.m"/* pathName */
};

emlrtRSInfo oc_emlrtRSI = { 13,        /* lineNo */
  "qua2eul",                           /* fcnName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/qua2eul.m"/* pathName */
};

emlrtRSInfo pc_emlrtRSI = { 14,        /* lineNo */
  "qua2eul",                           /* fcnName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/qua2eul.m"/* pathName */
};

emlrtRSInfo sc_emlrtRSI = { 38,        /* lineNo */
  "ismember",                          /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/ops/ismember.m"/* pathName */
};

emlrtRSInfo tc_emlrtRSI = { 152,       /* lineNo */
  "local_ismember",                    /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/ops/ismember.m"/* pathName */
};

emlrtRSInfo uc_emlrtRSI = { 151,       /* lineNo */
  "local_ismember",                    /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/ops/ismember.m"/* pathName */
};

emlrtRSInfo yc_emlrtRSI = { 290,       /* lineNo */
  "eml_float_colon",                   /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/ops/colon.m"/* pathName */
};

emlrtRTEInfo l_emlrtRTEI = { 33,       /* lineNo */
  6,                                   /* colNo */
  "find",                              /* fName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/elmat/find.m"/* pName */
};

emlrtRTEInfo qc_emlrtRTEI = { 12,      /* lineNo */
  9,                                   /* colNo */
  "sqrt",                              /* fName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/elfun/sqrt.m"/* pName */
};

emlrtRTEInfo tc_emlrtRTEI = { 12,      /* lineNo */
  13,                                  /* colNo */
  "toLogicalCheck",                    /* fName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/eml/+coder/+internal/toLogicalCheck.m"/* pName */
};

emlrtRTEInfo xc_emlrtRTEI = { 17,      /* lineNo */
  15,                                  /* colNo */
  "mean",                              /* fName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/datafun/mean.m"/* pName */
};

emlrtRTEInfo yc_emlrtRTEI = { 13,      /* lineNo */
  9,                                   /* colNo */
  "asin",                              /* fName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/elfun/asin.m"/* pName */
};

/* End of code generation (compute_pos_two_imus2_data.c) */
