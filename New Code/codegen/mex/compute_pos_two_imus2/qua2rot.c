/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * qua2rot.c
 *
 * Code generation for function 'qua2rot'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "compute_pos_two_imus2.h"
#include "qua2rot.h"
#include "compute_pos_two_imus2_data.h"

/* Function Definitions */
void qua2rot(const real_T quaternion[4], real_T rotation_matrix[9])
{
  real_T rotation_matrix_tmp;
  real_T b_rotation_matrix_tmp;
  real_T c_rotation_matrix_tmp;
  real_T d_rotation_matrix_tmp;
  real_T e_rotation_matrix_tmp;
  real_T f_rotation_matrix_tmp;
  real_T g_rotation_matrix_tmp;
  real_T h_rotation_matrix_tmp;
  emlrtMEXProfilingFunctionEntry(qua2rot_complete_name, isMexOutdated);

  /*  Author: Lauro Ojeda, 2003-2015 */
  /*  Bibliography: */
  /*  - David Titterton and John Weston, Strapdown Inertial Navigation Technology, 2nd Edition */
  /*  pp 44 */
  /*  - Ken Shoemake: Quaternions  */
  /*  rotation_matrix = LQ*RQ page 6  */
  /*  This can be multiplied by a vector V in order to rotate it */
  /*  QVQ' = LQ*RQ*V */
  /*  The last column and rows can be ignored because they are either 0 or 1 and are not used  */
  /*  to rotate vectors */
  emlrtMEXProfilingStatement(1, isMexOutdated);
  emlrtMEXProfilingStatement(2, isMexOutdated);
  emlrtMEXProfilingStatement(3, isMexOutdated);
  emlrtMEXProfilingStatement(4, isMexOutdated);
  emlrtMEXProfilingStatement(6, isMexOutdated);
  rotation_matrix_tmp = quaternion[0] * quaternion[0];
  b_rotation_matrix_tmp = quaternion[1] * quaternion[1];
  c_rotation_matrix_tmp = quaternion[2] * quaternion[2];
  d_rotation_matrix_tmp = quaternion[3] * quaternion[3];
  rotation_matrix[0] = ((rotation_matrix_tmp + b_rotation_matrix_tmp) -
                        c_rotation_matrix_tmp) - d_rotation_matrix_tmp;
  emlrtMEXProfilingStatement(7, isMexOutdated);
  e_rotation_matrix_tmp = quaternion[1] * quaternion[2];
  f_rotation_matrix_tmp = quaternion[0] * quaternion[3];
  rotation_matrix[3] = 2.0 * (e_rotation_matrix_tmp - f_rotation_matrix_tmp);
  emlrtMEXProfilingStatement(8, isMexOutdated);
  g_rotation_matrix_tmp = quaternion[1] * quaternion[3];
  h_rotation_matrix_tmp = quaternion[0] * quaternion[2];
  rotation_matrix[6] = 2.0 * (g_rotation_matrix_tmp + h_rotation_matrix_tmp);
  emlrtMEXProfilingStatement(9, isMexOutdated);
  rotation_matrix[1] = 2.0 * (e_rotation_matrix_tmp + f_rotation_matrix_tmp);
  emlrtMEXProfilingStatement(10, isMexOutdated);
  rotation_matrix_tmp -= b_rotation_matrix_tmp;
  rotation_matrix[4] = (rotation_matrix_tmp + c_rotation_matrix_tmp) -
    d_rotation_matrix_tmp;
  emlrtMEXProfilingStatement(11, isMexOutdated);
  b_rotation_matrix_tmp = quaternion[2] * quaternion[3];
  e_rotation_matrix_tmp = quaternion[0] * quaternion[1];
  rotation_matrix[7] = 2.0 * (b_rotation_matrix_tmp - e_rotation_matrix_tmp);
  emlrtMEXProfilingStatement(12, isMexOutdated);
  rotation_matrix[2] = 2.0 * (g_rotation_matrix_tmp - h_rotation_matrix_tmp);
  emlrtMEXProfilingStatement(13, isMexOutdated);
  rotation_matrix[5] = 2.0 * (b_rotation_matrix_tmp + e_rotation_matrix_tmp);
  emlrtMEXProfilingStatement(14, isMexOutdated);
  rotation_matrix[8] = (rotation_matrix_tmp - c_rotation_matrix_tmp) +
    d_rotation_matrix_tmp;
  emlrtMEXProfilingFunctionExit(isMexOutdated);
}

/* End of code generation (qua2rot.c) */
