/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * compute_pos_two_imus2_data.h
 *
 * Code generation for function 'compute_pos_two_imus2_data'
 *
 */

#ifndef COMPUTE_POS_TWO_IMUS2_DATA_H
#define COMPUTE_POS_TWO_IMUS2_DATA_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "compute_pos_two_imus2_types.h"

/* Variable Declarations */
extern emlrtCTX emlrtRootTLSGlobal;
extern const volatile char_T *emlrtBreakCheckR2012bFlagVar;
extern boolean_T isMexOutdated;
extern const char * c_compute_pos_two_imus2_complet;
extern const char * compute_pos2_complete_name;
extern const char * acc_tilt_complete_name;
extern const char * foot_fall2_complete_name;
extern const char * kf_tilt1_complete_name;
extern const char * qua_est_complete_name;
extern const char * qua2rot_complete_name;
extern const char * kf_tilt2_complete_name;
extern const char * qua2eul_complete_name;
extern const char * eul2qua_complete_name;
extern const char * zupts_func_complete_name;
extern emlrtContext emlrtContextGlobal;
extern emlrtRSInfo q_emlrtRSI;
extern emlrtRSInfo s_emlrtRSI;
extern emlrtRSInfo t_emlrtRSI;
extern emlrtRSInfo u_emlrtRSI;
extern emlrtRSInfo v_emlrtRSI;
extern emlrtRSInfo w_emlrtRSI;
extern emlrtRSInfo x_emlrtRSI;
extern emlrtRSInfo y_emlrtRSI;
extern emlrtRSInfo cb_emlrtRSI;
extern emlrtRSInfo vb_emlrtRSI;
extern emlrtRSInfo wb_emlrtRSI;
extern emlrtRSInfo xb_emlrtRSI;
extern emlrtRSInfo yb_emlrtRSI;
extern emlrtRSInfo ac_emlrtRSI;
extern emlrtRSInfo bc_emlrtRSI;
extern emlrtRSInfo dc_emlrtRSI;
extern emlrtRSInfo ec_emlrtRSI;
extern emlrtRSInfo hc_emlrtRSI;
extern emlrtRSInfo ic_emlrtRSI;
extern emlrtRSInfo jc_emlrtRSI;
extern emlrtRSInfo kc_emlrtRSI;
extern emlrtRSInfo nc_emlrtRSI;
extern emlrtRSInfo oc_emlrtRSI;
extern emlrtRSInfo pc_emlrtRSI;
extern emlrtRSInfo sc_emlrtRSI;
extern emlrtRSInfo tc_emlrtRSI;
extern emlrtRSInfo uc_emlrtRSI;
extern emlrtRSInfo yc_emlrtRSI;
extern emlrtRTEInfo l_emlrtRTEI;
extern emlrtRTEInfo qc_emlrtRTEI;
extern emlrtRTEInfo tc_emlrtRTEI;
extern emlrtRTEInfo xc_emlrtRTEI;
extern emlrtRTEInfo yc_emlrtRTEI;

#endif

/* End of code generation (compute_pos_two_imus2_data.h) */
