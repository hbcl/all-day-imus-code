/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * abs.c
 *
 * Code generation for function 'abs'
 *
 */

/* Include files */
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "compute_pos_two_imus2.h"
#include "abs.h"
#include "compute_pos_two_imus2_emxutil.h"

/* Variable Definitions */
static emlrtRSInfo ub_emlrtRSI = { 16, /* lineNo */
  "abs",                               /* fcnName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/elfun/abs.m"/* pathName */
};

static emlrtRTEInfo gc_emlrtRTEI = { 16,/* lineNo */
  5,                                   /* colNo */
  "abs",                               /* fName */
  "/Applications/MATLAB_R2019a.app/toolbox/eml/lib/matlab/elfun/abs.m"/* pName */
};

/* Function Definitions */
void b_abs(const real_T x[3621350], real_T y[3621350])
{
  int32_T k;
  for (k = 0; k < 3621350; k++) {
    y[k] = muDoubleScalarAbs(x[k]);
  }
}

void c_abs(const emlrtStack *sp, const emxArray_real_T *x, emxArray_real_T *y)
{
  int32_T nx;
  int32_T k;
  emlrtStack st;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &ub_emlrtRSI;
  nx = x->size[1];
  k = y->size[0] * y->size[1];
  y->size[0] = 1;
  y->size[1] = x->size[1];
  emxEnsureCapacity_real_T(&st, y, k, &gc_emlrtRTEI);
  for (k = 0; k < nx; k++) {
    y->data[k] = muDoubleScalarAbs(x->data[k]);
  }
}

/* End of code generation (abs.c) */
