/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * qua2rot.h
 *
 * Code generation for function 'qua2rot'
 *
 */

#ifndef QUA2ROT_H
#define QUA2ROT_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "compute_pos_two_imus2_types.h"

/* Function Declarations */
extern void qua2rot(const real_T quaternion[4], real_T rotation_matrix[9]);

#endif

/* End of code generation (qua2rot.h) */
