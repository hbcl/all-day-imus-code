/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * qua2eul.c
 *
 * Code generation for function 'qua2eul'
 *
 */

/* Include files */
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "compute_pos_two_imus2.h"
#include "qua2eul.h"
#include "compute_pos_two_imus2_data.h"

/* Function Definitions */
void qua2eul(const emlrtStack *sp, const real_T quaternion[4], real_T euler[3])
{
  real_T the;
  real_T euler_tmp;
  real_T b_euler_tmp;
  real_T c_euler_tmp;
  real_T d_euler_tmp;
  emlrtStack st;
  st.prev = sp;
  st.tls = sp->tls;
  emlrtMEXProfilingFunctionEntry(qua2eul_complete_name, isMexOutdated);

  /*  Author: Lauro Ojeda, 2003-2015 */
  /*  Bibliography: */
  /*  David Titterton and John Weston, Strapdown Inertial Navigation Technology, 2nd Edition */
  /*  pp 45 */
  /*  Converts quaternion to Euler representation */
  emlrtMEXProfilingStatement(1, isMexOutdated);
  emlrtMEXProfilingStatement(2, isMexOutdated);
  emlrtMEXProfilingStatement(3, isMexOutdated);
  emlrtMEXProfilingStatement(4, isMexOutdated);
  emlrtMEXProfilingStatement(5, isMexOutdated);
  emlrtMEXProfilingStatement(6, isMexOutdated);
  st.site = &oc_emlrtRSI;
  the = 2.0 * (quaternion[0] * quaternion[2] - quaternion[3] * quaternion[1]);
  if ((the < -1.0) || (the > 1.0)) {
    emlrtErrorWithMessageIdR2018a(&st, &yc_emlrtRTEI,
      "Coder:toolbox:ElFunDomainError", "Coder:toolbox:ElFunDomainError", 3, 4,
      4, "asin");
  }

  the = muDoubleScalarAsin(the);
  emlrtMEXProfilingStatement(7, isMexOutdated);
  emlrtMEXProfilingStatement(8, isMexOutdated);
  euler_tmp = quaternion[0] * quaternion[0];
  b_euler_tmp = quaternion[1] * quaternion[1];
  c_euler_tmp = quaternion[2] * quaternion[2];
  d_euler_tmp = quaternion[3] * quaternion[3];
  euler[0] = muDoubleScalarAtan2(2.0 * (quaternion[0] * quaternion[1] +
    quaternion[2] * quaternion[3]), ((euler_tmp - b_euler_tmp) - c_euler_tmp) +
    d_euler_tmp);
  euler[1] = the;
  euler[2] = muDoubleScalarAtan2(2.0 * (quaternion[0] * quaternion[3] +
    quaternion[1] * quaternion[2]), ((euler_tmp + b_euler_tmp) - c_euler_tmp) -
    d_euler_tmp);
  emlrtMEXProfilingFunctionExit(isMexOutdated);
}

/* End of code generation (qua2eul.c) */
