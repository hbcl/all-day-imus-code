/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * kf_tilt2.h
 *
 * Code generation for function 'kf_tilt2'
 *
 */

#ifndef KF_TILT2_H
#define KF_TILT2_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "compute_pos_two_imus2_types.h"

/* Function Declarations */
extern void kf_tilt2(const emlrtStack *sp, real_T quaternion[4], real_T
                     accel_theta, real_T accel_phi, real_T is_stationary, real_T
                     *P, const real_T *Q, const real_T *R);

#endif

/* End of code generation (kf_tilt2.h) */
