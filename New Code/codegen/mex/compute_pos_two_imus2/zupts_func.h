/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * zupts_func.h
 *
 * Code generation for function 'zupts_func'
 *
 */

#ifndef ZUPTS_FUNC_H
#define ZUPTS_FUNC_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "compute_pos_two_imus2_types.h"

/* Function Declarations */
extern void zupts_func(const emlrtStack *sp, const real_T An[10864050], real_T
  Anz[10864050], const emxArray_real_T *FFstart, const emxArray_real_T *FFend,
  real_T i, real_T last_footfall);

#endif

/* End of code generation (zupts_func.h) */
