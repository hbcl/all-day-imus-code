/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * compute_pos_two_imus2_terminate.c
 *
 * Code generation for function 'compute_pos_two_imus2_terminate'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "compute_pos_two_imus2.h"
#include "compute_pos_two_imus2_terminate.h"
#include "_coder_compute_pos_two_imus2_mex.h"
#include "compute_pos_two_imus2_data.h"

/* Function Definitions */
void compute_pos_two_imus2_atexit(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexFunctionCreateRootTLS();
  st.tls = emlrtRootTLSGlobal;
  emlrtEnterRtStackR2012b(&st);
  emlrtProfilerUnregisterMEXFcn(c_compute_pos_two_imus2_complet, isMexOutdated);
  emlrtProfilerUnregisterMEXFcn(compute_pos2_complete_name, isMexOutdated);
  emlrtProfilerUnregisterMEXFcn(acc_tilt_complete_name, isMexOutdated);
  emlrtProfilerUnregisterMEXFcn(foot_fall2_complete_name, isMexOutdated);
  emlrtProfilerUnregisterMEXFcn(kf_tilt1_complete_name, isMexOutdated);
  emlrtProfilerUnregisterMEXFcn(qua_est_complete_name, isMexOutdated);
  emlrtProfilerUnregisterMEXFcn(qua2rot_complete_name, isMexOutdated);
  emlrtProfilerUnregisterMEXFcn(kf_tilt2_complete_name, isMexOutdated);
  emlrtProfilerUnregisterMEXFcn(qua2eul_complete_name, isMexOutdated);
  emlrtProfilerUnregisterMEXFcn(eul2qua_complete_name, isMexOutdated);
  emlrtProfilerUnregisterMEXFcn(zupts_func_complete_name, isMexOutdated);
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
  emlrtExitTimeCleanup(&emlrtContextGlobal);
}

void compute_pos_two_imus2_terminate(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
}

/* End of code generation (compute_pos_two_imus2_terminate.c) */
