/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * toLogicalCheck.c
 *
 * Code generation for function 'toLogicalCheck'
 *
 */

/* Include files */
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "compute_pos_two_imus2.h"
#include "toLogicalCheck.h"
#include "compute_pos_two_imus2_data.h"

/* Function Definitions */
void toLogicalCheck(const emlrtStack *sp, real_T x)
{
  if (muDoubleScalarIsNaN(x)) {
    emlrtErrorWithMessageIdR2018a(sp, &tc_emlrtRTEI, "MATLAB:nologicalnan",
      "MATLAB:nologicalnan", 0);
  }
}

/* End of code generation (toLogicalCheck.c) */
