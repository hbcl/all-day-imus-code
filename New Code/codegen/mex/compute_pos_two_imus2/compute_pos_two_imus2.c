/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * compute_pos_two_imus2.c
 *
 * Code generation for function 'compute_pos_two_imus2'
 *
 */

/* Include files */
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "compute_pos_two_imus2.h"
#include "compute_pos2.h"
#include "compute_pos_two_imus2_data.h"

/* Variable Definitions */
static emlrtRSInfo emlrtRSI = { 36,    /* lineNo */
  "compute_pos_two_imus2",             /* fcnName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/compute_pos_two_imus2.m"/* pathName */
};

static emlrtRSInfo b_emlrtRSI = { 39,  /* lineNo */
  "compute_pos_two_imus2",             /* fcnName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/compute_pos_two_imus2.m"/* pathName */
};

/* Function Definitions */
void compute_pos_two_imus2(compute_pos_two_imus2StackData *SD, const emlrtStack *
  sp, const real_T leftWb[10864050], const real_T leftAb[10864050], const real_T
  rightWb[10864050], const real_T rightAb[10864050], real_T PERIOD, real_T
  walking_filter, const real_T periodic_section[3621350], struct0_T
  *left_walk_info, struct0_T *right_walk_info)
{
  real_T T_FF;
  real_T MAX_T_FF;
  emlrtStack st;
  st.prev = sp;
  st.tls = sp->tls;
  emlrtMEXProfilingFunctionEntry(c_compute_pos_two_imus2_complet, isMexOutdated);

  /*  Author: Lauro Ojeda, 2011-2015 */
  /*  same thing as compute_pos_two_imus but without optional arguments */
  /*  	if(~exist('USE_KF','var') || isempty(USE_KF)) */
  /*      end */
  /*  	if(~exist('W_FF','var') || isempty(W_FF)) */
  /*      end */
  /*  	if(~exist('A_FF','var') || isempty(A_FF)) */
  /*      end */
  /*      if(~exist('T_FF','var') || isempty(T_FF)) */
  emlrtMEXProfilingStatement(4, isMexOutdated);
  T_FF = muDoubleScalarFloor(0.4 / PERIOD);

  /*      end */
  /*  	if(~exist('FFL','var') || isempty(FFL)) */
  /*      end */
  /*  	if(~exist('FFR','var') || isempty(FFR)) */
  /*      end */
  /*      if(~exist('walking_filter','var') || isempty(walking_filter)) */
  /*  		walking_filter = 0; */
  /*      end */
  /*      if(~exist('periodic_section','var') || isempty(periodic_section)) */
  /*  		periodic_section = ones(size(leftWb,1),1); */
  /*      end */
  emlrtMEXProfilingStatement(7, isMexOutdated);
  MAX_T_FF = muDoubleScalarFloor(0.8 / PERIOD);

  /* 	%% Compute individual foot paths */
  /*  This is the file containing both feet IMU data */
  /*  Process each foot individually */
  /*  left_walk_info foot */
  emlrtMEXProfilingStatement(9, isMexOutdated);
  st.site = &emlrtRSI;
  compute_pos2(SD, &st, leftWb, leftAb, PERIOD, walking_filter, periodic_section,
               T_FF, MAX_T_FF, left_walk_info);

  /*  right_walk_info foot */
  emlrtMEXProfilingStatement(10, isMexOutdated);
  st.site = &b_emlrtRSI;
  compute_pos2(SD, &st, rightWb, rightAb, PERIOD, walking_filter,
               periodic_section, T_FF, MAX_T_FF, right_walk_info);

  /*  Verify that both IMU's collected the same amount of information */
  emlrtMEXProfilingStatement(13, isMexOutdated);

  /* 	%% Merge/Combine previous FF detection with new one based on cross-velocity */
  /*  Notice that the footfall depneds on information of the opposite foot */
  /* leftFF = zeros(left_samples,1); */
  /* leftFF(left_walk_info.FFstart) = 1; */
  /* leftFF(left_walk_info.FFend) = 1; */
  /* rightFF = zeros(right_samples,1); */
  /* rightFF(right_walk_info.FFstart) = 1; */
  /* rightFF(right_walk_info.FFend) = 1; */
  /* [left_walk_info.FF,left_walk_info.FF_walking,left_walk_info.FF_max_speed] = foot_fall_opposite_velocity(right_walk_info.Vm,leftFF,PERIOD); */
  /* [right_walk_info.FF,right_walk_info.FF_walking,right_walk_info.FF_max_speed] = foot_fall_opposite_velocity(left_walk_info.Vm,rightFF,PERIOD); */
  /* Default 2 */
  /* 	if(abs(length(find(left_walk_info.FF_walking)) - length(find(right_walk_info.FF_walking))) > MAX_NUMBER_STEP_DIFF) */
  /* 		disp('Warning! incompatible number of steps, this needs to be fixed.\nCheck MIN_WALK_SPEED in foot_fall_opp..'); */
  /* 		disp([length(find(left_walk_info.FF_walking)) length(find(right_walk_info.FF_walking))]); */
  /*         left_walk_info.problem = 1; */
  /* 		pause; */
  /* 	end; */
  /* 	%% Recompute accelerations (ZUPT) using combined FFs */
  /*  left_walk_info foot */
  /*  	An = left_walk_info.An; */
  /*  	FFstart = left_walk_info.FFstart; */
  /*      FFend = left_walk_info.FFend; */
  /*  	Anz = zeros(SAMPLES,3); */
  /*  	for(i = 2:SAMPLES)  */
  /*          if(i == 2) */
  /*  	      last_footfall = 0; */
  /*          end */
  /*          Anz = zupts_func(An,Anz,FFstart,FFend,i,last_footfall);  */
  /*          if(ismember(i,FFstart) || ismember(i,FFend)) */
  /*              last_footfall = i; */
  /*          end */
  /*      end */
  /*  	left_walk_info.Az = Anz; */
  /*  	left_walk_info.V = cumsum(Anz)*PERIOD; */
  /*  	left_walk_info.P = cumsum(left_walk_info.V)*PERIOD; */
  /*  	left_walk_info.Vm = (sum((left_walk_info.V.^2)')).^.5; */
  /*  	% right_walk_info foot */
  /*  	An = right_walk_info.An; */
  /*  	FFstart = right_walk_info.FFstart; */
  /*      FFend = right_walk_info.FFend; */
  /*  	Anz = zeros(SAMPLES,3); */
  /*  	for(i = 2:SAMPLES)  */
  /*          if(i == 2) */
  /*  	      last_footfall = 0; */
  /*          end */
  /*          Anz = zupts_func(An,Anz,FFstart,FFend,i,last_footfall);  */
  /*          if(ismember(i,FFstart) || ismember(i,FFend)) */
  /*              last_footfall = i; */
  /*          end */
  /*      end */
  /*  	right_walk_info.Az = Anz; */
  /*  	right_walk_info.V = cumsum(Anz)*PERIOD; */
  /*  	right_walk_info.P = cumsum(right_walk_info.V)*PERIOD; */
  /*  	right_walk_info.Vm = (sum((right_walk_info.V.^2)')).^.5; */
  /* 	%% Make Plots */
  /* figure; */
  /* hold on; */
  /* plot3(left_walk_info.P(left_walk_info.FF,1),left_walk_info.P(left_walk_info.FF,2),left_walk_info.P(left_walk_info.FF,3),'.g'); */
  /* plot3(right_walk_info.P(right_walk_info.FF,1),right_walk_info.P(right_walk_info.FF,2),right_walk_info.P(right_walk_info.FF,3),'.r'); */
  /* legend('left_walk_info','right_walk_info'); */
  /* plot3(left_walk_info.P(:,1),left_walk_info.P(:,2),left_walk_info.P(:,3)); */
  /* plot3(right_walk_info.P(:,1),right_walk_info.P(:,2),right_walk_info.P(:,3)); */
  /* xlabel('X [m]'); ylabel('Y [m]'); zlabel('Z [m]'); */
  /* axis equal; grid on; */
  /* hold off; */
  /* figure; */
  /* sh(1) = subplot(2,1,1);plot(t,left_walk_info.Vm); */
  /* hold on;grid on;ylabel('Left |V| [m/s]');xlabel('time [s]'); */
  /* plot(t(left_walk_info.FF),left_walk_info.Vm(left_walk_info.FF),'g*'); */
  /* plot(t(left_walk_info.FF_walking),left_walk_info.Vm(left_walk_info.FF_walking),'k.'); */
  /* hold off; */
  /* legend('|V|','FFs','Walking'); */
  /* title('Foot Fall detection using opposite shoe speed'); */
  /* sh(2) = subplot(2,1,2);plot(t,right_walk_info.Vm); */
  /* hold on;grid on;ylabel('Right |V| [m/s]');xlabel('time [s]'); */
  /* plot(t(right_walk_info.FF),right_walk_info.V(right_walk_info.FF),'g*'); */
  /* plot(t(right_walk_info.FF_walking),right_walk_info.V(right_walk_info.FF_walking),'k.'); */
  /* hold off; */
  /* linkaxes(sh,'x'); */
  /* if(PLOT_DETAILS) */
  /* 	left_az_mag = (sum((left_walk_info.Az.^2)')).^.5; */
  /* 	right_az_mag = (sum((right_walk_info.Az.^2)')).^.5; */
  /* 	figure; */
  /* 	sh(1) = subplot(2,1,1);plot(t,left_walk_info.Az,t,left_az_mag,'k');hold on;grid on;ylabel('left_walk_info A [m/s^2]');xlabel('time [s]'); */
  /* 	plot(t(left_walk_info.FF),left_walk_info.V(left_walk_info.FF),'g.'); */
  /* 	plot(t(left_walk_info.FF_walking),left_walk_info.V(left_walk_info.FF_walking),'yo'); */
  /* 	sh(2) = subplot(2,1,2);plot(t,right_walk_info.Az,t,right_az_mag,'k');hold on;grid on;ylabel('right_walk_info A [m/s^2]');xlabel('time [s]'); */
  /* 	plot(t(right_walk_info.FF),right_walk_info.V(right_walk_info.FF),'g.'); */
  /* 	plot(t(right_walk_info.FF_walking),right_walk_info.V(right_walk_info.FF_walking),'yo'); */
  /* 	hold off; */
  /* 	linkaxes(sh,'x'); */
  /* 	end; */
  emlrtMEXProfilingFunctionExit(isMexOutdated);
}

/* End of code generation (compute_pos_two_imus2.c) */
