/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * atan2.h
 *
 * Code generation for function 'atan2'
 *
 */

#ifndef ATAN2_H
#define ATAN2_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "compute_pos_two_imus2_types.h"

/* Function Declarations */
extern void b_atan2(const real_T y[3621350], const real_T x[3621350], real_T r
                    [3621350]);

#endif

/* End of code generation (atan2.h) */
