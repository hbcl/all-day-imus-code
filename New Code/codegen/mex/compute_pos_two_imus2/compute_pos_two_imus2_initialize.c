/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * compute_pos_two_imus2_initialize.c
 *
 * Code generation for function 'compute_pos_two_imus2_initialize'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "compute_pos_two_imus2.h"
#include "compute_pos_two_imus2_initialize.h"
#include "_coder_compute_pos_two_imus2_mex.h"
#include "compute_pos_two_imus2_data.h"

/* Function Declarations */
static void compute_pos_two_imus2_once(void);

/* Function Definitions */
static void compute_pos_two_imus2_once(void)
{
  static const int32_T lineInfo[18] = { 5, 8, 11, 14, 17, 20, 29, 30, 36, 39, 42,
    43, 44, 45, 46, 61, 62, 64 };

  static const int32_T b_lineInfo[65] = { 23, 45, 46, 48, 51, 52, 53, 54, 55, 58,
    59, 60, 61, 62, 64, 65, 67, 70, 71, 72, 75, 76, 77, 79, 81, 82, 84, 86, 89,
    94, 96, 97, 98, 99, 100, 101, 105, 106, 107, 108, 109, 110, 111, 112, 114,
    115, 116, 117, 118, 121, 122, 131, 132, 133, 134, 151, 152, 153, 154, 155,
    156, 157, 158, 159, 160 };

  static const int32_T c_lineInfo[9] = { 3, 5, 6, 9, 10, 11, 14, 15, 16 };

  static const int32_T d_lineInfo[122] = { 4, 5, 26, 27, 29, 31, 32, 33, 34, 35,
    36, 37, 38, 39, 41, 42, 43, 44, 45, 46, 47, 48, 49, 79, 80, 81, 83, 89, 95,
    96, 97, 101, 102, 107, 123, 124, 125, 126, 127, 128, 129, 130, 137, 138, 139,
    140, 141, 142, 143, 144, 145, 146, 147, 149, 150, 152, 153, 156, 164, 165,
    168, 183, 184, 185, 190, 191, 193, 194, 195, 196, 197, 198, 199, 200, 201,
    202, 203, 204, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217,
    221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235,
    236, 237, 245, 284, 289, 290, 292, 293, 296, 297, 298, 299, 300, 301, 302,
    303, 344 };

  int32_T i0;
  int32_T iv0[5];
  static const int32_T e_lineInfo[13] = { 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
    21, 22, 24 };

  static const int32_T f_lineInfo[14] = { 12, 13, 14, 15, 16, 17, 18, 19, 21, 22,
    23, 25, 26, 27 };

  static const int32_T g_lineInfo[8] = { 18, 19, 21, 22, 24, 26, 27, 29 };

  static const int32_T h_lineInfo[8] = { 7, 8, 9, 10, 12, 13, 14, 15 };

  int32_T iv1[14];
  static const int32_T i_lineInfo[11] = { 9, 10, 12, 13, 14, 15, 17, 19, 21, 22,
    24 };

  zupts_func_complete_name =
    "/Users/elizabeth/Documents/all-day-imus-code/New Code/zupts_func.m>zupts_func(codegen)";
  eul2qua_complete_name =
    "/Users/elizabeth/Documents/all-day-imus-code/New Code/eul2qua.m>eul2qua(codegen)";
  qua2eul_complete_name =
    "/Users/elizabeth/Documents/all-day-imus-code/New Code/qua2eul.m>qua2eul(codegen)";
  kf_tilt2_complete_name =
    "/Users/elizabeth/Documents/all-day-imus-code/New Code/kf_tilt2.m>kf_tilt2(codegen)";
  qua2rot_complete_name =
    "/Users/elizabeth/Documents/all-day-imus-code/New Code/qua2rot.m>qua2rot(codegen)";
  qua_est_complete_name =
    "/Users/elizabeth/Documents/all-day-imus-code/New Code/qua_est.m>qua_est(codegen)";
  kf_tilt1_complete_name =
    "/Users/elizabeth/Documents/all-day-imus-code/New Code/kf_tilt1.m>kf_tilt1(codegen)";
  foot_fall2_complete_name =
    "/Users/elizabeth/Documents/all-day-imus-code/New Code/foot_fall2.m>foot_fall2(codegen)";
  acc_tilt_complete_name =
    "/Users/elizabeth/Documents/all-day-imus-code/New Code/acc_tilt.m>acc_tilt(codegen)";
  compute_pos2_complete_name =
    "/Users/elizabeth/Documents/all-day-imus-code/New Code/compute_pos2.m>compute_pos2(codegen)";
  c_compute_pos_two_imus2_complet =
    "/Users/elizabeth/Documents/all-day-imus-code/New Code/compute_pos_two_imus2.m>compute_pos_two_imus2(codegen)";
  isMexOutdated = emlrtProfilerCheckMEXOutdated();
  emlrtProfilerRegisterMEXFcn(c_compute_pos_two_imus2_complet,
    "/Users/elizabeth/Documents/all-day-imus-code/New Code/compute_pos_two_imus2.m",
    "compute_pos_two_imus2", 18, lineInfo, isMexOutdated);
  emlrtProfilerRegisterMEXFcn(compute_pos2_complete_name,
    "/Users/elizabeth/Documents/all-day-imus-code/New Code/compute_pos2.m",
    "compute_pos2", 65, b_lineInfo, isMexOutdated);
  emlrtProfilerRegisterMEXFcn(acc_tilt_complete_name,
    "/Users/elizabeth/Documents/all-day-imus-code/New Code/acc_tilt.m",
    "acc_tilt", 9, c_lineInfo, isMexOutdated);
  emlrtProfilerRegisterMEXFcn(foot_fall2_complete_name,
    "/Users/elizabeth/Documents/all-day-imus-code/New Code/foot_fall2.m",
    "foot_fall2", 122, d_lineInfo, isMexOutdated);
  for (i0 = 0; i0 < 5; i0++) {
    iv0[i0] = 12 + i0;
  }

  emlrtProfilerRegisterMEXFcn(kf_tilt1_complete_name,
    "/Users/elizabeth/Documents/all-day-imus-code/New Code/kf_tilt1.m",
    "kf_tilt1", 5, iv0, isMexOutdated);
  emlrtProfilerRegisterMEXFcn(qua_est_complete_name,
    "/Users/elizabeth/Documents/all-day-imus-code/New Code/qua_est.m", "qua_est",
    13, e_lineInfo, isMexOutdated);
  emlrtProfilerRegisterMEXFcn(qua2rot_complete_name,
    "/Users/elizabeth/Documents/all-day-imus-code/New Code/qua2rot.m", "qua2rot",
    14, f_lineInfo, isMexOutdated);
  emlrtProfilerRegisterMEXFcn(kf_tilt2_complete_name,
    "/Users/elizabeth/Documents/all-day-imus-code/New Code/kf_tilt2.m",
    "kf_tilt2", 8, g_lineInfo, isMexOutdated);
  emlrtProfilerRegisterMEXFcn(qua2eul_complete_name,
    "/Users/elizabeth/Documents/all-day-imus-code/New Code/qua2eul.m", "qua2eul",
    8, h_lineInfo, isMexOutdated);
  for (i0 = 0; i0 < 14; i0++) {
    iv1[i0] = 6 + i0;
  }

  emlrtProfilerRegisterMEXFcn(eul2qua_complete_name,
    "/Users/elizabeth/Documents/all-day-imus-code/New Code/eul2qua.m", "eul2qua",
    14, iv1, isMexOutdated);
  emlrtProfilerRegisterMEXFcn(zupts_func_complete_name,
    "/Users/elizabeth/Documents/all-day-imus-code/New Code/zupts_func.m",
    "zupts_func", 11, i_lineInfo, isMexOutdated);
}

void compute_pos_two_imus2_initialize(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexFunctionCreateRootTLS();
  emlrtBreakCheckR2012bFlagVar = emlrtGetBreakCheckFlagAddressR2012b();
  st.tls = emlrtRootTLSGlobal;
  emlrtClearAllocCountR2012b(&st, false, 0U, 0);
  emlrtEnterRtStackR2012b(&st);
  if (emlrtFirstTimeR2012b(emlrtRootTLSGlobal)) {
    compute_pos_two_imus2_once();
  }
}

/* End of code generation (compute_pos_two_imus2_initialize.c) */
