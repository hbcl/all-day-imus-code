/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * asin.c
 *
 * Code generation for function 'asin'
 *
 */

/* Include files */
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "compute_pos_two_imus2.h"
#include "asin.h"
#include "compute_pos_two_imus2_data.h"

/* Function Definitions */
void b_asin(const emlrtStack *sp, emxArray_real_T *x)
{
  boolean_T p;
  int32_T nx;
  int32_T k;
  p = false;
  nx = x->size[0];
  for (k = 0; k < nx; k++) {
    if (p || ((x->data[k] < -1.0) || (x->data[k] > 1.0))) {
      p = true;
    } else {
      p = false;
    }
  }

  if (p) {
    emlrtErrorWithMessageIdR2018a(sp, &yc_emlrtRTEI,
      "Coder:toolbox:ElFunDomainError", "Coder:toolbox:ElFunDomainError", 3, 4,
      4, "asin");
  }

  nx = x->size[0];
  for (k = 0; k < nx; k++) {
    x->data[k] = muDoubleScalarAsin(x->data[k]);
  }
}

void c_asin(const emlrtStack *sp, real_T x[3621350])
{
  boolean_T p;
  int32_T k;
  p = false;
  for (k = 0; k < 3621350; k++) {
    if (p || ((x[k] < -1.0) || (x[k] > 1.0))) {
      p = true;
    } else {
      p = false;
    }
  }

  if (p) {
    emlrtErrorWithMessageIdR2018a(sp, &yc_emlrtRTEI,
      "Coder:toolbox:ElFunDomainError", "Coder:toolbox:ElFunDomainError", 3, 4,
      4, "asin");
  }

  for (k = 0; k < 3621350; k++) {
    x[k] = muDoubleScalarAsin(x[k]);
  }
}

/* End of code generation (asin.c) */
