/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * find.h
 *
 * Code generation for function 'find'
 *
 */

#ifndef FIND_H
#define FIND_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "compute_pos_two_imus2_types.h"

/* Function Declarations */
extern void b_eml_find(const emlrtStack *sp, const boolean_T x[3621349],
  emxArray_int32_T *i);
extern void c_eml_find(const emlrtStack *sp, const emxArray_boolean_T *x,
  emxArray_int32_T *i);
extern void eml_find(const emlrtStack *sp, const boolean_T x[3621350],
                     emxArray_int32_T *i);

#endif

/* End of code generation (find.h) */
