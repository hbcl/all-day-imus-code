/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * kf_tilt1.c
 *
 * Code generation for function 'kf_tilt1'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "compute_pos_two_imus2.h"
#include "kf_tilt1.h"
#include "compute_pos_two_imus2_data.h"

/* Function Definitions */
void kf_tilt1(real_T PERIOD, real_T *Q, real_T *R)
{
  emlrtMEXProfilingFunctionEntry(kf_tilt1_complete_name, isMexOutdated);

  /*  Author: Lauro Ojeda, 2001-2015 */
  /*  used with compute_pos2 */
  /*  Tilt error compensation using accelerometer - based tilt */
  /*  A one state indirect KF (complementary filter) is used for the process and the measurements are applied only on low */
  /*  motion conditions (is_stationary) */
  /*  KF constants */
  /* persistent P Q R */
  /*  Initialize KF */
  /*  Initial covariance error */
  emlrtMEXProfilingStatement(2, isMexOutdated);
  *Q = 1.5E-5 * PERIOD;

  /*  Process (gyros) covariance error */
  emlrtMEXProfilingStatement(3, isMexOutdated);
  *R = 0.15 * PERIOD;

  /*  Measurement (accelerometer) covariance error */
  emlrtMEXProfilingStatement(5, isMexOutdated);
  emlrtMEXProfilingFunctionExit(isMexOutdated);
}

/* End of code generation (kf_tilt1.c) */
