/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * kf_tilt2.c
 *
 * Code generation for function 'kf_tilt2'
 *
 */

/* Include files */
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "compute_pos_two_imus2.h"
#include "kf_tilt2.h"
#include "qua2eul.h"
#include "compute_pos_two_imus2_data.h"

/* Variable Definitions */
static emlrtRSInfo lc_emlrtRSI = { 19, /* lineNo */
  "kf_tilt2",                          /* fcnName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/kf_tilt2.m"/* pathName */
};

static emlrtRSInfo mc_emlrtRSI = { 22, /* lineNo */
  "kf_tilt2",                          /* fcnName */
  "/Users/elizabeth/Documents/all-day-imus-code/New Code/kf_tilt2.m"/* pathName */
};

/* Function Definitions */
void kf_tilt2(const emlrtStack *sp, real_T quaternion[4], real_T accel_theta,
              real_T accel_phi, real_T is_stationary, real_T *P, const real_T *Q,
              const real_T *R)
{
  real_T K;
  real_T euler[3];
  real_T sin_the;
  real_T Att_idx_1;
  real_T cos_phi;
  real_T sin_phi;
  real_T cos_the;
  real_T cos_psi;
  real_T sin_psi;
  real_T d3;
  real_T d4;
  emlrtStack st;
  st.prev = sp;
  st.tls = sp->tls;
  emlrtMEXProfilingFunctionEntry(kf_tilt2_complete_name, isMexOutdated);

  /*  Author: Lauro Ojeda, 2001-2015 */
  /*  used with compute_pos2 */
  /*  Tilt error compensation using accelerometer - based tilt */
  /*  A one state indirect KF (complementary filter) is used for the process and the measurements are applied only on low */
  /*  motion conditions (is_stationary) */
  /*  KF constants */
  /* persistent P Q R */
  /* if(~exist('quaternion','var')) */
  /* 	% Initialize KF */
  /* 	P = 0; % Initial covariance error */
  /* 	Q = 1.5e-5*PERIOD; % Process (gyros) covariance error */
  /* 	R = 1.5e-1*PERIOD; % Measurement (accelerometer) covariance error */
  /* 	quaternion = [1,0,0,0]; */
  /* 	return; */
  /* end; */
  /*  Propagate covariance error */
  emlrtMEXProfilingStatement(1, isMexOutdated);
  *P += *Q;
  emlrtMEXProfilingStatement(2, isMexOutdated);
  st.site = &lc_emlrtRSI;
  if (muDoubleScalarIsNaN(is_stationary)) {
    emlrtErrorWithMessageIdR2018a(&st, &tc_emlrtRTEI, "MATLAB:nologicalnan",
      "MATLAB:nologicalnan", 0);
  }

  if (is_stationary != 0.0) {
    /*  Compute Kalman gain */
    emlrtMEXProfilingStatement(3, isMexOutdated);
    K = *P / (*P + *R);
    emlrtMEXProfilingStatement(4, isMexOutdated);
    st.site = &mc_emlrtRSI;
    qua2eul(&st, quaternion, euler);

    /*  Apply corrections to the state */
    emlrtMEXProfilingStatement(5, isMexOutdated);
    sin_the = euler[0] - (euler[0] - accel_phi) * K;
    Att_idx_1 = euler[1] - (euler[1] - accel_theta) * K;
    emlrtMEXProfilingFunctionEntry(eul2qua_complete_name, isMexOutdated);

    /*  Author: Lauro Ojeda, 2003-2015 */
    /*  Bibliography: */
    /*  Collinson: Introduction to Avionics Systems, 2nd Edition */
    /*  Collinson pp 266 */
    emlrtMEXProfilingStatement(1, isMexOutdated);
    emlrtMEXProfilingStatement(2, isMexOutdated);
    emlrtMEXProfilingStatement(3, isMexOutdated);
    emlrtMEXProfilingStatement(4, isMexOutdated);
    cos_phi = muDoubleScalarCos(sin_the / 2.0);
    emlrtMEXProfilingStatement(5, isMexOutdated);
    sin_phi = muDoubleScalarSin(sin_the / 2.0);
    emlrtMEXProfilingStatement(6, isMexOutdated);
    cos_the = muDoubleScalarCos(Att_idx_1 / 2.0);
    emlrtMEXProfilingStatement(7, isMexOutdated);
    sin_the = muDoubleScalarSin(Att_idx_1 / 2.0);
    emlrtMEXProfilingStatement(8, isMexOutdated);
    cos_psi = muDoubleScalarCos(euler[2] / 2.0);
    emlrtMEXProfilingStatement(9, isMexOutdated);
    sin_psi = muDoubleScalarSin(euler[2] / 2.0);
    emlrtMEXProfilingStatement(10, isMexOutdated);
    emlrtMEXProfilingStatement(11, isMexOutdated);
    emlrtMEXProfilingStatement(12, isMexOutdated);
    emlrtMEXProfilingStatement(13, isMexOutdated);
    emlrtMEXProfilingStatement(14, isMexOutdated);
    d3 = cos_phi * cos_the;
    d4 = sin_phi * sin_the;
    quaternion[0] = d3 * cos_psi + d4 * sin_psi;
    Att_idx_1 = cos_phi * sin_the;
    sin_the = sin_phi * cos_the;
    quaternion[1] = sin_the * cos_psi - Att_idx_1 * sin_psi;
    quaternion[2] = Att_idx_1 * cos_psi + sin_the * sin_psi;
    quaternion[3] = d3 * sin_psi - d4 * cos_psi;
    emlrtMEXProfilingFunctionExit(isMexOutdated);

    /*  Update covariance error */
    emlrtMEXProfilingStatement(6, isMexOutdated);
    *P = (1.0 - K) * *P * (1.0 - K) + K * *R * K;
    emlrtMEXProfilingStatement(7, isMexOutdated);
  }

  emlrtMEXProfilingStatement(8, isMexOutdated);
  emlrtMEXProfilingFunctionExit(isMexOutdated);
}

/* End of code generation (kf_tilt2.c) */
