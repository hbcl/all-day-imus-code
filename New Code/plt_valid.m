function [ output_args ] = plt_valid( strides )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
j = 1;
figure;hist(strides.clearance.value*100,50);
xlabel('virtual clearance (cm)')
title('This histogram shows the distribution of virtual ground clearance (raw)')


for i = 1:length(strides.clearance.value)
   if strides.clearance.valid(i) == 1
       clr(j) = strides.clearance.value(i);
       j = j+1;
   end
end
figure;hist(clr*100,50);
xlabel('virtual clearance (cm)')
title('This histogram shows the distribution of virtual ground clearance (filtered)')
end

