function [ clearance,index,valid ] = plt_vgc_strides( strides, period,IF_PLOT,IF_FULTER,IF_DEBUG)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    if ~exist('IF_PLOT','var') 
		IF_PLOT=1;
    end;
    if ~exist('IF_FULTER','var')
		IF_FULTER=0;
    end;
    if ~exist('IF_DEBUG','var')
		IF_DEBUG=0;
    end;
	N=length(strides.time);
    index=zeros(1,N);
    clearance=zeros(1,N);
    valid=ones(1,N);
    time=zeros(1,N);
    for ii=1:N
        try
        [clearance(ii), index(ii),time(ii),valid(ii)]=findclearance(strides.frwd(:,ii),strides.elev(:,ii),period);
        catch
            error_str=sprintf('finding clearance error in stride %d',ii);
            disp(error_str);
            return
        end
        if IF_DEBUG
            [clearance(ii), index(ii),time(ii),valid(ii)]=findclearance(strides.frwd(:,ii),strides.elev(:,ii),period,1);
            pause();
        end
            
    
    end

    if IF_FULTER
        clearance=arrayregulator(clearance,2);
    end
    
    if IF_PLOT
        figure;
        tmp=clearance(valid==1);
        hist(tmp,'BinWidth',0.002,'Normalization','probability');
        hold on;
        x=xlim;
        x=linspace(x(1),x(2),1000);
        [mu,sigma]=normfit(tmp);
        plot(x,normpdf(x,mu,sigma)*0.002,'LineWidth',4);
        hold off;
        xlabel('ground clearance (m)');
        ylabel('frequency');
        title('Vitural Ground Clearance');
        my_text=sprintf('mean=%f cm;\n std=%d cm.',mu,sigma);
        text(mu+sigma,0.05,my_text);

    end
        


end

