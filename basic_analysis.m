close all


addpath('New Code');
% Two feet analysis
% Deal with the ".h5" file, using the following function to load the data
files_left = '/Users/elizabeth/Documents/short_walk_ramp/20200703-094751_short_ramp_left2.h5';
files_right = '/Users/elizabeth/Documents/short_walk_ramp/20200703-094754_short_ramp_right2.h5';
[LeftWb,LeftAb,RightWb,RightAb,PERIOD] = sync_New_apdm(files_left, files_right);


% Look at the data to make sure:
%   1. During the static period, the data should lay on the x-axis.
%   2. if not, we should add "BIAS" part in "getdata function"
% choose the proper section that you wanna analyze from the previous plot
% note: Unit of SECTION is sec.

%pause;
Begin = input('Enter where the section begins (sec)--> ');
End =  input('Enter where the section ends (sec)--> ');
%Begin = 4605;
%End = 4640;
SECTION = [Begin End];
BIAS = [];

[LeftWb,LeftAb] = getdata(LeftWb,LeftAb,PERIOD,SECTION,BIAS);
[RightWb,RightAb] = getdata(RightWb,RightAb,PERIOD,SECTION,BIAS);

Filter = 0;
walking_filter = 0;
if walking_filter
  periodic_section = find_walking_sections(LeftWb,LeftAb,RightWb,RightAb,WaistWb,WaistAb,PERIOD);
  periodic_section = merge_walking_sections(periodic_section,4/PERIOD);
else
  periodic_section = ones(length(LeftAb),1);    
end
[left_walk_info,right_walk_info] = compute_pos_two_imus(LeftWb,LeftAb,RightWb,RightAb,PERIOD,walking_filter,periodic_section);
  
left_strides= stride_segmentation(left_walk_info,PERIOD,Filter);
right_strides= stride_segmentation(right_walk_info,PERIOD,Filter);

% plot the strides
plt_ltrl_frwd_strides(left_strides);
plt_ltrl_frwd_strides(right_strides);
plt_frwd_elev_strides(left_strides);
plt_frwd_elev_strides(right_strides);




