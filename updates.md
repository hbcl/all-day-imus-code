Here's a list of the modification made to the analysis code, mostly to facilitate all-day imu use where we don't know exactly what the subject is doing

For specific changes in the code, see commit c466d9c


modifications made because of walking_filter:
FF (array showing where footfalls are located) becomes FFstart and FFend
periodic_section = array of 1's and 0's to show which sections are considered walking
walking_filter = 1 or 0 to determine if the walking_filter is implemented

compute_pos.m: 
made plot optional
changes to implement walking filter

compute_pos_two_imus.m:
changes to implement walking filter

get_steps -> get_steps2: 
previously used average direction of +-3 nearest steps, now only use steps that are within the same walking section.

sync_New_apdm -> sync_New_apdm_with_waist: 
made a new function that syncs three IMUs (left foot, right foot, waist).

plt_ltrl_frwd_strides.m:
added axis limits, equal axis

stride_segmentation.m:
changed how swing start and swing finished are set because of walking filter
removed the part where it eliminates steps that are too long

getdata: 
added an option to include in the plot points where messages were recorded. 
Added an option to put the plot in hours instead of seconds.
Now pass another argument to the function to make the plot optional.
There was previously an issue with how BIAS was used.

footfall: 
walking filter
removed time limit for steps 
changed where the footfall position is set for the first and last steps



