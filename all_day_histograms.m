% make some plots based on whole day activity

%% clean up everything
clear all;close all;

%% Load the raw data


%files_left = '/Users/elizabeth/Documents/auto-osman/summer_students/20200722-124919_left_3.h5';
%files_right = '/Users/elizabeth/Documents/auto-osman/summer_students/20200722-124923_right_3.h5';

files_combined = '/Users/elizabeth/Documents/all-day-imus-code/multi-day/kid-pilot/20220331-142142_6MWT_Pilot.h5';

%save_dir = 'all_day4-plots/test/'; % where to save the plots
walking_filter = 1; % 1 or 0 filter to look only at walking-like movement

%[LeftWb,LeftAb,RightWb,RightAb,PERIOD] = sync_New_apdm(files_left,files_right);
[LeftWb,LeftAb,LeftMb,RightWb,RightAb,RightMb,PERIOD] = sync_New_apdm_combined(files_combined);

%% define the target section
Begin = 1;
End = size(LeftAb,1)*PERIOD; 

BIAS = []; 


SECTION1 = [Begin End];
[LeftWb,LeftAb] = getdata(LeftWb,LeftAb,PERIOD,SECTION1,BIAS,[]);
[RightWb,RightAb] = getdata(RightWb,RightAb,PERIOD,SECTION1,BIAS,[]);
%[WaistWb,WaistAb] = getdata(WaistWb,WaistAb,PERIOD,SECTION1,BIAS,[]);


%% compute strides
% filter eliminate strange strides. e.g. too fast, too slow, too long, or
% too short strides.
%Filter = input('Do you want to turn on the filter, if yes, type 1, otherwise type 0--> ');
Filter = 0;
if walking_filter
periodic_section = find_walking_sections(LeftWb,LeftAb,RightWb,RightAb,[],[],PERIOD);
periodic_section = merge_walking_sections(periodic_section,2/PERIOD); % merge walking sections less than 2 seconds apart
figure('Renderer', 'painters', 'Position', [10 10 1000 200]);
N=size(LeftWb,1);
t=(1:N)*PERIOD;
imagesc(t,[1],periodic_section');
else
periodic_section = ones(length(LeftAb),1);    
end
[left_walk_info,right_walk_info] = compute_pos_two_imus(LeftWb,LeftAb,RightWb,RightAb,PERIOD,walking_filter,periodic_section);
left_strides= stride_segmentation_tmp(left_walk_info,PERIOD,Filter);
right_strides= stride_segmentation_tmp(right_walk_info,PERIOD,Filter);

if walking_filter

% now do everything again with the dtw step detection
% dtw_template_nstrides makes the template based on first 50 strides
% dtw_template uses all strides from longest walking section
%[tlw1,tlw2,tlw3,tla1,tla2,tla3] = dtw_template_nstrides(LeftWb,LeftAb,left_strides,PERIOD);
%[trw1,trw2,trw3,tra1,tra2,tra3] = dtw_template_nstrides(RightWb,RightAb,right_strides,PERIOD);
[tlw1,tlw2,tlw3,tla1,tla2,tla3] = dtw_template(LeftWb,LeftAb,left_strides,PERIOD);
[trw1,trw2,trw3,tra1,tra2,tra3] = dtw_template(RightWb,RightAb,right_strides,PERIOD);


  [left_section,right_section,threshl,threshr] = dtw_walking_section(LeftWb,LeftAb,left_strides,RightWb,RightAb,right_strides,PERIOD,1,size(LeftAb,1),tlw1,tlw2,tlw3,tla1,tla2,tla3,trw1,trw2,trw3,tra1,tra2,tra3);

  section_full = left_section | right_section;
  figure
  imagesc(t,[1],section_full');
  [left_walk_info,right_walk_info] = compute_pos_two_imus(LeftWb,LeftAb,RightWb,RightAb,PERIOD,1,section_full);
  left_strides= stride_segmentation_tmp(left_walk_info,PERIOD,Filter);
  right_strides= stride_segmentation_tmp(right_walk_info,PERIOD,Filter);
  
end
%% plot the strides
left_strides.length = left_strides.frwd(end,:);
plt_ltrl_frwd_strides(left_strides);
right_strides.length = right_strides.frwd(end,:);
plt_ltrl_frwd_strides(right_strides);

plt_frwd_elev_strides(left_strides);
plt_frwd_elev_strides(right_strides);
  
  
% histogram showing stride length
figure;
histogram(left_strides.frwd(end,:));
ylabel('Frequency');xlabel('Forward step length [m]');
title('Left strides');

figure;
histogram(right_strides.frwd(end,:));
ylabel('Frequency');xlabel('Forward step length [m]');
title('Right strides');



% walking speed
figure;
histogram(left_strides.frwd_speed);
ylabel('Frequency');
xlabel('Step forward velocity');
title('Left strides');

%figure;
histogram(right_strides.frwd_speed);
ylabel('Frequency');
xlabel('Step forward velocity');
title('Right strides');

% stride length vs. speed
figure;
plot(left_strides.frwd(end,:),left_strides.frwd_speed,'Marker','.','LineStyle','none')
ylabel('Step forward velocity');
xlabel('Forward step length');
title('Left strides');

figure;
plot(right_strides.frwd(end,:),right_strides.frwd_speed,'Marker','.','LineStyle','none')
ylabel('Step forward velocity');
xlabel('Forward step length');
title('Right strides');

%% saving all figures to current directory
%h = get(0,'children');
%for i=1:length(h)
%   saveas(h(i), [strcat(save_dir,'figure') num2str(i)], 'fig');  % this function saves every figure in .fig type. you can open this figure in matlab
%  saveas(h(i), [strcat(save_dir,'figure') num2str(i)], 'png'); % this function saves every figure in .png type.
%end
%% info about strides in each bout (use it for whole day data)
% Note! this function is time consuming!
%[left_strides.Section, left_strides.Hist]= data_divider(LeftWb,LeftAb,PERIOD);

%% saving all variables
%save(strcat(save_dir,'left_strides.mat'),'PERIOD','left_strides','LeftWb','LeftAb','SECTION1'); %this function saves all the useful variables for future review.
%save(strcat(save_dir,'right_strides.mat'),'PERIOD','right_strides','RightWb','RightAb','SECTION1'); 
%save(strcat(save_dir,'waist.mat'),'PERIOD','WaistWb','WaistAb','SECTION1'); 



