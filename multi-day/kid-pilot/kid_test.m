% make some plots based on whole day activity

addpath('/Users/elizabeth/Documents/all-day-imus-code/New Code');
addpath('/Users/elizabeth/Documents/all-day-imus-code');
addpath('/Users/elizabeth/Documents/all-day-imus-code/animator');

%% Load the raw data
clear all;close all;
nsubjects = 1;
startsubject = 2;
ndays = 1;
startday = 1;
walking_filter = 1; % 1 or 0 filter to look only at walking-like movement
base_dir = '/Users/elizabeth/Documents/all-day-imus-code/multi-day/kid-pilot/';

disthistleft = zeros(50,(nsubjects-startsubject+1)*(ndays-startday+1));
disthistright = zeros(50,(nsubjects-startsubject+1)*(ndays-startday+1));
stephistleft = zeros(50,(nsubjects-startsubject+1)*(ndays-startday+1));
stephistright = zeros(50,(nsubjects-startsubject+1)*(ndays-startday+1));

% bias is time period with very little movement
% this will have to be set manually
%biasarr{1} = [100 150]; 
%biasarr{2} = [1750 1800];
%biasarr{3} = [7000 7020]; 
BIAS = [];

for isub = startsubject:nsubjects+startsubject-1
subfolder = strcat('subject',int2str(isub));
for iday = startday:ndays+startday-1
itrial = (isub-startsubject)*(ndays-startday+1)+(iday-startday+1);
sync_required = 1; % this is set if left, right, and waist IMU are separate files    
% look for files in folders labeled day1, day2, ...
datafolder = strcat('day',int2str(iday));
lefts = dir(fullfile(base_dir,subfolder,datafolder,'*_left*'));
rights = dir(fullfile(base_dir,subfolder,datafolder,'*_right*'));
waists = dir(fullfile(base_dir,subfolder,datafolder,'*_waist*'));


if (size(lefts,1) > 1)
    disp('Multiple h5 files found for left foot')
    for i=1:size(lefts,1)
       disp(fullfile(lefts(i).folder,lefts(i).name)) 
    end
    file_left = fullfile(lefts(1).folder,lefts(1).name);
%elseif (size(lefts,1) < 1)
%    error('No file found for left foot')
elseif (size(lefts,1) == 1)
    file_left = fullfile(lefts.folder,lefts.name);
    disp('One file found for left foot')
    disp(file_left)
end 

if (size(rights,1) > 1)
    disp('Multiple h5 files found for right foot')
    for i=1:size(rights,1)
       disp(fullfile(rights(i).folder,rights(i).name)) 
    end
    file_right = fullfile(rights(1).folder,rights(1).name);
%elseif (size(rights,1) < 1)
%    error('No file found for right foot')
elseif (size(rights,1) == 1)
    file_right = fullfile(rights.folder,rights.name);
    disp('One file found for right foot')
    disp(file_right)
end 

if (size(waists,1) > 1)
    disp('Multiple h5 files found for waist')
    for i=1:size(waists,1)
       disp(fullfile(waists(i).folder,waists(i).name)) 
    end
    file_waist = fullfile(waists(1).folder,waists(1).name);
%elseif (size(waists,1) < 1)
%    error('No file found for waist')
elseif (size(waists,1) == 1)
    file_waist = fullfile(waists.folder,waists.name);
    disp('One file found for waist')
    disp(file_waist)
end 


if (size(waists,1) < 1 && size(rights,1) < 1 && size(lefts,1) < 1)
    % look for single file with all IMUs combined
    combined_file = dir(fullfile(base_dir,subfolder,datafolder,'*.h5'));
    sync_required = 0;
    nfiles = length(combined_file);
    if nfiles < 1
       error('No IMU file found') 
    end
    disp([num2str(nfiles) 'file(s) found' ]);
    for i=1:nfiles
       disp(fullfile(combined_file(i).folder,combined_file(i).name)) 
    end
    
else
  nfiles = length(waists);
  use_waist = 1;
  if ~(length(waists) == length(lefts) && length(lefts) == length(rights) && length(waists) == length(rights))
    warning('Unequal number of files for waist, right foot, and left foot')
    nfiles = min([length(waists) length(lefts) length(rights)]);
    if isempty(waists)
       nfiles = min( [length(lefts) length(rights)]);
       use_waist = 0;
    end
  end    
end


% figures that will be used to display step histograms
stepsperwalkingbout = figure('Renderer', 'painters', 'Position', [10 10 1400 400]);
stepsperwalkingboutright = figure('Renderer', 'painters', 'Position', [10 10 1400 400]);
stridelengthvsspeed = figure;
stridelengthvsspeedright = figure;
stepsperwalkgboutsmall = figure;
stepsperwalkingboutsmallright = figure;
diststepcount = figure;
diststepcountsmall = figure;
diststepcountright = figure;
diststepcountsmallright = figure;
footheading = figure;
footheadingright = figure;
polarheading = figure;
polarheadingright = figure;


for ifile=1:nfiles
  if (sync_required)
    if use_waist
      file_left = fullfile(lefts(ifile).folder,lefts(ifile).name);
      file_right = fullfile(rights(ifile).folder,rights(ifile).name);
      file_waist = fullfile(waists(ifile).folder,waists(ifile).name);

      [LeftWb,LeftAb,RightWb,RightAb,WaistWb,WaistAb,PERIOD] = sync_New_apdm_with_waist(file_left,file_right,file_waist);
      
    else % no waist file found
      file_left = fullfile(lefts(ifile).folder,lefts(ifile).name);
      file_right = fullfile(rights(ifile).folder,rights(ifile).name);  
      [LeftWb,LeftAb,RightWb,RightAb,PERIOD,LeftMb,RightMb, static_period] = sync_New_apdm(file_left,file_right,1); 
      WaistWb = [];
      WaistAb = [];
    end % if use_waist

  else
    combined_filename = fullfile(combined_file(ifile).folder,combined_file(ifile).name);
    [LeftWb,LeftAb,LeftMb,RightWb,RightAb,RightMb,PERIOD,WaistWb,WaistAb,WaistMb,HandWb,HandAb,HandMb] = sync_New_apdm_combined(combined_filename);
  end % if sync required

% prompt to manually enter bias for this day

%BIAS = input('Enter the bias in seconds (period of time with no movement) ');
%BIAS = biasarr{itrial};

%% define the target section
Begin = 1;
End = size(LeftAb,1)*PERIOD; 
%BIAS = biasarr{itrial};

%Begin = 17000;
%End = 19000;
        
SECTION1 = [Begin End];

[LeftWb,LeftAb] = getdata(LeftWb,LeftAb,PERIOD,SECTION1,BIAS);
[RightWb,RightAb] = getdata(RightWb,RightAb,PERIOD,SECTION1,BIAS);
if(exist('WaistWb','var') && ~isempty(WaistWb))
  [WaistWb,WaistAb] = getdata(WaistWb,WaistAb,PERIOD,SECTION1,BIAS);
end


%% compute strides
% filter eliminate strange strides. e.g. too fast, too slow, too long, or
% too short strides.
Filter = 0;
if walking_filter
periodic_section = find_walking_sections(LeftWb,LeftAb,RightWb,RightAb,WaistWb,WaistAb,PERIOD);
periodic_section = merge_walking_sections(periodic_section,4/PERIOD);
figure('Renderer', 'painters', 'Position', [10 10 1000 200]);
N=size(LeftWb,1);
t=(1:N)*PERIOD;
%imagesc(t,[1],periodic_section');
else
periodic_section = ones(length(LeftAb),1);    
end
[left_walk_info,right_walk_info] = compute_pos_two_imus(LeftWb,LeftAb,RightWb,RightAb,PERIOD,walking_filter,periodic_section);
left_strides1 = stride_segmentation_tmp(left_walk_info,PERIOD,Filter);
right_strides1 = stride_segmentation_tmp(right_walk_info,PERIOD,Filter);

if isempty(left_strides1) || isempty(right_strides1)
   disp(['No strides found for subject ' num2str(isub) ', day ' num2str(iday) ', file ' num2str(ifile)]);
else
% % now do everything again with the dtw step detection
% [tlw1,tlw2,tlw3,tla1,tla2,tla3] = dtw_template(LeftWb,LeftAb,left_strides1,PERIOD);
% [trw1,trw2,trw3,tra1,tra2,tra3] = dtw_template(RightWb,RightAb,right_strides1,PERIOD);
%   halfind1 = floor(size(LeftAb,1)/3);
%   halfind2 = halfind1*2;
%   left_section_full = zeros(size(LeftAb,1),1);
%   right_section_full = zeros(size(LeftAb,1),1);
%   [left_section,right_section] = dtw_walking_section(LeftWb,LeftAb,left_strides1,RightWb,RightAb,right_strides1,PERIOD,1,halfind1,tlw1,tlw2,tlw3,tla1,tla2,tla3,trw1,trw2,trw3,tra1,tra2,tra3);
%   left_section_full(1:halfind1) = left_section;
%   right_section_full(1:halfind1) = right_section;
%   [left_section,right_section] = dtw_walking_section(LeftWb,LeftAb,left_strides1,RightWb,RightAb,right_strides1,PERIOD,halfind1+1,halfind2,tlw1,tlw2,tlw3,tla1,tla2,tla3,trw1,trw2,trw3,tra1,tra2,tra3);
%   left_section_full(halfind1+1:halfind2) = left_section;
%   right_section_full(halfind1+1:halfind2) = right_section;
%   [left_section,right_section] = dtw_walking_section(LeftWb,LeftAb,left_strides1,RightWb,RightAb,right_strides1,PERIOD,halfind2+1,size(LeftAb,1),tlw1,tlw2,tlw3,tla1,tla2,tla3,trw1,trw2,trw3,tra1,tra2,tra3);
%   left_section_full(halfind2+1:size(LeftAb,1)) = left_section;
%   right_section_full(halfind2+1:size(LeftAb,1)) = right_section;
%   section_full = left_section_full | right_section_full;
%   figure
%   imagesc(t,[1],section_full');
%   [left_walk_info2,right_walk_info2] = compute_pos_two_imus(LeftWb,LeftAb,RightWb,RightAb,PERIOD,walking_filter,section_full);
%   left_strides2= stride_segmentation_tmp(left_walk_info2,PERIOD,Filter);
%   right_strides2= stride_segmentation_tmp(right_walk_info2,PERIOD,Filter);
  
end

if ~isempty(left_strides1)
  left_strides(ifile)= left_strides1;
end
if ~isempty(right_strides1)
  right_strides(ifile)= right_strides1;
end

% if left_strides and right_strides contain more than one struct, 
% merge them together
left_strides = merge_strides(left_strides);
right_strides = merge_strides(right_strides);
  
end

%% plot the strides
left_strides.length = left_strides.frwd(end,:);
plt_ltrl_frwd_strides(left_strides);
right_strides.length = right_strides.frwd(end,:);
plt_ltrl_frwd_strides(right_strides);


% histogram showing stride length
%figure;
%histogram(left_strides.frwd(end,:));
%ylabel('Frequency');xlabel('Forward step length [m]');
%xlim([-3 3]);
%title('Left strides');

%figure;
%histogram(right_strides.frwd(end,:));
%ylabel('Frequency');xlabel('Forward step length [m]');
%xlim([-3 3]);
%title('Right strides');



% walking speed
%figure;
%histogram(left_strides.frwd_speed);
%ylabel('Frequency');
%xlabel('Step forward velocity');
%xlim([-5 5]);
%title('Left strides');

%figure;
%histogram(right_strides.frwd_speed);
%ylabel('Frequency');
%xlabel('Step forward velocity');
%xlim([-5 5]);
%title('Right strides');

% stride length vs. speed
%figure;
%plot(left_strides.frwd(end,:),left_strides.frwd_speed,'Marker','.','LineStyle','none')
%ylabel('Step forward velocity');
%xlabel('Forward step length');
%xlim([-3 3]);
%ylim([-5 5]);
%title('Left strides');

%figure;
%plot(right_strides.frwd(end,:),right_strides.frwd_speed,'Marker','.','LineStyle','none')
%ylabel('Step forward velocity');
%xlabel('Forward step length');
%xlim([-3 3]);
%ylim([-5 5]);
%title('Right strides');

% make figure with three plots
pos1 = [0.4 0.4 0.5 0.5];
pos2 = [0.1 0.4 0.2 0.5];
pos3 = [0.4 0.1 0.5 0.2];
figure(stridelengthvsspeed);
% stride length vs speed
subplot('Position',pos1)
plot(left_strides.frwd_speed,left_strides.frwd(end,:),'Marker','.','LineStyle','none')
xlabel('Step forward velocity');
ylabel('Forward step length');
xlim([-2 2]);
ylim([-2 2]);
title('Left strides');
% speed histogram
subplot('Position',pos2)
%[counts,bins] = hist(left_strides.frwd_speed,20); %nbins = 20
[counts,bins] = hist(left_strides.frwd(end,:),20); %nbins = 20
barh(bins,counts);
xlabel('Frequency')
ylim([-2 2]);
% save stride speed
%left_speed = left_strides.frwd_speed;
%savename = ['results/save_var/subject' num2str(isub) '_day' num2str(iday) '_left_speed.mat'];
%save(savename,'left_speed');
% stride length histogram
subplot('Position',pos3)
histogram(left_strides.frwd_speed,'BinWidth',0.1);
% save stride length to use in summary plots later
%left_frwd = left_strides.frwd(end,:);
%savename = ['results/save_var/subject' num2str(isub) '_day' num2str(iday) '_left_frwd.mat'];
%save(savename,'left_frwd');
ylabel('Frequency');
xlim([-2 2]);

% right foot
figure(stridelengthvsspeedright)
% stride length vs speed
subplot('Position',pos1)
plot(right_strides.frwd(end,:),right_strides.frwd_speed,'Marker','.','LineStyle','none')
ylabel('Step forward velocity');
xlabel('Forward step length');
xlim([-3 3]);
ylim([-5 5]);
title('Right strides');
% speed histogram
subplot('Position',pos2)
[counts,bins] = hist(right_strides.frwd_speed,20);
barh(bins,counts);
xlabel('Frequency')
ylim([-5 5]);
% save stride speed
%right_speed = right_strides.frwd_speed;
%savename = ['results/save_var/subject' num2str(isub) '_day' num2str(iday) '_right_speed.mat'];
%save(savename,'right_speed');
% stride length histogram
subplot('Position',pos3)
histogram(right_strides.frwd(end,:),'BinWidth',0.1);
% save stride length
%right_frwd = right_strides.frwd(end,:);
%savename = ['results/save_var/subject' num2str(isub) '_day' num2str(iday) '_right_frwd.mat'];
%save(savename,'right_frwd');
ylabel('Frequency');
xlim([-3 3]);

% histogram of steps in each walking bout
walking_bout = count_steps_in_walking_bout(left_strides);
figure(stepsperwalkingbout);
hbig=histogram(walking_bout,'BinWidth',1);
xlabel('Steps in each walking bout');
ylabel('Frequency');
title('Left');
xl = xlim;
yl = ylim;
txt = {'Total steps:', num2str(sum(walking_bout))};
text(xl(1) + 0.6*(xl(2)-xl(1)),yl(1) + 0.8*(yl(2)-yl(1)),txt)
% save numbers of steps
%savename = ['results/save_var/subject' num2str(isub) '_day' num2str(iday) '_left_steps.mat'];
%save(savename,'walking_bout');

% same thing but only 1-50 steps
figure(stepsperwalkgboutsmall);
edges = 0:1:50;
h = histogram(walking_bout,edges);
xlabel('Steps in each walking bout');
ylabel('Frequency');
title('Left');
stephistleft(:,itrial) = h.Values./sum(hbig.Values);

% step count histogram right foot
walking_bout = count_steps_in_walking_bout(right_strides);
figure(stepsperwalkingboutright);
hbig = histogram(walking_bout,'BinWidth',1);
xlabel('Steps in each walking bout');
ylabel('Frequency');
title('Right');
xl = xlim;
yl = ylim;
txt = {'Total steps:', num2str(sum(walking_bout))};
text(xl(1) + 0.6*(xl(2)-xl(1)),yl(1) + 0.8*(yl(2)-yl(1)),txt)
%savename = ['results/save_var/subject' num2str(isub) '_day' num2str(iday) '_right_steps.mat'];
%save(savename,'walking_bout');

figure(stepsperwalkingboutsmallright);
h = histogram(walking_bout,edges);
xlabel('Steps in each walking bout');
ylabel('Frequency');
title('Right');
stephistright(:,itrial) = h.Values./sum(hbig.Values);



% histogram of distance in each walking bout
walking_bout_dist = get_distance_in_walking_bout(left_strides);
figure(diststepcount);
hbig=histogram(walking_bout_dist,'BinWidth',10);
xlabel('Distance of each walking bout (m)');
ylabel('Frequency');
title('Left');
xl = xlim;
yl = ylim;
txt = {'Total distance:', num2str(sum(walking_bout_dist))};
text(xl(1) + 0.6*(xl(2)-xl(1)),yl(1) + 0.8*(yl(2)-yl(1)),txt)

% zoom in on 0-100 m
figure(diststepcountsmall)
edges = 0:2:100;
h = histogram(walking_bout_dist,edges);
xlabel('Distance of each walking bout (m)');
ylabel('Frequency');
title('Left');
disthistleft(:,itrial) = h.Values./sum(hbig.Values);

walking_bout_dist = get_distance_in_walking_bout(right_strides);
figure(diststepcountright);
hbig = histogram(walking_bout_dist,'BinWidth',10);
xlabel('Distance of each walking bout (m)');
ylabel('Frequency');
title('Right');
xl = xlim;
yl = ylim;
txt = {'Total distance:', num2str(sum(walking_bout_dist))};
text(xl(1) + 0.6*(xl(2)-xl(1)),yl(1) + 0.8*(yl(2)-yl(1)),txt)

figure(diststepcountsmallright)
h = histogram(walking_bout_dist,edges);
xlabel('Distance of each walking bout (m)');
ylabel('Frequency');
title('Right');
disthistright(:,itrial) = h.Values./sum(hbig.Values);

% foot heading histogram
figure(footheading);
histogram(left_strides.foot_heading)
xlabel('Foot heading (radians)');
ylabel('Frequency');
title('Left strides');

figure(footheadingright);
histogram(right_strides.foot_heading)
xlabel('Foot heading (radians)');
ylabel('Frequency');
title('Right strides');

% save foot heading and x-y position of each step and speed
left_heading = left_strides.foot_heading;
right_heading = right_strides.foot_heading;
left_frwd = left_strides.frwd(end,:);
right_frwd = right_strides.frwd(end,:);
left_ltrl = left_strides.ltrl(end,:);
right_ltrl = right_strides.ltrl(end,:);
left_speed = left_strides.frwd_speed;
right_speed = right_strides.frwd_speed;
left_walking_bout = count_steps_in_walking_bout(left_strides);
right_walking_bout = count_steps_in_walking_bout(right_strides);

%savename = ['results/save_var/subject' num2str(isub) '_day' num2str(iday) '.mat'];
%save(savename,'left_heading','right_heading','left_frwd','right_frwd','left_ltrl','right_ltrl','left_speed','right_speed','left_walking_bout','right_walking_bout');

%figure(polarheading);
%polarhistogram(left_strides.foot_heading)
%title('Left strides')

%figure(polarheadingright);
%polarhistogram(right_strides.foot_heading)
%title('Right strides')


%% saving all figures to current directory
%h = get(0,'children');
%for i=1:length(h)
%   saveas(h(i), [strcat(save_dir,'figure') num2str(i)], 'fig');  % this function saves every figure in .fig type. you can open this figure in matlab
%  saveas(h(i), [strcat(save_dir,'figure') num2str(i)], 'png'); % this function saves every figure in .png type.
%end


% uncomment this part only if you want a big plot for all subjects
%save(strcat(base_dir,'stephistleft.mat'),'stephistleft');
%save(strcat(base_dir,'stephistright.mat'),'stephistright');
%save(strcat(base_dir,'disthistleft.mat'),'disthistleft');
%save(strcat(base_dir,'disthistright.mat'),'disthistright');

%% saving all variables
%save(strcat(save_dir,'left_strides.mat'),'PERIOD','left_strides','LeftWb','LeftAb','SECTION1'); %this function saves all the useful variables for future review.
%save(strcat(save_dir,'right_strides.mat'),'PERIOD','right_strides','RightWb','RightAb','SECTION1'); 
%save(strcat(save_dir,'waist.mat'),'PERIOD','WaistWb','WaistAb','SECTION1'); 

end
end
% 
% if nsubjects==10 % make big plot for all 10 subjects
% 
% load('stephistlefta.mat');
% stephistlefta = stephistleft;
% load('stephistrighta.mat');
% stephistrighta = stephistright;
% load('disthistlefta.mat');
% disthistlefta = disthistleft;
% load('disthistrighta.mat');  
% disthistrighta = disthistright;
% 
% load('stephistleftb.mat');
% stephistleftb = stephistleft;
% load('stephistrightb.mat');
% stephistrightb = stephistright;
% load('disthistleftb.mat');
% disthistleftb = disthistleft;
% load('disthistrightb.mat');  
% disthistrightb = disthistright;
% 
% % merge a and b
% 
% stephistleft = zeros(50,10);
% stephistright = zeros(50,10);
% disthistleft = zeros(50,10);
% disthistright = zeros(50,10);
% 
% stephistleft(:,1:5) = stephistlefta;
% stephistleft(:,6:10) = stephistleftb;
% stephistright(:,1:5) = stephistrighta;
% stephistright(:,6:10) = stephistrightb;
% disthistleft(:,1:5) = disthistlefta;
% disthistleft(:,6:10) = disthistleftb;
% disthistright(:,1:5) = disthistrighta;
% disthistright(:,6:10) = disthistrightb;
% 
% figure
% bar3(stephistleft)
% ylabel('Steps in each walking bout');
% xlabel('Subject');
% zlabel('Frequency');
% title('Left');
% 
% figure
% bar3(stephistright)
% ylabel('Steps in each walking bout');
% xlabel('Subject');
% zlabel('Frequency');
% title('Right');
% 
% edges = 1:2:99;
% figure
% bar3(edges,disthistleft)
% ylabel('Distance of each walking bout (m)');
% xlabel('Subject');
% zlabel('Frequency');
% title('Left');
% 
% figure
% bar3(edges,disthistright)
% ylabel('Distance of each walking bout (m)');
% xlabel('Subject');
% zlabel('Frequency');
% title('Right');
% 
% 
% % mean and standard deviation
% 
% figure
% data = mean(stephistleft,2);
% x = 1:50;
% bar(x,data)
% hold on
% er=errorbar(x,data,std(stephistleft,0,2),std(stephistleft,0,2));
% er.Color = [0 0 0];
% er.LineStyle = 'none';
% xlabel('Steps in each walking bout');
% ylabel('Frequency');
% title('Left');
% 
% 
% figure
% data = mean(stephistright,2);
% bar(x,data)
% hold on
% er=errorbar(x,data,std(stephistright,0,2),std(stephistright,0,2));
% er.Color = [0 0 0];
% er.LineStyle = 'none';
% xlabel('Steps in each walking bout');
% ylabel('Frequency');
% title('Right');
% 
% figure
% data = mean(disthistleft,2);
% bar(edges,data)
% hold on
% er=errorbar(edges,data,std(disthistleft,0,2),std(disthistleft,0,2));
% er.Color = [0 0 0];
% er.LineStyle = 'none';
% xlabel('Distance of each walking bout (m)');
% ylabel('Frequency');
% title('Left');
% 
% figure
% data = mean(disthistright,2);
% bar(edges,data)
% hold on
% er=errorbar(edges,data,std(disthistright,0,2),std(disthistright,0,2));
% er.Color = [0 0 0];
% er.LineStyle = 'none';
% xlabel('Distance of each walking bout (m)');
% ylabel('Frequency');
% title('Right');
% 
% 
% 
% end